<?php
$main="master";
$sub="staff";

include "header.php";
if($mode=="list"){
	$title="<strong>Staff List</strong>";
	//include "titlebar.php";
	?>
<script>
function removeStaff(id,catID){
	var r = confirm("Do you want to remove this Staff..?");
	if (r == true) {
		str="id="+id;	
		url="<?php echo site_url(); ?>delete_staff";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				if(data=='deleted'){
					alert("Staff Removed Successfully.");
					window.location.href="<?php echo site_url(); ?>staff";						
				}
			}		
		});			
	}
}
</script>
<div id="staff">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Staff</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->
		<?php
		//echo $param;
		if($param==0){
			$action="staff";
		} else if($param!=0){
			$action="liststaff/".$param;
		}
		?>
		<?php echo validation_errors(); ?>
		<?php echo form_open($action) ?>
			<div class="row" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<div class="col-md-6">
							<h2>Staff<small></small></h2>
						</div>
						<div class="col-md-6 text-right">
							<a href="<?php echo site_url();?>addstaff" class="btn btn-info btn-condensed"><span class="fa fa-plus"></span> Add New Staff</a> </div>
							
					</div>
					<!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <div class="panel-heading">                                                               
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table datatable">
                                    <thead>
                                        <tr>
                                            <th class="text-center">S.No</th>
                                            <th class="text-center">Staff Name</th>
                                            <th class="text-center">Staff Code</th>
                                            <th class="text-center">Type</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
									//print_r($staffList);
									$len=count($staffList);
									if($len>0){
										$i=0;
										foreach($staffList as $row){
											$i++;
									?>
									    <tr>
                                            <td align="center"><?=$i?></td>
                                            <td align="left"><?=$row['staff_name']?></td>
                                            <td align="center"><?=$row['staff_code']?></td>
                                            <td align="center"><?=$row['staff_type_name']?></td>
                                            <td align="center"><?=$row['staff_status']?></td>
                                            <td align="center">
                                                <a href="<?php echo site_url();?>stafftask/<?=$row['staff_id']?>" class="btn btn-info btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Task"><span class="fa fa-tasks"></span></a>                                                          
                                                <a href="<?php echo site_url();?>editstaff/<?=$row['staff_id']?>" class="btn btn-success btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="fa fa-pencil"></span></a>                                                          
                                                <a href="javascript:removeStaff(<?=$row['staff_id']?>)" class="btn btn-danger btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Delete"><span class="fa fa-times"></span></a>
                                            </td>
                                        </tr>
									<?php
										}
									} else {
									?>
										<tr>
											<td colspan="7" align="center"><b>No Records Found</b></td>
										</tr>
									<?php
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END DEFAULT DATATABLE -->
    			</div>
    		</div>
    	</div>
    </form>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>	
<?php
if($mode=="add"){
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>staff">Staff</a></li>
	<li class="active" >Add New Staff</li>
</ul>
<div class="page-content-wrap" id="staff">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('save_staff',$attr); ?>
		<div class="row form-horizontal" style="padding-top:10px;">
			<div class="col-md-12">
				<div class="page-title">
					<div class="page-title">
                        <div class="col-md-6">
                            <h2>Add New Staff<small></small></h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?=site_url()?>liststaff/<?=$catPastID?>" class="btn btn-info btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
                        </div>                   
                    </div>
				</div>
				<div class="panel panel-hidden-controls">
					<!-- <div class="panel-heading">
						<h1 class="panel-title">Add New Staff</h1>
					</div> -->
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Staff Name</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="staff_name" id="staff_name" placeholder="Enter Staff Name"/>											
										</div>
									</div>
								</div>	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Staff Code</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="staff_code" id="staff_code" placeholder="Enter Staff Code"/>
										</div>
									</div>
								</div>
                                <div class="col-md-12"><br></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Email ID</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="staff_email" id="staff_email" placeholder="Enter Email ID"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Mobile No</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="staff_mobile" id="staff_mobile" placeholder="Enter Mobile No"/>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-12"><br></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Type</label>
                                        <div class="col-md-8">
                                            <select class="form-control select" name="staff_type" id="staff_type">
												<option value="">Select Type</option>
												<?php
												foreach($typeList as $typ){
												?>
												<option value="<?=$typ['staff_type_id']?>"><?=$typ['staff_type_name']?></option>												
												<?php
												}
												?>
											</select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Date of Join</label>
                                        <div class="col-md-8">
											<div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" class="form-control datepicker" name="staff_doj" id="staff_doj" placeholder="Enter Date of Join"/>                                           
                                            </div>
										</div>
                                    </div>
                                </div>									
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Basic Salary</label>
                                        <div class="col-md-8">
											<div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-inr"></span></span>
                                                <input type="text" class="form-control" name="staff_basicsalary" id="staff_basicsalary" placeholder="Enter Basic Salary"/>
											</div>
										</div>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Address</label>
                                        <div class="col-md-8">
                                            <textarea placeholder="Enter Your Address" class="form-control" rows="3" name="staff_address" id="staff_address"></textarea>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">City</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="staff_city" id="staff_city" placeholder="Enter City"/>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Country</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="staff_country" id="staff_country" placeholder="Enter Country"/>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-12"><br></div>							
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Status</label>
										<div class="col-md-8">
											<select class="form-control select" name="staff_status" id="staff_status">
												<option value="Active">Active</option>
												<option value="Inactive">Inactive</option>
											</select>
										</div>
									</div>
								</div>
                                <div class="col-md-12"><br></div>
                                <div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
                                    <a href="<?=site_url()?>staff" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
                                   
                                </div>
							</div>
						</div>
					</div>
				</div>											                                
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
} 
?>
<?php
if($mode=="edit"){
	//print_r($staffDets);
	extract($staffDets);
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>staff">Staff</a></li>
	<li class="active">Edit Staff</li>
</ul>
<div class="page-content-wrap">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('update_staff',$attr); ?>
		<div class="row form-horizontal">
			<div class="col-md-12">
				<div class="page-title">
					<h2>Staff<small></small></h2>
				</div>
				<div class="panel panel-primary panel-hidden-controls">
					<div class="panel-heading">
						<h1 class="panel-title">Edit Staff</h1>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Staff Name</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="staff_name" id="staff_name" placeholder="Enter Staff Name" value="<?=$staff_name?>"/>											
											<input type="hidden" class="form-control" name="staff_id" id="staff_id" value="<?=$staff_id?>" />											
										</div>
									</div>
								</div>	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Staff Code</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="staff_code" id="staff_code" placeholder="Enter Staff Code" value="<?=$staff_code?>" />
										</div>
									</div>
								</div>
                                <div class="col-md-12"><br></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Email ID</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="staff_email" id="staff_email" placeholder="Enter Email ID" value="<?=$staff_mobile?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Mobile No</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="staff_mobile" id="staff_mobile" placeholder="Enter Mobile No" value="<?=$staff_mobile?>" />
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-12"><br></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Type</label>
                                        <div class="col-md-8">
                                            <select class="form-control select" name="staff_type" id="staff_type">
												<option value="">Select Type</option>
												<?php
												foreach($typeList as $typ){
													$sel=""; 
													if($typ['staff_type_id']==$staff_type){
														$sel="selected";
													}
												?>
												<option value="<?=$typ['staff_type_id']?>" <?=$sel?>><?=$typ['staff_type_name']?></option>												
												<?php
												}
												?>
											</select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Date of Join</label>
                                        <div class="col-md-8">
										<?php
										$doj=date("d/m/Y");
										if($staff_doj!="0000-00-00"){
											$doj=date("d/m/Y",strtotime($staff_doj));
										}
										?>
											<div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" class="form-control datepicker" name="staff_doj" id="staff_doj" placeholder="Enter Date of Join" value="<?=$doj?>" />                                           
                                            </div>
										</div>
                                    </div>
                                </div>									
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Basic Salary</label>
                                        <div class="col-md-8">
											<div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-inr"></span></span>
                                                <input type="text" class="form-control" name="staff_basicsalary" id="staff_basicsalary" placeholder="Enter Basic Salary" value="<?=$staff_basicsalary?>" />
											</div>
										</div>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Address</label>
                                        <div class="col-md-8">
                                            <textarea placeholder="Enter Your Address" class="form-control" rows="3" name="staff_address" id="staff_address"><?=$staff_address?></textarea>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">City</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="staff_city" id="staff_city" placeholder="Enter City" value="<?=$staff_city?>" />
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Country</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="staff_country" id="staff_country" placeholder="Enter Country" value="<?=$staff_country?>" />
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-12"><br></div>							
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Status</label>
										<div class="col-md-8">
											<select class="form-control select" name="staff_status" id="staff_status">
												<option value="Active">Active</option>
												<option value="Inactive">Inactive</option>
											</select>
											<script>
												setSelectedIndex(document.getElementById('staff_status'),"<?= $staff_status ?>");
											</script>
										</div>
									</div>
								</div>
                                <div class="col-md-12"><br></div>
                                <div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
                                    <a href="<?=site_url()?>staff" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
                                   
                                </div>
							</div>
						</div>
					</div>
				</div>								                           
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
}
include "footer.php"; 
?>
