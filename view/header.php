<?php
//print_r($_SESSION);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- META SECTION -->
		<title>DS Management</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!-- <link rel="icon" href="favicon.ico" type="image/x-icon" /> -->
		<!-- END META SECTION -->
		<!-- CSS INCLUDE -->   
		<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url(); ?>view/css/theme-default.css"/>
		<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url(); ?>view/css/theme-sas.css"/>
		<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url(); ?>view/css/style.css"/>
		<!-- EOF CSS INCLUDE -->      
		<script src="<?php echo base_url(); ?>view/js/jquery.min.js"></script>                                 
		<!--script src="<?php echo base_url(); ?>view/js/ajax_mappingfile.js"></script-->
		<script src="<?php echo base_url(); ?>view/js/functions.js"></script>
		<link href="<?php echo base_url(); ?>view/js/autocomplete/jquery-ui.css" rel="stylesheet" type="text/css" />
		
		<style>
		.dropdown-menu {
			min-width: 100px;
		}
		</style>
	</head>
	<body>
	<script>
	function setSelectedIndex(s, v) {
		for ( var i = 0; i < s.options.length; i++ ) {
			if ( s.options[i].value == v ) {
				s.options[i].selected = true;
				return;
			}
		}
	}
	function goBack(){
		window.history.go(-1);
	}
	</script>
	
	<?php
	$cursign="AED";
	?>	
		<!-- START PAGE CONTAINER -->        
		<div class="page-container page-navigation-top-fixed">
		<!-- START PAGE SIDEBAR -->
		<div class="page-sidebar">
			<!-- START X-NAVIGATION -->
			<ul class="x-navigation">
				<li class="xn-logo">
					<a href="<?php echo base_url(); ?>index.php">CRM</a>
					<a href="#" class="x-navigation-control"></a>					
				</li>				
				
				<li><a href="<?php echo site_url();?>dashboard"><span class="fa fa-desktop"></span> <span class="xn-text"> Dashboard</span></a></li>
				<?php
				$master="";				 
				if($main=="master"){ 
					$master="active"; 					
					$staff=""; if($sub=="staff"){ $staff="active"; }
					$client=""; if($sub=="client"){ $client="active"; }
					$local=""; if($sub=="localtour"){ $local="active"; }
					$trans=""; if($sub=="transport"){ $trans="active"; }
				}
				?>	
				<li class="xn-openable <?=$master?>"><a href="#"><span class="glyphicon glyphicon-briefcase"></span><span class="xn-text">Masters</span></a>
					<ul>
	          			<li class="<?=$staff?>"><a href="<?php echo site_url();?>staff"><span class="fa fa-user"></span> <span class="xn-text"> Staff</span></a></li>
	          			<li class="<?=$client?>"><a href="<?php echo site_url();?>client"><span class="fa fa-male"></span> <span class="xn-text"> Client</span></a></li>
	          			<!--<li class="<?=$clnt?>"><a href="<?php echo site_url();?>team"><span class="fa fa-users"></span> <span class="xn-text"> Team</span></a></li> -->
					</ul>
				</li>
				<?php
				$transaction="";				 
				if($main=="transaction"){ 
					$transaction="active"; 					
					$project=""; if($sub=="project"){ $project="active"; }
					$payItm=""; if($sub=="payitem"){ $payItm="active"; }
					$pay=""; if($sub=="payables"){ $pay="active"; }
					$payment=""; if($sub=="payments"){ $payment="active"; }
				}
				?>	
				<li class="xn-openable <?=$transaction?>"><a  href="#"><span class="fa fa-money"></span><span class="xn-text">Transactions</span></a>
					<ul>
						<li class="<?=$project?>"><a href="<?php echo site_url();?>project"><span class="fa fa-file-o"></span> <span class="xn-text"> Project</span></a></li>
						<li class="<?=$payItm?>"><a href="<?php echo site_url();?>payableitems"><span class="fa fa-file-text"></span> <span class="xn-text"> Payable Items</span></a></li>						
	          			<li class="<?=$pay?>"><a href="<?php echo site_url();?>payables"><span class="fa fa-inr"></span> <span class="xn-text"> Payables</span></a></li>
						
						<!--<li class="<?=$payment?>"><a href="<?php echo site_url();?>payments"><span class="fa fa-money"></span> <span class="xn-text"> Payments</span></a></li>
						<li class="<?=$clnt?>"><a href="<?php echo site_url();?>invoice"><span class="fa fa-file-text"></span> <span class="xn-text"> Invoice</span></a></li>
						
	          			<li class=""><a href="<?php echo site_url();?>paymentreceived"><span class="fa fa-dollar"></span> <span class="xn-text"> Payment Received</span></a></li>
	          			<li class=""><a href="<?php echo site_url();?>budget"><span class="fa fa-table"></span> <span class="xn-text"> Project Budget</span></a></li>
	          			<li class=""><a href="<?php echo site_url();?>projectcostsheet"><span class="fa fa-copy"></span> <span class="xn-text"> Project Cost Sheet</span></a></li>
	          			
						<li class="<?=$clnt?>"><a href="<?php echo site_url();?>projectcostsheet"><span class="fa fa-file-text-o"></span> <span class="xn-text"> Project Cost Sheet</span></a></li> -->
	          			
					</ul>
				</li>
				
				<?php
				$report="";				 
				if($main=="report"){ 
					$report="active"; 					
					$production=""; if($sub=="production"){ $production="active"; }					
				}
				?>	
				<li class="xn-openable <?=$report?>"><a  href="#"><span class="fa fa-clipboard"></span><span class="xn-text">Report</span></a>
					<ul>
						
	          			
	          			<!--<li class="<?=$clnt?>"><a href="<?php echo site_url();?>projecttask"><span class="fa fa-table"></span> <span class="xn-text"> Project Task</span></a></li>
						<li class=""><a href="<?php echo site_url();?>dailytask"><span class="fa fa-tasks"></span> <span class="xn-text"> Daily Task</span></a></li>
						<li class=""><a href="<?php echo site_url();?>taskreport"><span class="fa fa-list-alt"></span> <span class="xn-text"> Task Report</span></a></li>
						<li class=""><a href="<?php echo site_url();?>quotation"><span class="fa fa-file-text"></span> <span class="xn-text"> Quotation</span></a></li> -->
	          			<li class="<?=$production?>"><a href="<?php echo site_url();?>production"><span class="fa fa-copy"></span> <span class="xn-text"> Production</span></a></li>
					</ul>
				</li>
				<?php
				$setting="";				 
				if($main=="settings"){ 
					$setting="active"; 					
					$service=""; if($sub=="service"){ $service="active"; }
					$budgethead=""; if($sub=="budgethead"){ $budgethead="active"; }					
				}
				?>	
				<li class="xn-openable <?=$setting?>">
					<a href="#"><span class="xn-text"><span class="fa fa-cogs"></span> Settings</span></a>
					<ul>
						<li class="<?=$budgethead?>"><a href="<?php echo site_url()?>budgethead"><span class="xn-text"><span class="fa fa-briefcase"></span> Budget Head</span></a></li>						
						<li class="<?=$service?>"><a href="<?php echo site_url()?>service"><span class="xn-text"><span class="glyphicon glyphicon-wrench"></span> Service Item</span></a></li>						
						<!--<li class="<?=$sett?>"><a href="<?php echo site_url()?>user"><span class="xn-text"><span class="glyphicon glyphicon-cog"></span> User Settings</span></a></li>						
						<li class="<?=$sett?>"><a href="<?php echo site_url();?>settings"><span class="xn-text"><span class="glyphicon glyphicon-cog"></span> Company Settings</span></a></li> -->
						<li class="<?=$ch?>"><a href="<?php echo site_url();?>changepassword"><span class="xn-text"><span class="glyphicon glyphicon-cog"></span> Change Password</span></a></li>
					</ul>
				</li>
				
				<!--<?php
				$set=""; 
				if($main=="settings"){ 
					$set="active"; 																			
				}
				?>	
				<li class="<?=$set?>"><a href="<?php echo site_url();?>settings"><span class="xn-text">Settings</span></a></li>
				<?php
				$ch=""; 
				if($main=="changepassword"){ 
					$ch="active"; 																			
				}
				?>	
				<li class="<?=$ch?>"><a href="<?php echo site_url();?>changepassword"><span class="xn-text">Change Password</span></a></li> -->
				<li><a href="<?php echo site_url();?>logout"><span class="xn-text"><span class="glyphicon glyphicon-log-out"></span> Logout</span></a></li>
			</ul>
		</div>
		<!-- END PAGE SIDEBAR -->

		<div class="page-content">
	   <!-- START X-NAVIGATION VERTICAL -->
			<ul class="x-navigation x-navigation-horizontal x-navigation-panel">
				<!-- TOGGLE NAVIGATION -->
				<li class="xn-icon-button">
					<a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>                    
				</li>
				<!-- END TOGGLE NAVIGATION -->
	           <!-- <li  style="color:#FFFFFF; padding-top:15px; padding-left:15px; padding-bottom:10px; font-size:18px; border-left:#fff solid thin"><?=$title?></li>
	             -->
	           
				<!-- SEARCH -->
				<!--<li class="xn-search">
					<form role="form">
					    <input type="text" name="search" placeholder="Search..."/>
					</form>
					</li> -->   
				<!-- END SEARCH -->                    
				<!-- POWER OFF -->
				<li class="xn-icon-button pull-right last">
					<a href="" onClick="logout()"><span class="fa fa-power-off"></span></a>
					<ul class="xn-drop-left animated zoomIn">
						<!--     <li><a href="pages-lock-screen.html"><span class="fa fa-lock"></span> Lock Screen</a></li> !-->
						<li><a href="<?php echo site_url();?>logout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
					</ul>
				</li>
				<!-- END POWER OFF -->    
				<li class="xn-icon-button pull-right">
					<a href="#changepwd"><span class="fa fa-gear"></span></a>
					<ul class="xn-drop-left animated zoomIn">
						<!-- <li><a href="javascript: getDivContent('ajax_fixing_id','editprofile_pg','editprofile','','') ">Edit Profile</a></li> -->
						<li><a href="<?php echo site_url();?>changepassword">Change Password</a></li>
					</ul>
				</li>
				<li class="pull-right">
					<div class="alert alert-success text-center" role="alert" style="opacity:1; float:right; position:relative; top:50px; width:300px; display:none;" id="alert_div">               </div>
				</li>
			</ul>
<!-- END X-NAVIGATION VERTICAL -->