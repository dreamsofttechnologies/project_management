<?php 
include "header.php";
//print_r($_SESSION);
	?>
<div id="labdiv">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Dashboard</li>
	</ul>
	<div class="row" style="padding:100px 0px 0px 0px;">
		<div class="col-md-3">
			<div class="widget widget-warning" style="background:rgba(51, 133, 217, 1);">
				<div class="widget-title">Ordered</div>				
				<div class="widget-int"><?=$order['count']?></div>				
				<div class="widget-subtitle"><b><i>Paid : <?=$order['paid']?></i></b></div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="widget widget-info" style="background:rgba(51, 133, 217, 0.7);">
				<div class="widget-title">Dispatched</div>				
				<div class="widget-int"><?=$dispatch['count']?></div>				
				<div class="widget-subtitle"><b><i>Paid : <?=$dispatch['paid']?></i></b></div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="widget widget-success" style="background:rgba(51, 133, 217, 0.6);">
				<div class="widget-title">Delivered</div>				
				<div class="widget-int"><?=$deliver['count']?></div>				
				<div class="widget-subtitle"><b><i>Paid : <?=$deliver['paid']?></i></b></div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="widget widget-danger" style="background:rgba(51, 133, 217, 0.4);">
				<div class="widget-title">Canceled</div>				
				<div class="widget-int"><?=$cancel['count']?></div>				
				<div class="widget-subtitle"><b><i>Paid : <?=$cancel['paid']?></i></b></div>
			</div>
		</div>
	</div>    
	<div class="col-md-12"><br /><br /></div>
</div>
<div style="clear:both;"></div>
<?php include_once "footer.php";?>