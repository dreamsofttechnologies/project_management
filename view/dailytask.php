<?php
$main="main";
$sub="category";

include "header.php";
if($mode=="list"){
	$title="<strong>Category List</strong>";
	//include "titlebar.php";
	?>
<script>
function removecategory(id,catID){
	var r = confirm("Do you want to remove this Category..?");
	if (r == true) {
		str="id="+id;	
		url="<?php echo site_url(); ?>delete_category";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				if(data=='deleted'){
					alert("Category Removed Successfully.");
					if(catID==""){
						window.location.href="<?php echo site_url(); ?>category";	
					} else {
						window.location.href="<?php echo site_url(); ?>listcategory/"+catID;	
					}
				}
			}		
		});			
	}
}

</script>
<div id="dailytask">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Daily Task</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->
		<?php echo validation_errors(); ?>
		<?php echo form_open("save_dailytask") ?>
			<div class="row" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<div class="col-md-6">
							<h2>Dailytask<small></small></h2>
						</div>		
					</div>
					<!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <div class="panel-heading">                                
                            <div class="row">
								<div class="col-md-4">                                
                                    <div class="form-group">
										<label class="control-label">Project Name : <b><?=$taskDets['project_name']?></b></label>
										<input type="hidden" name="dailytask_project_id" id="dailytask_project_id" value="<?=$taskDets['project_id']?>" />
									</div> 
								</div>
								
                                <div class="col-md-4">                                
                                    <div class="form-group">
										<label class="control-label">Task Name : <b><?=$taskDets['task_name']?></b></label>
										<input type="hidden" name="dailytask_task_id" id="dailytask_task_id" value="<?=$taskDets['task_id']?>" />
									</div> 	
								</div>
								
								<div class="col-md-4">                                
                                    <div class="form-group">
										<label class="control-label">Staff Name : <b><?=$taskDets['staff_name']?></b></label>
										<input type="hidden" name="dailytask_staff_id" id="dailytask_staff_id" value="<?=$taskDets['staff_id']?>" />
									</div> 	
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-4">                                
                                    <div class="form-group">
										<label class="control-label">Date</label>
										<div class="input-group col-md-12"> 
											<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
											<input type="text" name="dailytask_date" id="dailytask_date" class="form-control datepicker" value="<?=date("d/m/Y")?>">  
										</div>
									</div> 	
								</div>
								
								<div class="col-md-4">                                
									<div class="form-group">
										<label class="control-label">Hours</label>
										<div class="input-group col-md-12"> 
											<span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
											<input type="text" name="dailytask_hours" id="dailytask_hours" class="form-control" placeholder="1 hrs">  
										</div>
									</div>	
								</div>
								<div class="col-md-12"><br></div>
                                <div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
                                    <a href="<?=site_url()?>projecttask/<?=$taskDets['project_id']?>" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a>                                     
                                </div>
							</div>                             
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table datatable">
                                    <thead>
                                        <tr>
                                            <th class="text-center">S.No</th>
											<th class="text-center">Project Name</th>
                                            <th class="text-center">Task Name</th>
                                            <th class="text-center">Staff Name</th>
                                            
                                            <th class="text-center">Date</th>
                                            <th class="text-center">Hours</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                   	<?php
									//print_r($dailyTaskList);
									$len=count($dailyTaskList);
									if($len>0){
										$i=0;
										foreach($dailyTaskList as $row){
											$i++;
									?>
									    <tr>
                                            <td align="center"><?=$i?></td>
                                            <td align="left"><?=$row['project_name']?></td>
                                            <td align="left"><?=$row['task_name']?></td>
                                            <td align="left"><?=$row['staff_name']?></td>
                                            <td align="center"><?=date("d/m/Y",strtotime($row['dailytask_date']))?></td>
                                            <td align="center"><?=$row['dailytask_hours']?> Hrs</td>
                                        </tr>
									<?php
										}
									} else {
									?>
										<tr>
											<td align="center" ></td>
										</tr>
									<?php
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END DEFAULT DATATABLE -->
    			</div>
    		</div>
    	</div>
    </form>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>	
<?php
if($mode=="add"){
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>category">Daily Task</a></li>
	<li class="active" >Add New Daily Task</li>
</ul>
<div class="page-content-wrap" id="dailytask">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('save_category',$attr); ?>
		<div class="row form-horizontal" style="padding-top:10px;">
			<div class="col-md-12">
				<div class="page-title">
					<div class="page-title">
                        <div class="col-md-6">
                            <h2>Add New Dailytask<small></small></h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?=site_url()?>listcategory/<?=$catPastID?>" class="btn btn-info btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
                        </div>                   
                    </div>
				</div>
				<div class="panel panel-hidden-controls">
					<!-- <div class="panel-heading">
						<h1 class="panel-title">Add New Category</h1>
					</div> -->
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Name</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="category_name" id="category_name" placeholder="Enter Your Category Name"/>
											<input type="hidden" class="form-control" name="catID" id="catID" value="<?=$catID?>" />
										</div>
									</div>
								</div>	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Designation</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="category_order" id="category_order" placeholder="Enter Your Designation"/>
										</div>
									</div>
								</div>
                                <div class="col-md-12"><br></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Email ID</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="category_order" id="category_order" placeholder="Enter Your Email ID"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Mobile No</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="category_order" id="category_order" placeholder="Enter Your Mobile No"/>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-12"><br></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Username</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="category_order" id="category_order" placeholder="Enter Your Username"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Password</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="category_order" id="category_order" placeholder="Enter Your Password"/>
                                        </div>
                                    </div>
                                </div>									
								<div class="col-md-12"><br></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Address</label>
                                        <div class="col-md-8">
                                            <textarea placeholder="Enter Your Address" class="form-control" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>								
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Status</label>
										<div class="col-md-8">
											<select class="form-control select" name="category_status" id="category_status">
												<option value="Active">Active</option>
												<option value="Inactive">Inactive</option>
											</select>
										</div>
									</div>
								</div>
                                <div class="col-md-12"><br></div>
                                <div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
                                    <?php
                                    if($catID){
                                    ?>
                                    <a href="<?=site_url()?>listcategory/<?=$catID?>" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
                                    <?php
                                    } else {
                                    ?>
                                    <a href="javascript:goBack()" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
                                    <?php
                                    }
                                    ?>
                                </div>
							</div>
						</div>
					</div>
				</div>											                                
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
} 
?>
<?php
if($mode=="edit"){
	//print_r($category);
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>category">Category</a></li>
	<li class="active">Edit Category</li>
</ul>
<div class="page-content-wrap">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('update_category',$attr); ?>
		<div class="row form-horizontal">
			<div class="col-md-12">
				<div class="page-title">
					<h2>Category<small></small></h2>
				</div>
				<div class="panel panel-primary panel-hidden-controls">
					<div class="panel-heading">
						<h1 class="panel-title">Edit Category</h1>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Category Name</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="category_name" id="category_name" placeholder="Enter First Name" value="<?=$category['category_name']?>"/>
											<input type="hidden" class="form-control" name="category_id" id="category_id" placeholder="Enter Category Code" value="<?=$category['category_id']?>"/>
											<input type="hidden" class="form-control" name="catID" id="catID" value="<?=$category['category_cat_id']?>"/>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Order Index</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="category_order" id="category_order" placeholder="Enter Order Index" value="<?=$category['category_order']?>"/>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Status</label>
										<div class="col-md-8">
											<select class="form-control select" name="category_status" id="category_status">
												<option value="Active">Active</option>
												<option value="Inactive">Inactive</option>
											</select>
										</div>
										<script>
											setSelectedIndex(document.getElementById('category_status'),"<?= $category['prd_status'] ?>");
										</script>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>				
				
				<div class="panel-footer"  style="text-align:center">
		   			<button type="submit" class="btn btn-warning"><span class="fa fa-check-circle"></span> Save</button> 
					<?php
					if($category['prd_catid']){
					?>
					<a href="<?=site_url()?>listcategory/<?=$category['prd_catid']?>" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
					<?php
					} else {
					?>
					<a href="javascript:goBack()" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
					<?php
					}
					?>
		   		</div>                                
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
}
include "footer.php"; 
?>
