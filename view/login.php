<!DOCTYPE html>
<html lang="en" class="body-full-height">
	<head>
		<!-- META SECTION -->
		<title>DS Project Management</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!-- END META SECTION -->
		<!-- CSS INCLUDE -->        
		<script src="<?php echo base_url(); ?>view/js/jquery.min.js"></script>                                 		
		<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url(); ?>view/css/theme-sas.css"/>
		<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url(); ?>view/css/style.css"/>
		<!-- EOF CSS INCLUDE -->                          
	</head>
	<body>
		<div class="page-container" style="background: #33414e;">
			<div class="row">				
				<div class="col-md-12">
					<div class="login-container" >        
						<div class="login-box ">
              				<div style="text-align:center; padding-bottom:10px"><img src="<?php echo base_url(); ?>view/img/logo.png"></div>
							<div class="login-body">
              				<?php
							//print_r($_SESSION);
							?>
								<div class="login-title"><strong>Welcome</strong>, Please login</div>
								<form action="login_check" name="login_form" id="login_form" class="form-horizontal" method="post">
									<?php
									if($this->session->flashdata('message')!=""){
									?>
									<div class="col-md-12 successbox"><?=$this->session->flashdata('message')?></div>
									<?php
										$this->session->set_flashdata('message','');
									} else if($this->session->flashdata('error')){ 
									?>
									<div class="col-md-12 errorbox"><?=$this->session->flashdata('error')?></div>
									<?php
										$this->session->set_flashdata('error','');
									}
									?>
									<div class="form-group">
										<div class="col-md-12" style="color:#fff;">
											<input type="hidden" name="token_key" id="token_key" value="<?=$token_key?>">
											<input type="text" name="usrname" id="usrname" class="form-control" placeholder="Username"  />
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12" style="color:#fff;">
											<input type="password" name="usrpwd" id="usrpwd" class="form-control" placeholder="Password" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-6">
										</div>
										<div class="col-md-6">
											<button type="submit" class="btn btn-warning btn-block" name="log_submit" id="log_submit">Log In</button>											
										</div>
									</div>
									<div class="col-md-12" id="error_div" style="display:none;"></div>						
								</form>
							</div>
						</div>
						<div style="font-size:18px; text-align:center"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
