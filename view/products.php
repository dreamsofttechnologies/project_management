<?php
$main="main";
$sub="products";

include "header.php";
if($mode=="list"){
	$title="<strong>Products List</strong>";
	//include "titlebar.php";
	?>
<script>
function removeproducts(id){
	var r = confirm("Do you want to remove this Products..?");
	if (r == true) {
		str="id="+id;	
		url="<?php echo site_url(); ?>delete_products";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				if(data=='deleted'){
					alert("Products Removed Successfully.");
					window.location.href="<?php echo site_url(); ?>listproducts/<?=$param?>";	
				}
			}		
		});			
	}
}

function removecategory(id,catID){
	var r = confirm("Do you want to remove this Category..?");
	if (r == true) {
		str="id="+id;	
		url="<?php echo site_url(); ?>delete_category";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				if(data=='deleted'){
					alert("Category Removed Successfully.");
					//window.location.href="<?php echo site_url(); ?>category";	
					window.location.href="<?php echo site_url(); ?>listproducts/"+catID;	
				}
			}		
		});			
	}
}

function rowLimit(v){
	pg=$('#page').val();
	window.location.href="<?php echo site_url(); ?>products/"+pg+"/"+v;					
}

</script>
<div id="labdiv">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Products</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->
		<?php echo validation_errors(); ?>
		<?php echo form_open('listproducts/'.$param) ?>
			<div class="row" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<div class="col-md-6">
							<h2>Category<small></small></h2>
						</div>
						<div class="col-md-6 text-right">
						<?php
						if($backID!=0){
						?>	
						<a href="<?=site_url()?>listproducts/<?=$backID?>" class="btn btn-link "><span class="fa fa-arrow-left"></span>Back</a> 
						<?php
						} else {
						?>
						<a href="<?=site_url()?>category" class="btn btn-link "><span class="fa fa-arrow-left"></span>Back</a> 
						<?php
						}
						?>
						</div>
					</div>
					<div class="panel panel-hidden-controls">				
						<div class="panel-body">
							<div class="col-md-9">
								<div class="input-group">
									<input class="form-control" id="cat_search" name="cat_search" type="text" placeholder="Search here" value="<?=$_REQUEST['cat_search']?>" >
									<div class="input-group-btn">
										<button type="submit" class="btn btn-info" name="btn_searchid" id="btn_searchid"><i class="fa fa-search"></i></button>
									</div>
								</div>                                            
							</div>						
							<div class="pull-right" style="margin-right:20px;">
								<a href="<?php echo site_url();?>addcategory/<?=$param?>" class="btn btn-warning pull-right" style="margin-right:0px;"><span class="fa fa-plus"></span> Add New Category</a> 
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-primary">
									<div class="panel-body panel-body-table">
										<div class="table-responsive1">										
											<table class="table table-hover table-striped table-actions" >
												<thead>													
													<tr>
														<!--th class="table_header"><input type="checkbox" id="main_chk" name="main_chk" class="" onClick="javascript: selectAll('category_check')"/></th-->
														<th class="table_header text-center">S.No. <input type="hidden" name="hidCount" id="hidCount" value="<?=$total_rows?>" /><input type="hidden" name="st_from" id="st_from" value="<?=$pg?>" /> </th>
														<th class="table_header text-center">Category Name</th>
														<th class="table_header text-center">No of Products</th>														
														<th class="table_header text-center">Status</th>
														<th class="table_header text-center">Action</th>
													</tr>
												</thead>
												<tbody id="tbl_dataload">
												<?php												
												$len=count($category);
												//print_r($category);
												if($len>0){
													foreach ($category as $b):
														$j++;
														//print_r($category);
														//print_r($unit);
													?>
													<tr>
														<!--td> <input type="checkbox" class="" name="category_check<?=$j?>" id="category_check<?=$j?>" value="<?=$b['cat_id']?>" /></td-->
														<td align="center"><?=$j?></td>
														<td align="center"><?=$b['prd_name']?></td>
														<td align="center"><?=$b['catCount']?></td>
														<td align="center"><?=$b['prd_status']?></td>														
														<td align="center">															
															<a href="<?php echo site_url();?>listproducts/<?=$b['prd_id']?>" class="btn btn-info btn-sm btn-condensed" data-toogle="tooltip" title="Products"><b>Products</b></a>
															<a href="<?php echo site_url();?>editcategory/<?=$b['prd_id']?>" class="btn btn-success btn-sm btn-condensed" data-toogle="tooltip" title="Edit"><span class="fa fa-pencil"></span></a>															
															<a href="javascript:removecategory('<?=$b['prd_id']?>','<?=$b['prd_catid']?>')" class="btn btn-danger btn-sm btn-condensed" data-toogle="tooltip" title="Delete"><span class="fa fa-times"></span></a>																
														</td>
													</tr>
													<?php 	
													endforeach; 														
												}else {	 
													?>	
													<tr >
														<td align="center" colspan="8"><strong><br />No Records Found<br /><br /></strong></td>
													</tr>
												<?php	
												}
												?>	
												</tbody>
											</table>											
										</div>
									</div>
								</div>
							</div>
						</div>					
				</div>
				
				
				<div class="col-md-12">
					<div class="page-title">
						<h2>Products<small></small></h2>
					</div>
					<div class="panel panel-hidden-controls">				
						<div class="panel-body">
							<div class="col-md-9">
								<div class="input-group">
									<input class="form-control" id="prod_search" name="prod_search" type="text" placeholder="Search here" value="<?=$_REQUEST['prod_search']?>" >
									<div class="input-group-btn">
										<button type="submit" class="btn btn-info" name="btn_searchid" id="btn_searchid"><i class="fa fa-search"></i></button>
									</div>
								</div>                                            
							</div>						
							<div class="pull-right" style="margin-right:20px;">
								<a href="<?php echo site_url();?>addproducts/<?=$param?>" class="btn btn-warning pull-right" style="margin-right:0px;"><span class="fa fa-plus"></span> Add New Products</a> 
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-primary">
									<div class="panel-body panel-body-table">
										<div class="table-responsive1">										
											<table class="table table-hover table-striped table-actions" >
												<thead>													
													<tr>
														<!--th class="table_header"><input type="checkbox" id="main_chk" name="main_chk" class="" onClick="javascript: selectAll('products_check')"/></th-->
														<th class="table_header text-center">S.No. <input type="hidden" name="hidCount" id="hidCount" value="<?=$total_rows?>" /><input type="hidden" name="st_from" id="st_from" value="<?=$pg?>" /> </th>
														<th class="table_header text-center">Products Name</th>
														<th class="table_header text-center">Code</th>														
														<th class="table_header text-center">Availability</th>														
														<th class="table_header text-center">Status</th>
														<th class="table_header text-center">Action</th>
													</tr>
												</thead>
												<tbody id="tbl_dataload">
												<?php		
												$j=$pg;										
												$len=count($products);
												//print_r($products);
												if($len>0){
													foreach ($products as $b):
														$j++;
														//print_r($products);
														//print_r($unit);
													?>
													<tr>
														<!--td> <input type="checkbox" class="" name="products_check<?=$j?>" id="products_check<?=$j?>" value="<?=$b['cat_id']?>" /></td-->
														<td align="center"><?=$j?></td>
														<td align="left"><?=$b['prd_name']?></td>
														<td align="center"><?=$b['prd_code']?></td>
														<td align="center"><?=$b['prd_avail']?></td>
														<td align="center"><?=$b['prd_status']?></td>														
														<td align="center">															
															<!--<a href="<?php echo site_url();?>listproducts/<?=$b['prd_id']?>" class="btn btn-info btn-sm btn-condensed" data-toogle="tooltip" title="Edit"><span class="fa fa-list"></span></a> -->
															<a href="<?php echo site_url();?>editproducts/<?=$b['prd_id']?>" class="btn btn-success btn-sm btn-condensed" data-toogle="tooltip" title="Edit"><span class="fa fa-pencil"></span></a>															
															<a href="javascript:removeproducts(<?=$b['prd_id']?>)" class="btn btn-danger btn-sm btn-condensed" data-toogle="tooltip" title="Delete"><span class="fa fa-times"></span></a>																
														</td>
													</tr>
													<?php 	
													endforeach; 														
												}else {	 
													?>	
													<tr >
														<td align="center" colspan="8"><strong><br />No Records Found<br /><br /></strong></td>
													</tr>
												<?php	
												}
												?>	
												</tbody>
											</table>
											<!--<div class="panel-footer">
												<div class="col-md-12 ">																						
													<input type="hidden" name="rows" id="rows" value="<?=$row_limit?>" />
													<input type="hidden" name="page" id="page" value="<?=$pg?>" />
													<div class="col-md-3 text-left">
														<label> Rows Limit: &nbsp;
															<select name="row_show" id="row_show" onChange="javascript: rowLimit(this.value)" >
																<option value="<?=$def_rows?>" <?php if($def_rows==$rows){ echo 'selected="selected"'; }?>><?=$def_rows?></option>
																<option value="<?=($def_rows*2)?>" <?php if(($def_rows*2)==$rows){ echo 'selected="selected"'; }?>><?=($def_rows*2)?></option>
																<option value="<?=($def_rows*4)?>" <?php if(($def_rows*4)==$rows){ echo 'selected="selected"'; }?>><?=($def_rows*4)?></option>
															</select>
														</label>
													</div>
													<div class="col-md-7 text-center">
													<?php	
													
													if (!$pg) { 
														 $pagenum = 1; 
													} else {
														$pagenum = intval($pg/$row_limit)+1; 		
													}
			
													//echo "<br>".$total_rows."===".$row_limit."===".$pagenum;									
													$last = ceil($total_rows/$row_limit); 
													//this makes sure the page number isn't below one, or more than our maximum pages 
													if ($pagenum < 1) { 
														$pagenum = 1; 
													} else if ($pagenum > $last)  { 
														$pagenum = $last; 
													}
													$lower_limit = ($pagenum - 1) * $row_limit;
													//echo "<br>".$pagenum."===".$lower_limit."====".$last;
	
													$st_lp="";
													$ed_lp="";
													if ( ($pagenum-1) > 0) {
													?>	
													
													<a href="<?=site_url().'products/'.$param.'/0/'.$rows?>" class="btn btn-default active">First</a>
													<a href="<?=site_url().'products/'.$param.'/'.(($pagenum-2)*$row_limit).'/'.$rows?>" class="btn btn-default active">Previous</a>
													<?php
													}
													if($pagenum>=5){
														$st_lp=$pagenum-3;
														if($last>$pagenum+3){
															$ed_lp=$pagenum+3;
														} else if($pagenum<=$last && $pagenum>$last-4){
																$st_lp=$last-4;
																$ed_lp=$last;
															} else {
																$ed_lp=$last;
																}
													} else {
														$st_lp=1;
														if($last>4){
															$ed_lp=4;
														} else {
															$ed_lp=$last;
														}
													}
													//echo "<br>".$st_lp." == ".$ed_lp;
													
													//Show page links
													//$a=0;
													for($i=$st_lp; $i<=$ed_lp; $i++) {													
														if ((($i-1)*$row_limit) == $pg) {
												?>
													<a class="btn btn-primary" href="<?=site_url().'products/'.$param.'/'.(($i-1)*$row_limit).'/'.$rows?>"><?php echo $i ?></a>
												<?php		} else {  ?>
													<a href="<?=site_url().'products/'.$param.'/'.(($i-1)*$row_limit).'/'.$rows?>" class="btn btn-default active"><?php echo $i ?></a>
												<?php 
														}
													}
													 
													if ( ($pagenum+1) <= $last) {
												?>
													<a href="<?=site_url() . 'products/'.$param.'/'.(($pagenum)*$row_limit).'/'.$rows?>" class="btn btn-default active">Next</a>
												<?php 	} 
													if ( ($pagenum) != $last) { 
												?>	
													<a href="<?=site_url() . 'products/'.$param.'/'.(($last-1)*$row_limit).'/'.$rows?>" class="btn btn-default active">Last</a>
												<?php
													} 
												?>
													</div>
													<div class="col-md-2 text-right">
														Page <?php echo $pagenum; ?> of <?php echo $last; ?>														
													</div>													
												</div>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</div>					
				</div>
			</div>
		</div>
	</div>
</form>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>	
<script>
$(document).ready(function() {	
	$('#products_mobile1').on('keypress', function(key) {
        if((key.charCode < 48 || key.charCode > 57) && key.charCode != 43 && key.charCode > 31) {
			return false;
		}
    });
	$('#products_mobile2').on('keypress', function(key) {
        if((key.charCode < 48 || key.charCode > 57) && key.charCode != 43 && key.charCode > 31) {
			return false;
		}
    });
});
</script>

<?php
if($mode=="add"){
	?>
<script type="text/javascript">
$('.select').selectpicker('refresh');
$(".datepicker").datepicker({format: 'dd/mm/yyyy'});

	
$(document).ready(function() {
	//alert("inv");
	$("#invent").click(function() {
		if ($(this).is(":checked")) {
			$("#prd_avail").prop("disabled", true);
		} else {
			$("#prd_avail").prop("disabled", false);  
		}
	});
});
function onsize(){
	if($("#prd_size").prop("checked")==true){
		$("#hidp").css("display","none");
		$("#hidqty").css("display","none");
		$("#hiddp").css("display","none");
		$("#tabl").css("display","block");
	} else {
		$("#hidp").css("display","block");
		$("#hidqty").css("display","block");
		$("#hiddp").css("display","block");
		$("#tabl").css("display","none");
	}
}
function onqtycheck(qty){
	//alert(qty);
	q=parseInt(qty);	
	if(q>0){
		$('#prd_avail option[value="Available"]').attr('selected', true);
	} else {
		$('#prd_avail option[value="Out of Stock"]').attr('selected', true);
	}
	$(".select").selectpicker("refresh");
}
function addRow(){
	//alert("New");
	ind=parseInt($("#hidCount").val())+1;
	$("#hidCount").val(ind);
	siz=document.getElementById('size1').innerHTML;
	//alert(u);
	nr="";
	nr=nr+"<tr id='r"+ind+"'>"
	nr=nr+"<td>"+ind+"</td>"
	nr=nr+"<td><input type='text' class='form-control' id='size"+ind+"' name='size"+ind+"' placeholder='Enter Variant' onfocus='search2("+ind+")' /></td>"
	//nr=nr+"<td><select id='size"+ind+"' class='form-control select' name='size"+ind+"' >"+siz+"</select></td>"
	nr=nr+"<td><input type='text' class='form-control' id='qty"+ind+"' name='qty"+ind+"' placeholder='Enter Qty'/></td>"
	nr=nr+"<td><input type='text' class='form-control' id='price"+ind+"' name='price"+ind+"' placeholder='Enter Price'/></td>"
	nr=nr+"<td><input type='text' class='form-control' id='disprice"+ind+"' name='disprice"+ind+"' placeholder='Enter Dis.Price'/></td>"
	nr=nr+"<td style='text-align:right'><button type='button' class='btn btn-success btn-sm btn-condensed' data-toogle='tooltip' onClick='addRow()' title='Edit'><span class='fa fa-plus'></span></button>&nbsp;<button type='button' class='btn btn-danger btn-sm btn-condensed' data-toogle='tooltip' onClick='delRow("+ind+")' title='Remove'><span class='fa fa-times'></span></button></td>"
	nr=nr+"</tr>";
	$("#tbl_dataload").append(nr);
	$('.select').selectpicker();
}
function delRow(ind){
	//alert("del");
	$("#r"+ind).empty();
}
function delRelRow(ind){
	//alert("del");
	$("#rel"+ind).empty();
}
function addRelrow(){
	//alert("test");
	ind=parseInt($("#hidRelCount").val())+1;
	$("#hidRelCount").val(ind);
	nam=$("#prd_relname").val();
	cod=$("#prd_relcode").val();
	id=$("#prd_relid").val();
	nr="";
	nr=nr+"<tr id='rel"+ind+"'>"
	nr=nr+"<td align='center'><b>"+ind+"</b></td>"
	nr=nr+"<td><input type='hidden' id='relid"+ind+"' name='relid"+ind+"' value='"+id+"'><b>"+cod+"</b></td>"
	nr=nr+"<td><b>"+nam+"</b></td>"
	nr=nr+"<td align='center'><button type='button' class='btn btn-danger btn-sm btn-condensed' data-toogle='tooltip' onClick='delRelRow("+ind+")' title='Remove'><span class='fa fa-times'></span></button></td>"
	nr=nr+"</tr>";
	if(ind==1){
		$("#tbl_reldata").empty();
	}
	$("#tbl_reldata").append(nr);
	$("#prd_relname").val('');
}
</script>
<link href="<?php echo base_url(); ?>view/js/autocomplete/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='<?php echo base_url(); ?>view/js/autocomplete/jquery.mcautocomplete.js'></script>
<script type='text/javascript'>
function search(){
	//alert(ind);
	
	function customRenderItem(ul, item) {
		var t = '<span class="mcacCol1">' + item[0] + '</span><span class="mcacCol2">' + item[1] + '</span><span class="mcacCol3">' + item[2] + '</span>',
			result = $('<li class="ui-menu-item" role="menuitem"></li>')
			.data('item.autocomplete', item)
			.append('<a class="ui-corner-all" style="position:relative;" tabindex="-1">' + t + '</a>')
			.appendTo(ul);

		return result;
	}

	var columns = [{name: 'Code', minWidth: '150px'}, {name: 'Products', minWidth:'500px'}],
	colors = [];
		
	cnt=$('#acnt').val();
	for(i=1;i<cnt+1;i++){
		colors.push([$('#code'+i).val(),$('#name'+i).val(),$('#id'+i).val()]);
	}
		
	//[$('#clr1').val(), '#f00'], ['Green', '#0f0'], ['Blue', '#00f'],];

	$("#prd_relname").mcautocomplete({
		showHeader: true,
		columns: columns,
		source: colors,
		select: function(event, ui) {
			this.value = (ui.item ? ui.item[1]: '');
			$('#prd_relcode').val(ui.item ? ui.item[0]: '');
			$('#prd_relid').val(ui.item ? ui.item[2]: '');
			
			return false;
		}
	});
}


function search2(ind){
	//alert(ind);
	
	function customRenderItem(ul, item) {
		var t = '<span class="mcacCol1">' + item[0] + '</span>',
			result = $('<li class="ui-menu-item" role="menuitem"></li>')
			.data('item.autocomplete', item)
			.append('<a class="ui-corner-all" style="position:relative;" tabindex="-1">' + t + '</a>')
			.appendTo(ul);

		return result;
	}

	var columns = [{name: 'Variant Name', minWidth: '200px'}],
	colors = [];
		
	cnt=$('#varCount').val();
	for(i=1;i<cnt+1;i++){
		colors.push([$('#var_name'+i).val()]);
	}
		
	//[$('#clr1').val(), '#f00'], ['Green', '#0f0'], ['Blue', '#00f'],];

	$("#size"+ind).mcautocomplete({
		showHeader: true,
		columns: columns,
		source: colors,
		select: function(event, ui) {
			$('#size'+ind).val(ui.item ? ui.item[0]: '');			
			return false;
		}
	});
}

</script>	
<ul class="breadcrumb">
	<li><a href="#dashboard">Home</a></li>
	<li><a href="#products">Products</a></li>
	<li class="active" >Add New Product</li>
</ul>
<div class="page-content-wrap">
<?php echo validation_errors(); ?>
<?php echo form_open_multipart('save_products'); ?>
<div class="row form-horizontal">
	<div class="col-md-12">
		<div class="page-title">
			<h2><span class="glyphicon glyphicon-shopping-cart"></span> Products<small></small></h2>
		</div>
		<div class="panel panel-primary panel-hidden-controls">
			<div class="panel-heading">
				<h1 class="panel-title">Add New Product</h1>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Product Code</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="prd_code" id="prd_code" placeholder="Enter Product Code "/>
									<input type="hidden" class="form-control" name="category_id" id="category_id" value="<?=$param?>"/>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Product Name</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="prd_name" id="prd_name" placeholder="Enter Product Name"/>
									<input type="hidden" id="prd_id" name="prd_id" class=""  value="<?=$prdid?>"/>
								</div>
							</div>
						</div>
						<div class="col-md-12"><br /></div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Product Brand</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="prd_brand" id="prd_brand" placeholder="Enter Product Brand"/>									
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Hot Products</label>
								<div class="col-md-8 ">
									<input type="radio" name="prd_hot" id="prd_hot" value="1"/><b> Yes</b>
									<input type="radio" name="prd_hot" id="prd_hot" value="0"/><b> No</b>
								</div>
							</div>
						</div>
						
						<div class="col-md-12"><br /></div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Weight</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="prd_weight" id="prd_weight" placeholder="Enter Product Weight"/>									
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Length</label>
								<div class="col-md-8 ">
									<input type="text" class="form-control" name="prd_length" id="prd_length" placeholder="Enter Product Length"/>									
								</div>
							</div>
						</div>
						
						<div class="col-md-12"><br /></div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Height</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="prd_height" id="prd_height" placeholder="Enter Product Height"/>									
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Breadth</label>
								<div class="col-md-8 ">
									<input type="text" class="form-control" name="prd_breadth" id="prd_breadth" placeholder="Enter Product Breadth"/>									
								</div>
							</div>
						</div>
						
						
						<div class="col-md-12"><br /></div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Short Description</label>
								<div class="col-md-8">
									<textarea class="form-control" name="prd_short_description" id="prd_short_description" placeholder="Enter Short Description" cols="8" rows="5" ></textarea>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label"></label>
								<label class="col-md-6 control-label text-left"><input type="checkbox" name="invent" id="invent" value="1"> <b>Inventory</b></label>
							</div>
							<div class="form-group" id="hidp" name="hidp">
								<label class="col-md-4 control-label">Price</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="prd_price" id="prd_price" placeholder="Enter Price "/>
									<input type="hidden" name="hidCount" id="hidCount" value="1" />
								</div>
							</div>
							<div class="form-group" id="hiddp" name="hiddp">
								<label class="col-md-4 control-label">Discounted Price</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="prd_discount" id="prd_discount" placeholder="Enter Discount"/>
								</div>
							</div>				
						</div>												
						<div class="col-md-12"><br /></div>												
						<div class="col-md-6">
							<div class="form-group" id="hidqty" name="hidqty">
								<label class="col-md-4 control-label">Quantity</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="prd_qty" id="prd_qty" placeholder="Enter Quantity " onblur="onqtycheck(this.value)"/>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Availablity </label>
								<div class="col-md-8">
									<select class="form-control select" name="prd_avail" id="prd_avail">
										<option value="Available">Available</option>
										<option value="Out of Stock">Out of Stock</option>
									</select>
								</div>
							</div>	
						</div>
						<div class="col-md-12"><br /></div>
						<div class="col-md-12">		
							<div class="form-group">
								<label class="col-md-12 control-label text-left">Long Description</label>
								<div class="col-md-12">
									<textarea class="summernote" name="prd_long_description" id="prd_long_description" placeholder="Enter Short Description"></textarea>
								</div>
							</div>
						</div>
						<div class="col-md-12"><br /></div>
						<div class="col-md-12">	
							<div class="form-group" >
								<label class="col-md-1 control-label">Photo</label>
								<div class="col-md-11">
									<input type="file" class="fileinput" name="prd_image[]" id="prd_image[]" multiple />
								</div>
							</div>
						</div>
						<div class="col-md-12"><br /></div>
						<div class="col-md-6">		
							<div class="form-group">
								<label class="col-md-4 control-label"></label>
								<label class="col-md-8 control-label text-left"><input type="checkbox" name="prd_size" id="prd_size" onclick="onsize()" value="1"> <b>Variants</b></label>
							</div>						
						</div>
						<div class="col-md-6">
																																			
							<div class="form-group">
								<label class="col-md-4 control-label">Status</label>
								<div class="col-md-8">
									<select class="form-control select" name="prd_status" id="prd_status">
										<option value="Active">Active</option>
										<option value="Inactive">Inactive</option>
									</select>
								</div>
							</div>
						</div>														
					</div>
					<div class="col-md-12"><br /></div>
					<div class="col-md-12" id="tabl" name="tabl" style="display:none">
						<table class="table table-hover table-striped table-actions">
							<thead>
								<tr>
									<th class="table_header text-center">S.No. </th>
									<th class="table_header text-center">Variants</th>
									<th class="table_header text-center">Quantity</th>
									<th class="table_header text-center">Price</th>
									<th class="table_header text-center">Dis. Price</th>
									<th class="table_header text-center" style="width:10%;">Action</th>
								</tr>
							</thead>
							<tbody id="tbl_dataload">
								<? 
								$i=1;
								?>
								<tr>
									<td><?=$i?></td>
									<td>
										<input type="text" class="form-control" id="size<?=$i?>" name="size<?=$i?>" placeholder="Enter Variant" onfocus="search2(<?=$i?>)" />
										<!--<select class="form-control select" name="size<?=$i?>" id="size<?=$i?>">
										<?
										foreach($size as $sqs){
										?>	
											<option value="<?=$sqs['size_id']?>"><?=$sqs['size_name']?></option>
										<?
										}
										?>		
										</select> --> 
									</td>
									<td><input type="text" class="form-control" id="qty<?=$i?>" name="qty<?=$i?>" placeholder="Enter Qty"/></td>
									<td><input type="text" class="form-control" id="price<?=$i?>" name="price<?=$i?>" placeholder="Enter Price"/></td>
									<td><input type="text" class="form-control" id="disprice<?=$i?>" name="disprice<?=$i?>" placeholder="Enter Dis.Price"/></td>
									<td style="text-align:right">
										<button type="button" class="btn btn-success btn-sm btn-condensed" data-toogle="tooltip" onClick="addRow()" title="Edit"><span class="fa fa-plus"></span></button>
										<button type="button" class="btn btn-danger btn-sm btn-condensed" data-toogle="tooltip" onClick="delRow()" title="Remove"><span class="fa fa-times"></span></button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-12"><br /></div>
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="form-group">
								<label class="col-md-2 control-label">Related Products</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="prd_relname" id="prd_relname" placeholder="Enter Product Code" onfocus="search()"/>
									<input type="hidden" class="form-control" name="prd_relcode" id="prd_relcode" placeholder="Enter Product Code" />
									<input type="hidden" class="form-control" name="prd_relid" id="prd_relid" />
								</div>
								<div class="col-md-2">
									<button type="button" class="btn btn-success btn-condensed " data-toogle="tooltip" title="Add" onclick="addRelrow()"><span class="fa fa-plus"></span> Add</button>
								</div>
							</div>
						</div>
						
						<div class="col-md-12" id="tabla" name="tabla"><br>
							<div class="table-responsive">
								<table class="table table-hover table-striped table-actions" >
									<thead>
										<tr>
											<th class="table_header text-center">S.No. <input type="hidden" name="hidRelCount" id="hidRelCount" value="0" /></th>
											<th class="table_header text-center"> Product Code &nbsp;&nbsp; </th>
											<th class="table_header text-center">Product Name  &nbsp;&nbsp;</th>
											<th class="table_header text-center">Action</th>
										</tr>
									</thead>
									<tbody id="tbl_reldata">
										<tr>
											<td colspan="4" align="center"><b>No Records Found</b></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		
		<div class="panel panel-primary panel-hidden-controls">
			<div class="panel-heading">
				<h1 class="panel-title">Product Parameters</h1>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Field Tag</label>
								<div class="col-md-8">
									<select class="form-control select" name="prd_catid" id="prd_catid" disabled="disabled">
										<option value="">Select Field Tag</option>
										<?
										foreach($tags as $p){
											$sel=""; if($p['ftags_id']==$tag_id){ $sel="selected"; }
										?>
										<option value="<?=$p['ftags_id']?>" <?=$sel?>><?=$p['ftags_name']?></option>
										<?
										}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12"><br /></div>
						<?
						$i=0;
						$paramID="";
						foreach($params as $par){
							$i++;
							$paramID=$paramID.','.$par['fparam_id'];
						?>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label"><?=$par['fparam_name']?></label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="param_value<?=$par['fparam_id']?>" id="param_value<?=$par['fparam_id']?>" placeholder="Enter <?=$par['fparam_name']?> Value"/>									
								</div>
							</div>
						</div>
						<?
							if($i%2==0){
						?>
						<div class="col-md-12"><br /></div>
						<?
							}
						}
						$paramID=substr($paramID,1);
						?>
						<input type="hidden" name="paramID" id="paramID" value="<?=$paramID?>" />
					</div>
				</div>
			</div>
		</div>	
		<div style="text-align:center">
			<button type="submit" class="btn btn-warning" name="btn_update" id="btn_update"><span class="fa fa-check-circle"></span> Save</button>
			<a href="<?php echo site_url();?>listproducts/<?=$param?>" class="btn btn-link "><span class="fa fa-arrow-left"></span>Back</button>  
		</div>	
	</div>
	</form>
</div>
<div style="clear:both;"></div>
<? $cnt=count($autoc);?>
<input type="hidden" name="acnt" id="acnt" value="<?=$cnt?>">
<? 
$i=0;
foreach($autoc as $ac){
	$i++;
	?>
<input type="hidden" name="code<?=$i?>" id="code<?=$i?>" value="<?=$ac['prd_code']?>">
<input type="hidden" name="name<?=$i?>" id="name<?=$i?>" value="<?=$ac['prd_name']?>">
<input type="hidden" name="id<?=$i?>" id="id<?=$i?>" value="<?=$ac['prd_id']?>">
<? 
}
?>

<? 
$cnt2=count($variants);
?>
<input type="hidden" name="varCount" id="varCount" value="<?=$cnt2?>">
<? 
$i=0;
foreach($variants as $var){
	$i++;
	?>
<input type="hidden" name="var_name<?=$i?>" id="var_name<?=$i?>" value="<?=$var['siz_size']?>">
<? 
}
?>


<? 
} 
?>
<?
if($mode=="edit"){
	//print_r($products_edit);
	?>
<script type="text/javascript">
$('.select').selectpicker('refresh');
$(".datepicker").datepicker({format: 'dd/mm/yyyy'});

	
$(document).ready(function() {
	//alert("inv");
	$("#invent").click(function() {
		if ($(this).is(":checked")) {
			$("#prd_avail").prop("disabled", true);
		} else {
			$("#prd_avail").prop("disabled", false);  
		}
	});
});
function onsize(){
	if($("#prd_size").prop("checked")==true){
		$("#hidp").css("display","none");
		$("#hidqty").css("display","none");
		$("#hiddp").css("display","none");
		$("#tabl").css("display","block");
	} else {
		$("#hidp").css("display","block");
		$("#hidqty").css("display","block");
		$("#hiddp").css("display","block");
		$("#tabl").css("display","none");
	}
}
function onqtycheck(qty){
	//alert(qty);
	q=parseInt(qty);	
	if(q>0){
		$('#prd_avail option[value="Available"]').attr('selected', true);
	} else {
		$('#prd_avail option[value="Out of Stock"]').attr('selected', true);
	}
	$(".select").selectpicker("refresh");
}
function addRow(){
	//alert("New");
	ind=parseInt($("#hidCount").val())+1;
	$("#hidCount").val(ind);
	siz=document.getElementById('size1').innerHTML;
	//alert(u);
	nr="";
	nr=nr+"<tr id='r"+ind+"'>"
	nr=nr+"<td>"+ind+"</td>"
	nr=nr+"<td><input type='text' class='form-control' id='size"+ind+"' name='size"+ind+"' placeholder='Enter Variant' onfocus='search2("+ind+")' /></td>"
	//nr=nr+"<td><select id='size"+ind+"' class='form-control select' name='size"+ind+"' >"+siz+"</select></td>"
	nr=nr+"<td><input type='text' class='form-control' id='balqty"+ind+"' name='balqty"+ind+"' placeholder='Enter Qty'/></td>"
	nr=nr+"<td><input type='text' class='form-control' id='price"+ind+"' name='price"+ind+"' placeholder='Enter Price'/></td>"
	nr=nr+"<td><input type='text' class='form-control' id='disprice"+ind+"' name='disprice"+ind+"' placeholder='Enter Dis.Price'/></td>"
	nr=nr+"<td style='text-align:right'><button type='button' class='btn btn-success btn-sm btn-condensed' data-toogle='tooltip' onClick='addRow()' title='Edit'><span class='fa fa-plus'></span></button>&nbsp;<button type='button' class='btn btn-danger btn-sm btn-condensed' data-toogle='tooltip' onClick='delRow("+ind+")' title='Remove'><span class='fa fa-times'></span></button></td>"
	nr=nr+"</tr>";
	$("#tbl_dataload").append(nr);
	$(".select").selectpicker("refresh");
}
function delRow(ind){
	//alert("del");
	$("#r"+ind).empty();
}
function delRelRow(ind){
	//alert("del");
	$("#rel"+ind).empty();
}
function addRelrow(){
	//alert("test");
	ind=parseInt($("#hidRelCount").val())+1;
	$("#hidRelCount").val(ind);
	nam=$("#prd_relname").val();
	cod=$("#prd_relcode").val();
	id=$("#prd_relid").val();
	nr="";
	nr=nr+"<tr id='rel"+ind+"'>"
	nr=nr+"<td align='center'><b>"+ind+"</b></td>"
	nr=nr+"<td><input type='hidden' id='relid"+ind+"' name='relid"+ind+"' value='"+id+"'><b>"+cod+"</b></td>"
	nr=nr+"<td><b>"+nam+"</b></td>"
	nr=nr+"<td align='center'><button type='button' class='btn btn-danger btn-sm btn-condensed' data-toogle='tooltip' onClick='delRelRow("+ind+")' title='Remove'><span class='fa fa-times'></span></button></td>"
	nr=nr+"</tr>";
	if(ind==1){
		$("#tbl_reldata").empty();
	}
	$("#tbl_reldata").append(nr);
	$("#prd_relname").val('');
}
</script>
<link href="<?php echo base_url(); ?>view/js/autocomplete/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='<?php echo base_url(); ?>view/js/autocomplete/jquery.mcautocomplete.js'></script>
<script type='text/javascript'>
function search(){
	//alert(ind);
	
	function customRenderItem(ul, item) {
		var t = '<span class="mcacCol1">' + item[0] + '</span><span class="mcacCol2">' + item[1] + '</span><span class="mcacCol3">' + item[2] + '</span>',
			result = $('<li class="ui-menu-item" role="menuitem"></li>')
			.data('item.autocomplete', item)
			.append('<a class="ui-corner-all" style="position:relative;" tabindex="-1">' + t + '</a>')
			.appendTo(ul);

		return result;
	}

	var columns = [{name: 'Code', minWidth: '100px'}, {name: 'Products', minWidth:'250px'}],
	colors = [];
	
	cnt=$('#acnt').val();
	for(i=1;i<cnt+1;i++){
		colors.push([$('#code'+i).val(),$('#name'+i).val(),$('#id'+i).val()]);
	}
	
	//[$('#clr1').val(), '#f00'], ['Green', '#0f0'], ['Blue', '#00f'],];

	$("#prd_relname").mcautocomplete({
		showHeader: true,
		columns: columns,
		source: colors,
		select: function(event, ui) {
			this.value = (ui.item ? ui.item[1]: '');
			$('#prd_relcode').val(ui.item ? ui.item[0]: '');
			$('#prd_relid').val(ui.item ? ui.item[2]: '');
			
			return false;
		}
	});		
}

function search2(ind){
	//alert(ind);
	
	function customRenderItem(ul, item) {
		var t = '<span class="mcacCol1">' + item[0] + '</span>',
			result = $('<li class="ui-menu-item" role="menuitem"></li>')
			.data('item.autocomplete', item)
			.append('<a class="ui-corner-all" style="position:relative;" tabindex="-1">' + t + '</a>')
			.appendTo(ul);

		return result;
	}

	var columns = [{name: 'Variant Name', minWidth: '200px'}],
	colors = [];
		
	cnt=$('#varCount').val();
	for(i=1;i<cnt+1;i++){
		colors.push([$('#var_name'+i).val()]);
	}
		
	//[$('#clr1').val(), '#f00'], ['Green', '#0f0'], ['Blue', '#00f'],];

	$("#size"+ind).mcautocomplete({
		showHeader: true,
		columns: columns,
		source: colors,
		select: function(event, ui) {
			$('#size'+ind).val(ui.item ? ui.item[0]: '');			
			return false;
		}
	});
}
</script>
<? 
if($products_edit['prd_size']=="1"){ 
	$chks="checked"; 
	$disp="none";
	$disptab="block";
} else { 
	$chks="";
	$disp="block";
	$disptab="none";
}

$sizcnt=count($products_size);
$sizecnt=$sizcnt;
if($sizcnt==0){ $sizcnt=1; }
?>
<ul class="breadcrumb">
    <li><a href="#dashboard">Home</a></li>
    <li><a href="#products">Products</a></li>
    <li class="active" >Edit Product</li>
</ul>
<div class="page-content-wrap">
    <!--<form class="form-horizontal" action=" " method="post" enctype="multipart/form-data" name="jvalidate"id="jvalidate" > -->
    <?php echo validation_errors(); ?>
    <?php echo form_open_multipart('update_products') ?>
    <?
        //print_r($bra_edit);
        ?>
    <div class="row form-horizontal">
        <div class="col-md-12">
            <div class="page-title">
                <h2><span class="glyphicon glyphicon-shopping-cart"></span> Products<small></small></h2>
            </div>
            <div class="panel panel-primary panel-hidden-controls">
                <div class="panel-heading">
                    <h1 class="panel-title">Edit Product</h1>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label">Product Code</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="prd_code" id="prd_code" placeholder="Enter Product Code " value="<?=$products_edit['prd_code']?>"/>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label">Product Name</label>
									<div class="col-md-8">
										<input type="hidden"id="prd_catid" name="prd_catid" class=""  value="<?=$products_edit['prd_catid']?>"/>
                                        <input type="text" class="form-control" name="prd_name" id="prd_name" placeholder="Enter Product Name" value="<?=$products_edit['prd_name']?>" />
                                        <input type="hidden" id="prd_id" name="prd_id" class=""  value="<?=$products_edit['prd_id']?>"/>
										<input type="hidden" id="category_id" name="category_id" value="<?=$products_edit['prd_catid']?>"/>
									</div>
								</div>
							</div>
							<div class="col-md-12"><br /></div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label">Product Brand</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="prd_brand" id="prd_brand" placeholder="Enter Product Brand" value="<?=$products_edit['prd_brand']?>" />									
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label">Hot Products</label>
									<div class="col-md-8 ">
										<? if($products_edit['prd_hotproduct']=="1"){ $chk="checked";$chkg=""; } else { $chk="";$chkg="checked";}?>
                                        <input type="radio" name="prd_hot" id="prd_hot" value="1" <?=$chk?>/><b> Yes</b>
                                        <input type="radio" name="prd_hot" id="prd_hot" value="0" <?=$chkg?>/><b> No</b>
									</div>
								</div>
							</div>
							
							<div class="col-md-12"><br /></div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Weight</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="prd_weight" id="prd_weight" placeholder="Enter Product Weight" value="<?=$products_edit['prd_weight']?>"/>									
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Length</label>
								<div class="col-md-8 ">
									<input type="text" class="form-control" name="prd_length" id="prd_length" placeholder="Enter Product Length" value="<?=$products_edit['prd_length']?>"/>									
								</div>
							</div>
						</div>
						
						<div class="col-md-12"><br /></div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Height</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="prd_height" id="prd_height" placeholder="Enter Product Height" value="<?=$products_edit['prd_height']?>"/>									
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Breadth</label>
								<div class="col-md-8 ">
									<input type="text" class="form-control" name="prd_breadth" id="prd_breadth" placeholder="Enter Product Breadth" value="<?=$products_edit['prd_breadth']?>"/>									
								</div>
							</div>
						</div>
						
							
							<div class="col-md-12"><br /></div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label">Short Description</label>
									<div class="col-md-8">
										<textarea class="form-control" name="prd_short_description" id="prd_short_description" placeholder="Enter Short Description" cols="8" rows="5" ><?=$products_edit['prd_short_description']?></textarea>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									  <? if($products_edit['prd_inventory']=="1"){ $chk="checked"; $dsb="disabled"; } else { $chk=""; $dsb=""; }?>
									<label class="col-md-4 control-label"></label>
                                    <label class="control-label"><input type="checkbox" name="invent" id="invent" value="1" <?=$chk?>> <b>Inventory</b></label>
								</div>
								<div class="form-group" id="hidp" name="hidp" style="display:<?=$disp?>">
									<label class="col-md-4 control-label">Price</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="prd_price" id="prd_price" placeholder="Enter Price " value="<?=$products_size[0]['siz_price']?>"/>
									</div>
								</div>
								<div class="form-group" id="hiddp" name="hiddp" style="display:<?=$disp?>">
									<label class="col-md-4 control-label">Discounted Price</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="prd_discount" id="prd_discount" placeholder="Enter Discount" value="<?=$products_size[0]['siz_discount']?>"/>
									</div>
								</div>				
							</div>							
							<div class="col-md-12"><br /></div>
							<div class="col-md-6">
								<div class="form-group" id="hidqty" name="hidqty" style="display:<?=$disp?>">
									<label class="col-md-4 control-label">Quantity</label>
									<div class="col-md-8">
										 <input type="text" class="form-control" name="prd_balqty" id="prd_balqty" placeholder="Enter Available Quantity " onblur="onqtycheck(this.value)" value="<?=$products_size[0]['siz_balanceqty']?>"/>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label">Availablity </label>
									<div class="col-md-8">
										<select class="form-control select" name="prd_avail" id="prd_avail" <?=$dsb?>>
											<option value="Available">Available</option>
											<option value="Out of Stock">Out of Stock</option>
										</select>
										 <script>
                                            setSelectedIndex(document.getElementById('prd_avail'),"<?=$products_edit['prd_avail']?>");
                                        </script>
									</div>
								</div>	
							</div>
							<div class="col-md-12"><br /></div>
							<div class="col-md-12">		
								<div class="form-group">
									<label class="col-md-12 control-label text-left">Long Description</label>
									<div class="col-md-12">
										<textarea class="summernote" name="prd_long_description" id="prd_long_description" placeholder="Enter Short Description"><?=$products_edit['prd_long_description']?></textarea>
									</div>
								</div>
							</div>
							<div class="col-md-12"><br /></div>
							<div class="col-md-12">	
								<div class="form-group" >
									<label class="col-md-1 control-label text-left">Photo</label>
									<div class="col-md-11 text-left">
										<input type="file" class="fileinput" name="prd_image[]" id="prd_image[]" multiple /><br/>										
									</div>
									<? 
									foreach($products_image as $img) {
									?>
									<div class="col-md-2 text-center" id="hidimg<?=$img['att_id']?>">
										<img src="<? echo '../images/products/t/'.$img['att_name'];?>" style="height:150px; width:125px; padding:5px"/> 
										<a href="javascript:removeImage(<?=$img['att_id']?>)" >Remove</a>
									</div>
									<?
									}
									?>
								</div>
							</div>
							<div class="col-md-12"><br /></div>
							<div class="col-md-6">		
								<div class="form-group">
									<input type="hidden" name="hidCount" id="hidCount" value="<?=$sizcnt?>" />
									<label class="col-md-4 control-label"></label>
                                    <label class="col-md-8 control-label text-left">
										<input type="checkbox" name="prd_size" id="prd_size" value="1" onclick="onsize()" <?=$chks?>> <b>Variants</b>
									</label>
								</div>						
							</div>
							<div class="col-md-6">
																																				
								<div class="form-group">
									<label class="col-md-4 control-label">Status</label>
									<div class="col-md-8">
										<select class="form-control select" name="prd_status" id="prd_status">
											<option value="Active">Active</option>
											<option value="Inactive">Inactive</option>
										</select>
										<script>
                                            setSelectedIndex(document.getElementById('prd_status'),"<?=$products_edit['prd_status']?>");
                                        </script>
									</div>
								</div>
							</div>																				
							<div class="col-md-12"><br /></div>
							<div class="col-md-12" id="tabl" name="tabl" style="display:<?=$disptab?>">
								<table class="table table-hover table-striped table-actions" >
									<thead>
										<tr>
											<th class="table_header text-center">S.No.</th>
											<th class="table_header text-center">Variants</th>
											<th class="table_header text-center">Available Quantity</th>
											<th class="table_header text-center">Price</th>
											<th class="table_header text-center">Dis. Price</th>
											<th class="table_header text-center" style="width:10%;">Action</th>
										</tr>
									</thead>
									<tbody id="tbl_dataload">
									<? 
										if($sizecnt>0){
											$i=0; 
											//print_r($products_size);
											foreach($products_size as $ps){
												$i++;
												?>
										<tr id="r<?=$i?>">
											<td align="center"><?=$i?></td>
											<td align="center">                                           		<input type="text" class="form-control" id="size<?=$i?>" name="size<?=$i?>" placeholder="Enter Variant" value="<?=$ps['siz_size']?>" onfocus="search2(<?=$i?>)" /> 
												<!--<select class="form-control select" name="size<?=$i?>" id="size<?=$i?>">
													<?
														foreach($size as $sqs){
															if($sqs['size_id']==$ps['siz_size']){ $sel="selected";  } else { $sel=""; }
														?>	
													<option value="<?=$sqs['size_id']?>" <?=$sel?>><?=$sqs['size_name']?></option>
													<?
														}
														?>		
												</select> -->
											</td>
											
											<td><input type="text" class="form-control" id="balqty<?=$i?>" name="balqty<?=$i?>" placeholder="Enter Available Qty" value="<?=$ps['siz_balanceqty']?>"/></td>
											<td><input type="text" class="form-control" id="price<?=$i?>" name="price<?=$i?>" placeholder="Enter Price" value="<?=$ps['siz_price']?>"/></td>
											<td><input type="text" class="form-control" id="disprice<?=$i?>" name="disprice<?=$i?>" placeholder="Enter Dis.Price" value="<?=$ps['siz_discount']?>"/></td>
											<td align="center">
												<button type="button" class="btn btn-success btn-sm btn-condensed" data-toogle="tooltip" onClick="addRow()" title="Edit"><span class="fa fa-plus"></span></button>
												<button type="button" class="btn btn-danger btn-sm btn-condensed" data-toogle="tooltip" onClick="delRow(<?=$i?>)" title="Remove"><span class="fa fa-times"></span></button>
											</td>
										</tr>
										<? 	
											} 
										} else {
											$i=1;
											?>
										<tr>
											<td align="center"><?=$i?></td>
											<td align="center">
												<input type="text" class="form-control" id="size<?=$i?>" name="size<?=$i?>" placeholder="Enter Size" value="" onfocus="search2(<?=$i?>)"  /> 
												<!--<select class="form-control select" name="size<?=$i?>" id="size<?=$i?>">
												<?
												foreach($size as $sqs){
													?>	
													<option value="<?=$sqs['size_id']?>"><?=$sqs['size_name']?></option>
												<?
												}
												?>		
												</select> -->
											</td>                                        
											<td><input type="text" class="form-control" id="balqty<?=$i?>" name="balqty<?=$i?>" placeholder="Enter Available Qty" /></td>
											<td><input type="text" class="form-control" id="price<?=$i?>" name="price<?=$i?>" placeholder="Enter Price"/></td>
											<td><input type="text" class="form-control" id="disprice<?=$i?>" name="disprice<?=$i?>" placeholder="Enter Dis.Price"/></td>
											<td align="center">
												<button type="button" class="btn btn-success btn-sm btn-condensed" data-toogle="tooltip" onClick="addRow()" title="Edit"><span class="fa fa-plus"></span></button>
												<button type="button" class="btn btn-danger btn-sm btn-condensed" data-toogle="tooltip" onClick="delRow()" title="Remove"><span class="fa fa-times"></span></button>
											</td>
										</tr>
										<? 
										}
											?>
									</tbody>
								</table>
							 </div>
							 <div class="col-md-12"><br /></div>
							 <div class="form-group col-md-12">
                            	<label class="col-md-2 control-label">Related Products</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="prd_relname" id="prd_relname" placeholder="Enter Product Code" onfocus="search()"/>
                                    <input type="hidden" class="form-control" name="prd_relcode" id="prd_relcode" placeholder="Enter Product Code" />
                                    <input type="hidden" class="form-control" name="prd_relid" id="prd_relid" />
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-success btn-condensed " data-toogle="tooltip" title="Add" onclick="addRelrow()"><span class="fa fa-plus"></span> Add</button>
                                </div>
                            </div>
							<div class="col-md-12"><br /></div>
                            <div id="tabla" name="tabla" class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped table-actions" >
                                        <thead>
                                            <? $relcnt=count($products_relat);?>
                                            <tr>
                                                <th class="table_header text-center">S.No. <input type="hidden" name="hidRelCount" id="hidRelCount" value="<?=$relcnt?>" /></th>
                                                <th class="table_header text-center">Product Code</th>
                                                <th class="table_header text-center">Product Name</th>
                                                <th class="table_header text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbl_reldata">
                                            <? if($relcnt>0){
                                                $j=0;
                                                foreach($products_relat as $pr){
                                                $j++;
                                                ?>
                                            <tr id="rel<?=$j?>">
                                                <td align="center"><b><?=$j?></b></td>
                                                <td align="center"><input type='hidden' id='relid<?=$j?>' name='relid<?=$j?>' value='<?=$pr['rp_relprdid']?>'><b><?=$products_relatsub[$j]['prd_code']?></b></td>
                                                <td><b><?=$products_relatsub[$j]['prd_name']?></b></td>
                                                <td align="center"><button type='button' class='btn btn-danger btn-sm btn-condensed' data-toogle='tooltip' onClick='delRelRow(<?=$j?>)' title='Remove'><span class='fa fa-times'></span></button></td>
                                            </tr>
                                            <?} }else {?>
                                            <tr>
                                                <td colspan="4" align="center"><b>No Records Found</b></td>
                                            </tr>
                                            <? }?>
                                        </tbody>
                                    </table>
                                </div>                            
							</div>																					                        </div>
                    </div>
                </div>
				
				<div class="col-md-12"><br /></div>
            </div>
			<div class="panel panel-primary panel-hidden-controls">
				<div class="panel-heading">
					<h1 class="panel-title">Product Parameters</h1>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label">Field Tag</label>
									<div class="col-md-8">
										<select class="form-control select" name="prd_catid" id="prd_catid" disabled="disabled">
											<option value="">Select Field Tag</option>
											<?
											foreach($tags as $p){
												$sel=""; if($p['ftags_id']==$tag_id){ $sel="selected"; }
											?>
											<option value="<?=$p['ftags_id']?>" <?=$sel?>><?=$p['ftags_name']?></option>
											<?
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-12"><br /></div>
							<?
							$i=0;
							$paramID="";
							//print_r($params);
							foreach($params as $par){
								$i++;
								$paramID=$paramID.','.$par['fparam_id'];
							?>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label"><?=$par['fparam_name']?></label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="param_value<?=$par['fparam_id']?>" id="param_value<?=$par['fparam_id']?>" placeholder="Enter <?=$par['fparam_name']?> Value" value="<?=$par['value']?>"/>									
									</div>
								</div>
							</div>
							<?
								if($i%2==0){
							?>
							<div class="col-md-12"><br /></div>
							<?
								}
							}
							$paramID=substr($paramID,1);
							?>
							<input type="hidden" name="paramID" id="paramID" value="<?=$paramID?>" />
						</div>
					</div>
				</div>
			</div>	
			<div class="col-md-12"><br /></div>
			<div style="text-align:center">
				<button type="submit" class="btn btn-warning" name="btn_update" id="btn_update"><span class="fa fa-check-circle"></span> Save</button>
				<a href="<?php echo site_url();?>listproducts/<?=$products_edit['prd_catid']?>" class="btn btn-link "><span class="fa fa-arrow-left"></span>Back</a> 
			</div>
			<div class="col-md-12"><br /></div>
        </div>
    </div>
    </form>
</div>
<div style="clear:both;"></div>
<? $cnt=count($autoc);?>
<input type="hidden" name="acnt" id="acnt" value="<?=$cnt?>">
<? 
	$i=0;
	foreach($autoc as $ac){
	$i++;
	?>
<input type="hidden" name="code<?=$i?>" id="code<?=$i?>" value="<?=$ac['prd_code']?>">
<input type="hidden" name="name<?=$i?>" id="name<?=$i?>" value="<?=$ac['prd_name']?>">
<input type="hidden" name="id<?=$i?>" id="id<?=$i?>" value="<?=$ac['prd_id']?>">
<? }?>

<? 
$cnt2=count($variants);
?>
<input type="hidden" name="varCount" id="varCount" value="<?=$cnt2?>">
<? 
$i=0;
foreach($variants as $var){
	$i++;
	?>
<input type="hidden" name="var_name<?=$i?>" id="var_name<?=$i?>" value="<?=$var['siz_size']?>">
<? 
}
?>

<script>
function removeImage(id){
	var r = confirm("Do you want to remove this Image..?");
	if (r == true) {
		str="id="+id;	
		var prd_id=$('#prd_id').val();
		url="<?php echo site_url(); ?>products_attimageremove";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				//alert(data);
				if(data==1){
					alert("Image Removed Successfully.");					
					window.location.href="<?php echo site_url();?>editproducts/"+prd_id;	
				}
			}		
		});			
	}
}
</script>
<? 
}

include "footer.php"; 
?>
