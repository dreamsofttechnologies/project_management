<?php
$main="transaction";
$sub="project";

include "header.php";
if($mode=="list"){
	$title="<strong>Project List</strong>";
	//include "titlebar.php";
	?>
<script>
function removeProject(id){
	var r = confirm("Do you want to remove this Project..?");
	if (r == true) {
		str="id="+id;	
		//alert(id);
		url="<?php echo site_url(); ?>delete_project";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				//alert(data);
				if(data=='deleted'){
					alert("Project Removed Successfully.");
					window.location.href="<?php echo site_url(); ?>project";						
				}
			}		
		});			
	}
}
</script>
<div id="project">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Project</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->
		<?php
		$action="project";
		?>
		<?php echo validation_errors(); ?>
		<?php echo form_open($action) ?>
			<div class="row" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<div class="col-md-6">
							<h2>Project<small></small></h2>
						</div>
						<div class="col-md-6 text-right">
							<a href="<?php echo site_url();?>addproject" class="btn btn-info btn-condensed"><span class="fa fa-plus"></span> Add New Project</a> </div>								
					</div>
					<!-- START DEFAULT DATATABLE -->
					<div class="panel panel-default">
						<div class="panel-heading">                                                               
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table datatable">
									<thead>
										<tr>
											<th class="text-center">S.No</th>
											<th class="text-center">Project Name</th>
											<th class="text-center">Client</th>
											<th class="text-center">Type</th>
											<th class="text-center">Due Date</th>
											<th class="text-center">Status</th>
											<th class="text-center">Action</th>
										</tr>
									</thead>
									<tbody>
									<?php
									//print_r($projectList);
									$len=count($projectList);
									if($len>0){
										$i=0;
										foreach($projectList as $row){
											$i++;
									?>
										<tr>
											<td align="center"><?=$i?></td>
											<td align="left"><?=$row['project_name']?></td>
											<td align="left"><?=$row['customer_name']?></td>
											<td align="center"><?=$row['project_type_name']?></td>
											<td align="center"><?=date("d/m/Y",strtotime($row['project_due_date']))?></td>
											<td align="center"><?=$row['project_status']?></td>
											<td align="center">
																										
												<div>
													<a href="<?php echo site_url();?>quotation/<?=$row['project_id']?>" class="btn btn-info btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Project Quote"><span class="fa fa-file-o"></span></span></a>                                        
													<a href="<?php echo site_url();?>invoice/<?=$row['project_id']?>" class="btn btn-info btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Project Invoice"><span class="fa fa-file-text-o"></span></a>                                                          
													<a href="<?php echo site_url();?>projecttask/<?=$row['project_id']?>" class="btn btn-info btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Project Task"><span class="glyphicon glyphicon-tasks"></span></a> 
													<a href="<?php echo site_url();?>budget/<?=$row['project_id']?>" class="btn btn-info btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Project Budget"><span class="fa fa-money"></span></a> 	
												</div>
												<div style="padding-top:5px;">
													<a href="<?php echo site_url();?>team/<?=$row['project_id']?>" class="btn btn-info btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Project Team"><span class="fa fa-users"></span></a>
													<a href="<?php echo site_url();?>projectcostsheet/<?=$row['project_id']?>" class="btn btn-info btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Project Cost Sheet"><span class="fa fa-file-text"></span></a>
													<a href="<?php echo site_url();?>editproject/<?=$row['project_id']?>" class="btn btn-success btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="fa fa-pencil"></span></a> 
													<a href="javascript:removeProject(<?=$row['project_id']?>)" class="btn btn-danger btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Delete"><span class="fa fa-times"></span></a> 	
												</div>
											</td>
										</tr>
									<?php
										}
									} else {
									?>
										<tr>
											<td align="center" ></td>
										</tr>
									<?php
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END DEFAULT DATATABLE -->
				</div>
    		</div>
    	</div>
    </form>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>	
<?php
if($mode=="add"){
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>project">Project</a></li>
	<li class="active" >Add New Project</li>
</ul>
<div class="page-content-wrap" id="project">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('save_project',$attr); ?>
		<div class="row form-horizontal" style="padding-top:10px;">
			<div class="col-md-12">
				<div class="page-title">
					<div class="page-title">
                        <div class="col-md-6">
                            <h2>Add New Project<small></small></h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?=site_url()?>project" class="btn btn-info btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
                        </div>                   
                    </div>
				</div>
				<div class="panel panel-hidden-controls">
					<!-- <div class="panel-heading">
						<h1 class="panel-title">Add New Project</h1>
					</div> -->
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Project Name</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="project_name" id="project_name" placeholder="Enter Your Project Name"/>											
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Client</label>
										<div class="col-md-8">
											<select class="form-control select" data-live-search="true" name="project_customer_id" id="project_customer_id">
												<option value="">Select Client</option>
												<?php
												foreach($clientList as $client){
												?>
												<option value="<?=$client['customer_id']?>"><?=$client['customer_name']?></option>
												<?php
												}
												?>												
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Type</label>
										<div class="col-md-8">
											<select class="form-control select" name="project_type" id="project_type">
												<option value="">Select Type</option>
												<?php
												foreach($typeList as $type){
												?>
												<option value="<?=$type['project_type_id']?>"><?=$type['project_type_name']?></option>
												<?php
												}
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Parent Project</label>
										<div class="col-md-8">
											<select class="form-control select" data-live-search="true" name="project_parent" id="project_parent">
												<option value="">Select Parent</option>
												<?php
												foreach($parentList as $parent){
												?>
												<option value="<?=$parent['project_id']?>"><?=$parent['project_name']?></option>
												<?php
												}
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Start Date</label>
										<div class="col-md-8">
											<div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" class="form-control datepicker" name="project_start_date" id="project_start_date" placeholder="Enter Your Designation" value="<?=date("d/m/Y")?>"/>                                           
                                            </div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Due Date</label>
										<div class="col-md-8">
											<div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" class="form-control datepicker" name="project_due_date" id="project_due_date" placeholder="Enter Your Designation" value="<?=date("d/m/Y")?>" />                                           
                                            </div>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Description</label>
										<div class="col-md-8">
											<textarea placeholder="Enter Description" class="form-control" name="project_description" id="project_description" rows="3"></textarea>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Status</label>
										<div class="col-md-8">
											<select class="form-control select" name="project_status" id="project_status">
												<option value="Active">Active</option>
												<option value="Inactive">Inactive</option>
											</select>
										</div>
									</div>
								</div>
                          	</div>
                            <div class="col-md-12"><br></div>
							<div class="col-md-12">      							    
								<h3>Delivery Details</h3>                          	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Delivery Date</label>
										<div class="col-md-8">
										<?php
										$delivery="";										
										?>
											<div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" class="form-control datepicker" name="project_delivery_date" id="project_delivery_date" placeholder="Enter Delivery Date" value="" />                                           
                                            </div>
										</div>
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Delivery Remarks</label>
										<div class="col-md-8">
											<textarea placeholder="Enter Remarks" class="form-control" name="project_delivery_description" id="project_delivery_description" rows="3"><?=$project_delivery_description?></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12"><br></div>
							<div class="col-md-12"> 							   							   								<div class="col-md-12" style="text-align:center;">                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button>                                    <a href="<?=site_url()?>project" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a>                                
								</div>
							</div>
						</div>
					</div>
				</div>											                                
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
} 
?>

<?php
if($mode=="edit"){
	//print_r($projectDets);
	extract($projectDets);
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>project">Project</a></li>
	<li class="active">Edit Project</li>
</ul>
<div class="page-content-wrap">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('update_project',$attr); ?>
		<div class="row form-horizontal">
			<div class="col-md-12">
				<div class="page-title">
					<h2>Project<small></small></h2>
				</div>
				<div class="panel panel-primary panel-hidden-controls">
					<div class="panel-heading">
						<h1 class="panel-title">Edit Project</h1>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Project Name</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="project_name" id="project_name" placeholder="Enter Your Project Name" value="<?=$project_name?>" />											
											<input type="hidden" class="form-control" name="project_id" id="project_id" value="<?=$project_id?>" />											
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Client</label>
										<div class="col-md-8">
											<select class="form-control select" data-live-search="true" name="project_customer_id" id="project_customer_id">
												<option value="">Select Client</option>
												<?php
												foreach($clientList as $client){
												$sel=""; 
												if($client['customer_id']==$project_customer_id){
													$sel="selected";												
												}
												?>
												<option value="<?=$client['customer_id']?>" <?=$sel?>><?=$client['customer_name']?></option>
												<?php
												}
												?>												
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Type</label>
										<div class="col-md-8">
											<select class="form-control select" name="project_type" id="project_type">
												<option value="">Select Type</option>
												<?php
												foreach($typeList as $type){
													$sel=""; 
													if($type['project_type_id']==$project_type){
														$sel="selected";												
													}
												?>
												<option value="<?=$type['project_type_id']?>" <?=$sel?>><?=$type['project_type_name']?></option>
												<?php
												}
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Parent Project</label>
										<div class="col-md-8">
											<select class="form-control select" data-live-search="true" name="project_parent" id="project_parent">
												<option value="">Select Parent</option>
												<?php
												foreach($parentList as $parent){
													$sel=""; 
													if($parent['project_id']==$project_parent){
														$sel="selected";												
													}
												?>
												<option value="<?=$parent['project_id']?>" <?=$sel?>><?=$parent['project_name']?></option>
												<?php
												}
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>	
								<div class="col-md-6">
									<div class="form-group">
									<?php
									$start=date("d/m/Y");
									if($project_start_date!="1970-01-01"){
										$start=date("d/m/Y",strtotime($project_start_date));
									}
									?>
										<label class="col-md-4 control-label">Start Date</label>
										<div class="col-md-8">
											<div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" class="form-control datepicker" name="project_start_date" id="project_start_date" placeholder="Enter Your Designation" value="<?=$start?>"/>                                           
                                            </div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Due Date</label>
										<div class="col-md-8">
										<?php
										$due=date("d/m/Y");
										if($project_due_date!="1970-01-01"){
											$due=date("d/m/Y",strtotime($project_due_date));
										}
										?>	
											<div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" class="form-control datepicker" name="project_due_date" id="project_due_date" placeholder="Enter Your Designation" value="<?=$due?>" />                                           
                                            </div>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Description</label>
										<div class="col-md-8">
											<textarea placeholder="Enter Description" class="form-control" name="project_description" id="project_description" rows="3"><?=$project_description?></textarea>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Status</label>
										<div class="col-md-8">
											<select class="form-control select" name="project_status" id="project_status">
												<option value="Active">Active</option>
												<option value="Inactive">Inactive</option>
											</select>
											<script>
												setSelectedIndex(document.getElementById('project_status'),"<?=$project_status?>");
											</script>
										</div>
									</div>
								</div>
							</div>
                            <div class="col-md-12"><br></div>
							<div class="col-md-12">      							    
								<h3>Delivery Details</h3>                          	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Delivery Date</label>
										<div class="col-md-8">
										<?php
										$delivery="";
										if($project_delivery_date!="1970-01-01"){
											$delivery=date("d/m/Y",strtotime($project_delivery_date));
										}
										?>
											<div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" class="form-control datepicker" name="project_delivery_date" id="project_delivery_date" placeholder="Enter Delivery Date" value="<?=$delivery?>" />                                           
                                            </div>
										</div>
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Delivery Remarks</label>
										<div class="col-md-8">
											<textarea placeholder="Enter Remarks" class="form-control" name="project_delivery_description" id="project_delivery_description" rows="3"><?=$project_delivery_description?></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12"><br></div>
							<div class="col-md-12">
								<div class="col-md-12" style="text-align:center;">                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button>                                    <a href="<?=site_url()?>project" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a>                                
								</div>
							</div>
						</div>
					</div>
				</div>												
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
}

include "footer.php"; 
?>
