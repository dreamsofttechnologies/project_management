<?php
$main="main";
$sub="category";

include "header.php";
if($mode=="list"){
	$title="<strong>Category List</strong>";
	//include "titlebar.php";
	?>
<script>
function addPayableItem(projectID,budgetID,staffID){	
	str="projectID="+projectID+"&budgetID="+budgetID+"&staffID="+staffID;	
	url="<?php echo site_url(); ?>delete_category";
	$.ajax({	
		type: "POST",
		url: url,		
		data: str,		
		dataType: "html",
		success: function(data){
			if(data=='deleted'){
				alert("Category Removed Successfully.");
				if(catID==""){
					window.location.href="<?php echo site_url(); ?>category";	
				} else {
					window.location.href="<?php echo site_url(); ?>listcategory/"+catID;	
				}
			}
		}		
	});			
}
</script>
<div id="projectcostsheet">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Project Cost Sheet</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->
		<?php echo validation_errors(); ?>
		<?php echo form_open("projectcostsheet") ?>
			<div class="row" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<div class="col-md-6">
							<h2>Project Cost Sheet<small></small></h2>
						</div>		
					</div>
					<!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <div class="panel-heading">                                
                            <div class="row">
								<div class="col-md-6">                                
                                    <div class="form-group">
										<h3 style="margin:0px;">Project Name: <label style="font-weight:900;color:#3385d9;"><?=$projectName?><label></h3>
									</div> 	
								</div>
							</div>                             
                        </div>
						
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table" style="border-radius:5px;overflow:hidden;">
                                    <thead>
                                        <tr>
                                            <th style="width:16.66%;text-align:center;">Budget Head / Staff</th>
                                            <th colspan="2" class="text-center" style="border-left:1px solid #fff;width:16.66%;">Estimated</th>
                                            <th colspan="2" class="text-center" style="border-left:1px solid #fff;width:16.66%;">Actual</th>
                                            <th style="border-left:1px solid #fff;width:16.66%;text-align:center;">Budget Cost</th>
                                            <th style="border-left:1px solid #fff;width:16.66%;text-align:center;">Actual Cost</th>
                                            <th class="text-center" style="border-left:1px solid #fff;width:16.66%;">Action</th>
                                        </tr>
										<tr>
                                            <th></th>
                                            <th style="border-left:1px solid #fff;text-align:center;">Hrs</th>
                                            <th style="border-left:1px solid #fff;text-align:center;">Rs</th>
                                            <th style="border-left:1px solid #fff;text-align:center;">Hrs</th>
                                            <th style="border-left:1px solid #fff;border-right:1px solid #fff;text-align:center;">Rs</th>
                                            <th colspan="3"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									//print_r($projectBudgetList);
									$totEstHrs=0; $totEstCost=0;
									$totActHrs=0; $totActCost=0;
									$totalEst=0; $totalAct=0;
									
									if(!empty($projectBudgetList)){
										foreach($projectBudgetList as $row){
											$totEstHrs+=$row['estTime']; 
											$totEstCost+=$row['budget_amount'];
											$totActHrs+=$row['actTime']; 
											$totActCost+=$row['actCost'];
											$totalEst+=$row['budget_amount']; 
											$totalAct+=$row['actCost'];
									?>	
										<tr>
											<td align="left" class="iner-th" style="width:13%;"><?=$row['budget_head_name']?></td>
											<td align="right" class="iner-th" style="width:12%;"><?=$row['estTime']?></td>
											<td align="right" class="iner-th" style="width:12%;"><?=number_format($row['budget_amount'],2)?></td>
											<td align="right" class="iner-th" style="width:12%;"><?=$row['actTime']?></td>
											<td align="right" class="iner-th" style="width:12%;"><?=number_format($row['actCost'],2)?></td>
											<td align="right" class="iner-th" style="width:13%;"><?=number_format($row['budget_amount'],2)?></td>
											<td align="right"class="iner-th" style="width:13%;"><?=number_format($row['actCost'],2)?></td>
											<td align="center" class="iner-th" style="width:13%; text-align:right;"></td>
										</tr>
										<?php
											$staffList=$row['staffList'];
											if(!empty($staffList)){
												foreach($staffList as $inp){
										?>
										<tr>
											<td align="center"><?=$inp['staff_name']?></td>
											<td align="right"><?=$inp['staff_estTime']?></td>
											<td align="right"><?=number_format($inp['staff_estCost'],2)?></td>
											<td align="right"><?=$inp['staff_actTime']?></td>
											<td align="right"><?=number_format($inp['staff_actCost'],2)?></td>
											<td align="right"><?=number_format($inp['staff_estCost'],2)?></td>
											<td align="right"><?=number_format($inp['staff_actCost'],2)?></td>
											<td align="center">
												<a href="<?php echo site_url();?>addpayableitems/<?=$inp['task_project_id']?>/<?=$inp['task_budget_id']?>/<?=$inp['task_assigned']?>" class="btn btn-info btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Add Payable Item"><span class="fa fa-plus"></span></a>
											</td>
										</tr>
										<?php
												}
											}										
										}
									}
									?>
										
										
										<!--<tr>
											<td>	
												<table class="table" style="border-radius:5px;overflow:hidden;width:98%;margin:0px auto;">
													<thead>
														<tr>
															<th class="iner-th" style="width:16.66%;">Development</th>
															<th class="iner-th" style="width:8.33%;">12</th>
															<th class="iner-th" style="width:8.33%;">6000</th>
															<th class="iner-th" style="width:8.33%;">17</th>
															<th class="iner-th" style="width:8.33%;">8500</th>
															<th class="iner-th" style="width:16.66%;">4500</th>
															<th class="iner-th" style="width:16.66%;">7500</th>
															<th class="iner-th" style="width:16.66%;"></th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td align="center">ibrahim</td>
															<td align="center">5Hrs</td>
															<td align="center">1000/-</td>
															<td align="center">5Hrs</td>
															<td align="center">1000/-</td>
															<td align="center">5Hrs</td>
															<td align="center">1000/-</td>
															<td align="center">
																<a href="<?php echo site_url();?>task/<?=$b['staff_id']?>" class="btn btn-info btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Add Payable"><span class="fa fa-plus"></span></a>
															</td>
														</tr>
														<tr>
															<td align="center">Shahul</td>
															<td align="center">5Hrs</td>
															<td align="center">1000/-</td>
															<td align="center">5Hrs</td>
															<td align="center">1000/-</td>
															<td align="center">5Hrs</td>
															<td align="center">1000/-</td>
															<td align="center">
																<a href="<?php echo site_url();?>task/<?=$b['staff_id']?>" class="btn btn-info btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Add Payable"><span class="fa fa-plus"></span></a>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr> -->
                                    </tbody>
									<thead>
                                        <tr>										
                                            <th class="text-center">Total</th>
                                            <th class="text-right" style="border-left:1px solid #fff;"><?=$totEstHrs?></th>
                                            <th class="text-right" style="border-left:1px solid #fff;"><?=number_format($totEstCost,2)?></th>
                                            <th class="text-right" style="border-left:1px solid #fff;"><?=$totActHrs?></th>
                                            <th class="text-right" style="border-left:1px solid #fff;"><?=number_format($totActCost,2)?></th>
                                            <th class="text-right" style="border-left:1px solid #fff;"><?=number_format($totalEst,2)?></th>
                                            <th class="text-right" style="border-left:1px solid #fff;"><?=number_format($totalAct,2)?></th>
                                            <th class="text-right" style="border-left:1px solid #fff;"></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END DEFAULT DATATABLE -->
    			</div>
    		</div>
    	</div>
    </form>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>	
<?php 
include "footer.php"; 
?>
