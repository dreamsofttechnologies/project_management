<?php
$main="transaction";
$sub="payables";

include "header.php";
if($mode=="list"){
	$title="<strong>Category List</strong>";
	//include "titlebar.php";
	?>
<script>
function removepayables(id){
	var r = confirm("Do you want to remove this Payables..?");
	if (r == true) {
		str="id="+id;	
		url="<?php echo site_url(); ?>delete_payables";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				if(data=='deleted'){
					alert("Payables Removed Successfully.");
					window.location.href="<?php echo site_url(); ?>payables";						
				}
			}		
		});			
	}
}
</script>
<div id="payables">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Payables</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->
		<?php
		//echo $param;
		if($param==0){
			$action="payables";
		} else if($param!=0){
			$action="listcategory/".$param;
		}
		?>
		<?php echo validation_errors(); ?>
		<?php echo form_open($action) ?>
			<div class="row" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<div class="col-md-6">
							<h2>Payables<small></small></h2>
						</div>															
					</div>
					<!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <div class="panel-heading">                                                               
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table datatable">
                                    <thead>
                                        <tr>
                                            <th class="text-center">S.No</th>
                                            <th class="text-center">Date</th>
                                            <th class="text-center">Staff</th>
                                            <th class="text-center">Payable Amount</th>
                                            <th class="text-center">Payable Item Amount</th>
                                            <th class="text-center">Paid Amount</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									//print_r($payablesList);
									if(!empty($payablesList)){
										$i=0;
										$totalPay=0;
										$totalPayItm=0;
										$totalPaid=0;
										foreach($payablesList as $row){	
											$i++;	
											$totalPay=$totalPay+$row['payable_amount'];
											$totalPayItm=$totalPayItm+$row['payable_payitm_amount'];
											$totalPaid=$totalPaid+$row['payable_amount'];									
									?>
                                        <tr>
                                            <td align="center"><?=$i?></td>
                                            <td align="center"><?=date("d/m/Y",strtotime($row['payable_date']))?></td>
                                            <td align="left"><?=$row['staff_name']?></td>
                                            <td align="right"><?=number_format($row['payable_amount'],2)?></td>
                                            <td align="right"><?=number_format($row['payable_payitm_amount'],2)?></td>
                                            <td align="right"><a href="<?php echo site_url();?>payments/<?=$row['payable_id']?>" data-toggle="tooltip" title="Paid Amount"><?=number_format($row['paidAmt'],2)?></a></td>                                            
											<td align="center">                                                <a href="<?php echo site_url();?>editpayables/<?=$row['payable_id']?>" class="btn btn-success btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="fa fa-pencil"></span></a>                                                          
                                                <a href="javascript:removepayables(<?=$row['payable_id']?>)" class="btn btn-danger btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Delete"><span class="fa fa-times"></span></a>
                                            </td>
                                        </tr>
									<?php
										}
									} else {
									?>
										<tr>
											<td align="center" colspan="7"><b>No Records Found</b></td>
										</tr>
									<?php
									}
									?>
                                    </tbody>
									<thead>
                                        <tr>
                                            <th colspan="3" class="text-right">Total</th>
                                            <th class="text-right"><?=number_format($totalPay,2)?></th>
                                            <th class="text-right"><?=number_format($totalPayItm,2)?></th>
                                            <th class="text-right"><?=number_format($totalPaid,2)?></th>
                                            <th style="text-align:center;"></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END DEFAULT DATATABLE -->
    			</div>
    		</div>
    	</div>
    </form>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>	
<?php
if($mode=="add"){
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>category">Payables</a></li>
	<li class="active" >Add New Payables</li>
</ul>
<div class="page-content-wrap" id="payables">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('save_payables',$attr); ?>
		<div class="row form-horizontal" style="padding-top:10px;">
			<div class="col-md-12">
				<div class="page-title">
					<div class="page-title">
                        <div class="col-md-6">
                            <h2>Add New Payables<small></small></h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?=site_url()?>payables" class="btn btn-info btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
                        </div>                   
                    </div>
				</div>
				<div class="panel panel-hidden-controls">
					<!-- <div class="panel-heading">
						<h1 class="panel-title">Add New Category</h1>
					</div> -->
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Staff</label>
										<div class="col-md-8">
											<label> : <?=$itemsDets['staff_name']?></label>
											<input type="hidden" class="form-control" name="payable_staff_id" id="payable_staff_id" value="<?=$itemsDets['payitm_staff_id']?>" />
											<input type="hidden" class="form-control" name="itemsIds" id="itemsIds" value="<?=$itemsIds?>" />
										</div>
									</div>
								</div>
								<div class="col-md-6">                                
                                    <div class="form-group">
										<label class="col-md-4 control-label">Date</label>
										<div class="input-group col-md-8"> 
											<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
											<input type="text" name="payable_date" id="payable_date" class="form-control datepicker" value="<?=date("d/m/Y")?>">  
										</div>
									</div> 	
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Payable Amount</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="payable_amount" id="payable_amount" placeholder="Enter Payable Amount"/>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-5 control-label">Payable Item Amount</label>
										<div class="col-md-7">
											<label> : <?=number_format($itemsDets['totAmt'],2)?></label>
											<input type="hidden" class="form-control" name="payable_payitm_amount" id="payable_payitm_amount" value="<?=$itemsDets['totAmt']?>"/>
										</div>
									</div>
								</div>                                
                                <div class="col-md-12"><br></div>
                                <div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
                                    <a href="<?=site_url()?>payables" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a>                                     
                                </div>
							</div>
						</div>
					</div>
				</div>											                                
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
} 
?>
<?php
if($mode=="edit"){
	//print_r($payableDets);
	extract($payableDets);
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>category">Category</a></li>
	<li class="active">Edit Payables</li>
</ul>
<div class="page-content-wrap">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('update_payables',$attr); ?>
		<div class="row form-horizontal">
			<div class="col-md-12">
				<div class="page-title">
					<h2>Payables<small></small></h2>
				</div>
				<div class="panel panel-primary panel-hidden-controls">
					<div class="panel-heading">
						<h1 class="panel-title">Edit Payables</h1>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Staff</label>
										<div class="col-md-8">
											<label> : <?=$staff_name?></label>
											<input type="hidden" class="form-control" name="payable_staff_id" id="payable_staff_id" value="<?=$payable_staff_id?>" />
											<input type="hidden" class="form-control" name="payable_id" id="payable_id" value="<?=$payable_id?>" />
											
										</div>
									</div>
								</div>
								<div class="col-md-6">                                
                                    <div class="form-group">
										<label class="col-md-4 control-label">Date</label>
										<div class="input-group col-md-8"> 
											<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
											<input type="text" name="payable_date" id="payable_date" class="form-control datepicker" value="<?=date("d/m/Y",strtotime($payable_date))?>">  
										</div>
									</div> 	
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Payable Amount</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="payable_amount" id="payable_amount" placeholder="Enter Payable Amount" value="<?=$payable_amount?>"/>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-5 control-label">Payable Item Amount</label>
										<div class="col-md-7">
											<label> : <?=number_format($payable_payitm_amount,2)?></label>
											<input type="hidden" class="form-control" name="payable_payitm_amount" id="payable_payitm_amount" value="<?=$payable_payitm_amount?>"/>
										</div>
									</div>
								</div>                                
                                <div class="col-md-12"><br></div>
                                <div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
                                    <a href="<?=site_url()?>payables" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a>                                     
                                </div>
							</div>
						</div>
					</div>
				</div>												
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
}
include "footer.php"; 
?>
