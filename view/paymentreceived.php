<?php
$main="transaction";
$sub="project";

include "header.php";
if($mode=="list"){
	$title="<strong>Payment Received List</strong>";
	//include "titlebar.php";
	?>
<div id="paymentreceived">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Payment Received</li>
	</ul>
	<div class="page-content-wrap" id="paymentreceived">
		<?php echo validation_errors(); ?>
		<?php 
		
		$attr=array('onsubmit' => 'return checkInputs()');
		echo form_open_multipart('save_paymentreceived',$attr); ?>
			<div class="row form-horizontal" style="padding-top:10px;">
				<div class="col-md-12">
					<div class="page-title">
						<div class="page-title">
							<div class="col-md-6">
								<h2>Payment Received<small></small></h2>
							</div>                  
						</div>
					</div>
					<div class="panel panel-hidden-controls">
						<!-- <div class="panel-heading">
							<h1 class="panel-title">Add New Payment Received</h1>
						</div> -->
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
								<?php
								//print_r($invoiceDets);
								extract($invoiceDets);
								?>
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-4 control-label">Project Name</label>
											<div class="col-md-8">
												<label> : <?=$project_name?></label>
												<input type="hidden" class="form-control" name="pr_invoice_id" id="pr_invoice_id" value="<?=$invoice_id?>" />
												<input type="hidden" class="form-control" name="pr_project_id" id="pr_project_id" value="<?=$project_id?>" />
											</div>
										</div>
									</div>									
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-4 control-label">Client Name</label>
											<div class="col-md-8">
												<label> : <?=$customer_name?></label>
												<input type="hidden" class="form-control" name="pr_client_id" id="pr_client_id" value="<?=$customer_id?>" />
											</div>
										</div>
									</div>
									<div class="col-md-12"><br></div>
									
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-4 control-label">Received Date</label>
											<div class="col-md-8">
												<div class="input-group">
													<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
													<input type="text" class="form-control datepicker" name="pr_date" id="pr_date" value="<?=date("d/m/Y")?>" />                                           
												</div>
											</div>
										</div>
									</div>	
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-4 control-label">Amount</label>
											<div class="col-md-8">
												<div class="input-group">
													<span class="input-group-addon"><span class="fa fa-inr"></span></span>
													<input type="text" class="form-control" name="pr_amount" id="pr_amount" placeholder="Enter Received Amount" value="" />                                           
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12"><br></div>
									
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-4 control-label">Remarks</label>
											<div class="col-md-8">
												<textarea placeholder="Enter Remarks" class="form-control" name="pr_remarks" id="pr_remarks" rows="3"></textarea>
											</div>
										</div>
									</div>
									<div class="col-md-12"><br></div>
									<div class="col-md-12" style="text-align:center;">
										<button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
										<a href="<?=site_url()?>invoice/<?=$project_id?>" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 										
									</div>
								</div>
							</div>
						</div>
					</div>											                                
				</div>
			</div>
		</form>
	</div>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>
<?php	
include "footer.php"; 
?>
