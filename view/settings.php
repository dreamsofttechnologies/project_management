<?php
    $main="settings";
    $sub="curr_settings";
    include "header.php";
    
    if($mode=="list"){
    	?>
<script type="text/javascript">
    function setSelectedIndexval(s, v) {
    	for ( var i = 0; i < s.options.length; i++ ) {
    		if ( s.options[i].value == v ) {
    			s.options[i].selected = true;
    			return;
    		}
    	}
    }		
</script>
<ul class="breadcrumb">
    <li><a href="#dashboard">Home</a></li>
    <li class="active">Settings</li>
</ul>
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    <?php echo form_open_multipart('save_settings'); ?>
    <div class="row form-horizontal">
        <div class="col-md-12">
            <div class="page-title">
                <h2><span class="glyphicon fa fa-gear"></span> Settings<small></small></h2>
            </div>
            <div class="panel panel-primary panel-hidden-controls">
                <div class="panel-heading">
                    <h1 class="panel-title">Settings</h1>
                </div>
                <div class="panel-body">
                    <div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="set_name" id="set_name" placeholder="Enter Name" value="<?=$settings['set_name']?>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label">Address</label>
							<div class="col-md-8">
								<textarea class="form-control" name="set_address" id="set_address" placeholder="Enter Address"><?=$settings['set_address']?></textarea>
							</div>
						</div>
					</div>								
					<div class="col-md-12"><br></div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label">Mobile No</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="set_mobileno" id="set_mobileno" placeholder="Enter Mobile No" value="<?=$settings['set_mobileno']?>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label">Email ID</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="set_emailid" id="set_emailid" placeholder="Enter Email ID" value="<?=$settings['set_emailid']?>" />
							</div>
						</div>
					</div>								
					<div class="col-md-12"><br></div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label">Logo</label>
							<div class="col-md-8">
								<input type="file" class="fileinput" name="set_logo" id="set_logo" /><br/>								
								<?php
								if($settings['set_logo']!=""){
								?>											
								<br />											
								<div class="col-md-12">
									<img src="<?php echo site_url().'images/logo/'.$settings['set_logo']; ?>" style="width:250px; padding:5px"/><br />
									<a href="javascript:removeImage(<?=$settings['set_id']?>)" >Remove</a>
								</div>
								<?php
								}
								?>
							</div>
						</div>
					</div>	
					<div class="col-md-6">
						<div class="form-group">
						</div>
					</div>											
					<div class="col-md-12"><br></div>
					<div class="col-md-12"><h3>Currency Details</h3></div>
					<div class="col-md-12"><br></div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Currency</label>
                            <div class="col-md-8">
                                <input type="hidden" name="set_id" id="set_id" value="<?=$settings['set_id']?>">
                                <select class="form-control select" name="currency" id="currency" data-live-search="true">
                                    <?php foreach($currency as $cur){
                                        if($cur['cur_id']==$settings['set_curid']){ $sel="selected"; }else{ $sel=""; }
                                        ?>
                                    <option value="<?=$cur['cur_id']?>" <?=$sel?>><?=$cur['cur_country']?> (<?=$cur['cur_currcode']?>)</option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                    </div>
					<div class="col-md-6">
					    <div class="form-group">
                            <label class="col-md-4 control-label">Time Zone</label>
                            <div class="col-md-8">
                                <select class="form-control select" name="timezonenew" id="timezonenew" data-live-search="true">
                                    <option value="Pacific/Midway">(GMT-12:00) International Date Line West</option>
                                    <option value="Pacific/Samoa">(GMT-11:00) Midway Island, Samoa</option>
                                    <option value="Pacific/Honolulu">(GMT-10:00) Hawaii</option>
                                    <option value="US/Alaska">(GMT-09:00) Alaska</option>
                                    <option value="America/Los_Angeles">(GMT-08:00) Pacific Time (US & Canada)</option>
                                    <option value="America/Tijuana">(GMT-08:00) Tijuana, Baja California</option>
                                    <option value="US/Arizona">(GMT-07:00) Arizona</option>
                                    <option value="America/Chihuahua">(GMT-07:00) Chihuahua</option>
                                    <option value="America/Chihuahua">(GMT-07:00) La Paz</option>
                                    <option value="America/Mazatlan">(GMT-07:00) Mazatlan</option>
                                    <option value="US/Mountain">(GMT-07:00) Mountain Time (US & Canada)</option>
                                    <option value="America/Managua">(GMT-06:00) Central America</option>
                                    <option value="US/Central">(GMT-06:00) Central Time (US & Canada)</option>
                                    <option value="America/Mexico_City">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>
                                    <option value="Canada/Saskatchewan">(GMT-06:00) Saskatchewan</option>
                                    <option value="America/Bogota">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>
                                    <option value="US/Eastern">(GMT-05:00) Eastern Time (US & Canada)</option>
                                    <option value="US/East-Indiana">(GMT-05:00) Indiana (East)</option>
                                    <option value="Canada/Atlantic">(GMT-04:00) Atlantic Time (Canada)</option>
                                    <option value="'America/Caracas">(GMT-04:30) Caracas</option>
                                    <option value="'America/La_Paz">(GMT-04:00) La Paz</option>
                                    <option value="America/Santiago">(GMT-04:00) Manaus</option>
                                    <option value="America/Santiago">(GMT-04:00) Santiago</option>
                                    <option value="Canada/Newfoundland">(GMT-03:30) Newfoundland</option>
                                    <option value="America/Sao_Paulo">(GMT-03:00) Brasilia</option>
                                    <option value="America/Argentina/Buenos_Aires">(GMT-03:00) Buenos Aires, Georgetown</option>
                                    <option value="America/Godthab">(GMT-03:00) Greenland</option>
                                    <option value="America/Godthab">(GMT-03:00) Montevideo</option>
                                    <option value="America/Noronha">(GMT-02:00) Mid-Atlantic</option>
                                    <option value="Atlantic/Cape_Verde">(GMT-01:00) Cape Verde Is.</option>
                                    <option value="Atlantic/Azores">(GMT-01:00) Azores</option>
                                    <option value="Africa/Casablanca">(GMT+00:00) Casablanca, Monrovia, Reykjavik</option>
                                    <option value="Etc/Greenwich">(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London</option>
                                    <option value="Europe/Amsterdam">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
                                    <option value="Europe/Belgrade">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
                                    <option value="Europe/Brussels">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>
                                    <option value="Europe/Sarajevo">(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb</option>
                                    <option value="Africa/Lagos">(GMT+01:00) West Central Africa</option>
                                    <option value="Europe/Athens">(GMT+02:00) Amman</option>
                                    <option value="Europe/Athens">(GMT+02:00) Athens, Bucharest, Istanbul</option>
                                    <option value="Europe/Bucharest">(GMT+02:00) Beirut</option>
                                    <option value="Africa/Cairo">(GMT+02:00) Cairo</option>
                                    <option value="Africa/Harare">(GMT+02:00) Harare, Pretoria</option>
                                    <option value="Europe/Helsinki">(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>
                                    <option value="Asia/Jerusalem">(GMT+02:00) Jerusalem</option>
                                    <option value="Asia/Jerusalem">(GMT+02:00) Minsk</option>
                                    <option value="Asia/Jerusalem">(GMT+02:00) Windhoek</option>
                                    <option value="Asia/Kuwait">(GMT+03:00) Kuwait, Riyadh, Baghdad</option>
                                    <option value="Europe/Moscow">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>
                                    <option value="Africa/Nairobi">(GMT+03:00) Nairobi</option>
                                    <option value="Asia/Tbilisi">(GMT+03:00) Tbilisi</option>
                                    <option value="Asia/Tehran">(GMT+03:30) Tehran</option>
                                    <option value="Asia/Muscat">(GMT+04:00) Abu Dhabi, Muscat</option>
                                    <option value="Asia/Baku">(GMT+04:00) Baku</option>
                                    <option value="Asia/Yerevan">(GMT+04:00) Yerevan</option>
                                    <option value="Asia/Kabul">(GMT+04:30) Kabul</option>
                                    <option value="Asia/Yekaterinburg">(GMT+05:00) Yekaterinburg</option>
                                    <option value="Asia/Karachi">(GMT+05:00) Islamabad, Karachi, Tashkent</option>
                                    <option value="Asia/Calcutta">(GMT+05:30) Sri Jayawardenapura</option>
                                    <option value="Asia/Calcutta">(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
                                    <option value="Asia/Katmandu">(GMT+05:45) Kathmandu</option>
                                    <option value="Asia/Almaty">(GMT+06:00) Almaty, Novosibirsk</option>
                                    <option value="Asia/Dhaka">(GMT+06:00) Astana, Dhaka</option>
                                    <option value="Asia/Rangoon">(GMT+06:30) Yangon (Rangoon)</option>
                                    <option value="Asia/Bangkok">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>
                                    <option value="Asia/Krasnoyarsk">(GMT+07:00) Krasnoyarsk</option>
                                    <option value="Asia/Chongqing">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
                                    <option value="Asia/Kuala_Lumpur">(GMT+08:00) Kuala Lumpur, Singapore</option>
                                    <option value="Asia/Irkutsk">(GMT+08:00) Irkutsk, Ulaan Bataar</option>
                                    <option value="Australia/Perth">(GMT+08:00) Perth</option>
                                    <option value="Asia/Taipei">(GMT+08:00) Taipei</option>
                                    <option value="Asia/Tokyo">(GMT+09:00) Osaka, Sapporo, Tokyo</option>
                                    <option value="Asia/Seoul">(GMT+09:00) Seoul</option>
                                    <option value="Asia/Yakutsk">(GMT+09:00) Yakutsk</option>
                                    <option value="Australia/Adelaide">(GMT+09:30) Adelaide</option>
                                    <option value="Australia/Darwin">(GMT+09:30) Darwin</option>
                                    <option value="Australia/Brisbane">(GMT+10:00) Brisbane</option>
                                    <option value="Australia/Canberra">(GMT+10:00) Canberra, Melbourne, Sydney</option>
                                    <option value="Australia/Hobart">(GMT+10:00) Hobart</option>
                                    <option value="Pacific/Guam">(GMT+10:00) Guam, Port Moresby</option>
                                    <option value="Asia/Vladivostok">(GMT+10:00) Vladivostok</option>
                                    <option value="Asia/Magadan">(GMT+11:00) Magadan, Solomon Is., New Caledonia</option>
                                    <option value="Pacific/Auckland">(GMT+12:00) Auckland, Wellington</option>
                                    <option value="Pacific/Fiji">(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>
                                    <option value="Pacific/Tongatapu">(GMT+13:00) Nuku'alofa</option>
                                </select>
                                <script>
                                    setSelectedIndexval(document.getElementById('timezonenew'),"<?=$settings['set_timezone']?>");
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="panel-footer"  style="text-align:center">
                    <button type="submit"  class="btn  btn-warning" name="btn_change" id="btn_change"><span class="fa fa-check-circle"></span>Save</button>
                </div>
                <br>
                <br>
            </div>
        </div>
    </div>
    </form>
</div>
<div style="clear:both;"></div>
<script>
function removeImage(id){
	var r = confirm("Do you want to remove this Image..?");
	if (r == true) {
		str="id="+id;	
		url="<?php echo site_url(); ?>setting_imageremove";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				//alert(data);
				if(data==1){
					alert("Image Removed Successfully.");					
					window.location.href="<?php echo site_url(); ?>settings";	
				}				
			}		
		});			
	}
}
</script>
<?php 
}
include "footer.php"; 
?>