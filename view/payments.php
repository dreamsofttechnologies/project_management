<?php
$main="transaction";
$sub="payables";

include "header.php";
if($mode=="list"){
	$title="<strong>Category List</strong>";
	//include "titlebar.php";
	?>
<script>
function removePayment(id){
	var r = confirm("Do you want to remove this Payments..?");
	if (r == true) {
		str="id="+id;	
		url="<?php echo site_url(); ?>delete_payments";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				if(data=='deleted'){
					alert("Payments Removed Successfully.");
					window.location.href="<?php echo site_url(); ?>payments";	
				}
			}		
		});			
	}
}
</script>
<div id="payments">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Payments</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->
		<?php
		//echo $param;
		if($param==0){
			$action="payments";
		} else if($param!=0){
			$action="listcategory/".$param;
		}
		?>
		<?php echo validation_errors(); ?>
		<?php echo form_open($action) ?>
			<div class="row" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<div class="col-md-6">
							<h2>Payments<small></small></h2>
						</div>
						<div class="col-md-6 text-right">
							<a href="<?php echo site_url();?>addpayments/<?=$payableID?>" class="btn btn-info btn-condensed"><span class="fa fa-plus"></span> Add New Payments</a> 
							<a href="<?=site_url()?>payables" class="btn btn-danger btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
						</div>
							
					</div>
					<!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <div class="panel-heading">                                                               
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table datatable">
                                    <thead>
                                        <tr>
                                            <th class="text-center">S.No</th>
                                            <th class="text-center">Staff</th>
                                            <th class="text-center">Date</th>
                                            <th class="text-center">Amount</th>
                                            <th class="text-center">Particulars</th>
                                            <th class="text-center">Remarks</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
									//print_r($paymentList);
									if(!empty($paymentList)){
										$i=0;
										foreach($paymentList as $row){
											$i++;
									?>  
										<tr>
                                        	<td align="center"><?=$i?></td>
                                            <td align="left"><?=$row['staff_name']?></td>
                                            <td align="center"><?=date("d/m/Y",strtotime($row['payment_date']))?></td>
                                            <td align="right"><?=number_format($row['payment_amount'],2)?></td>
                                            <td align="left"><?=$row['payment_particulars']?></td>
                                            <td align="left"><?=$row['payment_remarks']?></td>
                                            <td align="center">                                                       
                                                <a href="<?php echo site_url();?>editpayments/<?=$row['payment_id']?>" class="btn btn-success btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="fa fa-pencil"></span></a>                                                          
                                                <a href="javascript:removePayment(<?=$row['payment_id']?>)" class="btn btn-danger btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Delete"><span class="fa fa-times"></span></a>
                                            </td>
                                        </tr>
									<?php
										}
									} else {
									?>
										<tr>
											<td align="center" colspan="8"><b>No Records Found</b></td>
										</tr>
									<?php
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END DEFAULT DATATABLE -->
    			</div>
    		</div>
    	</div>
    </form>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>	
<?php
if($mode=="add"){	
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>category">Payments</a></li>
	<li class="active" >Add New Payments</li>
</ul>
<div class="page-content-wrap" id="payments">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('save_payments',$attr); ?>
		<div class="row form-horizontal" style="padding-top:10px;">
			<div class="col-md-12">
				<div class="page-title">
					<div class="page-title">
                        <div class="col-md-6">
                            <h2>Add New Payments<small></small></h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?=site_url()?>payments" class="btn btn-info btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
                        </div>                   
                    </div>
				</div>
				<div class="panel panel-hidden-controls">
					<!-- <div class="panel-heading">
						<h1 class="panel-title">Add New Category</h1>
					</div> -->
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Staff</label>
										<div class="col-md-8">
											<input type="hidden" name="payment_payable_id" id="payment_payable_id" class="form-control" value="<?=$payableID?>">
											<select class="form-control select" data-live-search="true" name="payment_staff_id" id="payment_staff_id">
												<option value="">Select Staff</option>
												<?php
												foreach($staffList as $staff){
													$sel=""; if($payableDet['payable_staff_id']==$staff['staff_id']){ $sel="selected"; }
												?>
												<option value="<?=$staff['staff_id']?>" <?=$sel?>><?=$staff['staff_name']?></option>
												<?php
												}
												?>												
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6">                                
                                    <div class="form-group">
										<label class="col-md-4 control-label">Date</label>
										<div class="input-group col-md-8"> 
											<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
											<input type="text" name="payment_date" id="payment_date" class="form-control datepicker" value="<?= date("d/m/Y")?>">  
										</div>
									</div> 	
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">                                
                                    <div class="form-group">
										<label class="col-md-4 control-label">Amount</label>
										<div class="input-group col-md-8"> 
											<span class="input-group-addon"><span class="fa fa-inr"></span></span>
											<input type="text" name="payment_amount" id="payment_amount" class="form-control" placeholder="Enter Amount">  
										</div>
									</div> 	
								</div>
								<div class="col-md-6">                                
                                    <div class="form-group">
										<label class="col-md-4 control-label">Particulars</label>
										<div class="col-md-8"> 
											<input type="text" name="payment_particulars" id="payment_particulars" class="form-control" placeholder="Enter Particulars">  
										</div>
									</div> 	
								</div>
                                <div class="col-md-12"><br></div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Remarks</label>
                                        <div class="col-md-8">
                                            <textarea placeholder="Enter Remarks" class="form-control" name="payment_remarks" id="payment_remarks" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>	
                                <div class="col-md-12"><br></div>
                                <div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
                                    <a href="<?=site_url()?>payments" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a>                                     
                                </div>
							</div>
						</div>
					</div>
				</div>											                                
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
} 
?>
<?php
if($mode=="edit"){
	//print_r($paymentDet);
	extract($paymentDet);
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>category">Category</a></li>
	<li class="active">Edit Payments</li>
</ul>
<div class="page-content-wrap">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('update_payments',$attr); ?>
		<div class="row form-horizontal">
			<div class="col-md-12">
				<div class="page-title">
					<h2>Payments<small></small></h2>
				</div>
				<div class="panel panel-primary panel-hidden-controls">
					<div class="panel-heading">
						<h1 class="panel-title">Edit Payments</h1>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Staff</label>
										<div class="col-md-8">
											<input type="hidden" name="payment_id" id="payment_id" value="<?=$payment_id?>">  
											<input type="hidden" name="payment_payable_id" id="payment_payable_id" class="form-control" value="<?=$payment_payable_id?>">
											<select class="form-control select" data-live-search="true" name="payment_staff_id" id="payment_staff_id">
												<option value="">Select Staff</option>
												<?php
												foreach($staffList as $staff){
													$sel="";
													if($staff['staff_id']==$payment_staff_id){
														$sel="selected";													
													}
												?>
												<option value="<?=$staff['staff_id']?>" <?=$sel?>><?=$staff['staff_name']?></option>
												<?php
												}
												?>												
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6">                                
                                    <div class="form-group">
										<label class="col-md-4 control-label">Date</label>
										<div class="input-group col-md-8"> 
											<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
											<input type="text" name="payment_date" id="payment_date" class="form-control datepicker" value="<?=date("d/m/Y",strtotime($payment_date))?>">  
										</div>
									</div> 	
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">                                
                                    <div class="form-group">
										<label class="col-md-4 control-label">Amount</label>
										<div class="input-group col-md-8"> 
											<span class="input-group-addon"><span class="fa fa-inr"></span></span>
											<input type="text" name="payment_amount" id="payment_amount" class="form-control" placeholder="Enter Amount" value="<?=$payment_amount?>" >  
										</div>
									</div> 	
								</div>
								<div class="col-md-6">                                
                                    <div class="form-group">
										<label class="col-md-4 control-label">Particulars</label>
										<div class="col-md-8"> 
											<input type="text" name="payment_particulars" id="payment_particulars" class="form-control" placeholder="Enter Particulars" value="<?=$payment_particulars?>">  
										</div>
									</div> 	
								</div>
                                <div class="col-md-12"><br></div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Remarks</label>
                                        <div class="col-md-8">
                                            <textarea placeholder="Enter Remarks" class="form-control" name="payment_remarks" id="payment_remarks" rows="3"><?=$payment_remarks?></textarea>
                                        </div>
                                    </div>
                                </div>	
                                <div class="col-md-12"><br></div>
                                <div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
                                    <a href="<?=site_url()?>payments" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a>                                     
                                </div>
							</div>
						</div>
					</div>
				</div>															
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
}
include "footer.php"; 
?>
