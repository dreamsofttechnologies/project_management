<?php
$main="settings";
$sub="changepassword";

include "header.php";
if($mode=="list"){	
	?>
	<style>
	
.successbox{
	background-color:#ffcc33 !important;
	color: #000 !important;
	display: inline-block;
	margin-bottom:10px;
	padding:15px 20px;
	border-radius:5px;
	font-size:16px;
	text-align:center;
}
	</style>
<script type="text/javascript">
function savePage(){
	if($('#usr_currpwd').val()!="" && $('#usr_conpwd').val()!="" && $('#usr_newpwd').val()!=""){		
		if($('#usr_conpwd').val()==$('#usr_newpwd').val()){
			str=$( "form" ).serialize();
			//alert(str);
			url="changepassword/change_save";
			url="<?php echo site_url();?>"+url;
			//alert(url);
			$.ajax({	//create an ajax request to load_page.php
				type: "POST",
				url: url,		
				data: str,		
				dataType: "html",
				success: function(data){
					//alert(data);
					//$('#ajax_fixing_id').html(data);	
					window.location.href="<?php echo site_url();?>/changepassword";
				}		
			});		
		} else {
			alert("New password and Confirm Password Does not Match");
			$('#usr_conpwd').focus();
		}
	} else {
		if($('#usr_currpwd').val()==""){
			alert("Enter Current password");
			$('#usr_currpwd').focus();
		} else if($('#usr_newpwd').val()==""){
			alert("Enter New password");
			$('#usr_newpwd').focus();
		} else if($('#usr_conpwd').val()==""){
			alert("Enter Confirm password");
			$('#usr_conpwd').focus();
		}
	}
}
</script>

<ul class="breadcrumb">
	<li><a href="#">Home</a></li>
	<li class="active">Change Password</li>
</ul>
<div class="page-content-wrap">
<!--<form class="form-horizontal" action="" method="post" enctype="multipart/form-data" name="jvalidate"id="jvalidate"> -->
<?php echo validation_errors(); ?>
<?php echo form_open('change_password'); ?>
	<div class="row form-horizontal">
		<div class="col-md-12">
			<div class="page-title">
				<h2><span class="fa fa-gear"></span> Change Password<small></small></h2>
				</div>
			<div class="panel panel-primary panel-hidden-controls">
				<div class="panel-heading">
					<h1 class="panel-title">Change Password</h1>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
						<?php
				if($this->session->flashdata('message')!=""){
				?>
				<div class="col-md-12 successbox"><?=$this->session->flashdata('message')?></div>
				<?php
					$this->session->set_flashdata('message','');
					} 
					?>											
							<div class="col-md-10">								
								<div class="form-group">
									<label class="col-md-4 control-label"> Current Password </label>
									<div class="col-md-8">
										<input type="password" class="form-control" name="usr_currpwd" id="usr_currpwd" placeholder="Enter Current Password"/>
										<input type="hidden" class="form-control" name="usr_id" id="usr_id" value="<?=$usr_id?>"/>
									</div>
								</div>								
								<div class="form-group">
									<label class="col-md-4 control-label"> New Password </label>
									<div class="col-md-8">
										<input type="password" class="form-control" name="usr_newpwd" id="usr_newpwd" placeholder="Enter New Password"/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label"> Confirm Password </label>
									<div class="col-md-8">
										<input type="password" class="form-control" name="usr_conpwd" id="usr_conpwd" placeholder="Enter Confirm Password"/>
									</div>
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div style="text-align:center">			 
				<button type="button" class="btn btn-warning" name="btn_add" id="btn_add" onClick="javascript: savePage()"><span class="fa fa-check-circle"></span> Save</button> 
			</div>
		</div>
</form>
</div>
<div style="clear:both;"></div>
<?php 
} 
include "footer.php";
?>
