<?php
$main="settings";
$sub="service";

include "header.php";
if($mode=="list"){
	$title="<strong>Service List</strong>";
	//include "titlebar.php";
	?>
<script>
function removeservice(id){
	var r = confirm("Do you want to remove this Service..?");
	if (r == true) {
		str="id="+id;	
		url="<?php echo site_url(); ?>delete_service";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				if(data=='deleted'){
					alert("Service Removed Successfully.");
					window.location.href="<?php echo site_url(); ?>service";						
				}
			}		
		});			
	}
}
function rowLimit(v){
	pg=$('#page').val();
	window.location.href="<?php echo site_url(); ?>service/"+pg+"/"+v;					
}
</script>
<div id="service">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Service</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->
		<?php
		$action="service";		
		?>
		<?php echo validation_errors(); ?>
		<?php echo form_open($action) ?>
			<div class="row" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<div class="col-md-6">
							<h2>Service<small></small></h2>
						</div>						
						<div class="col-md-6 text-right">
							<a href="<?php echo site_url();?>addservice" class="btn btn-info btn-condensed"><span class="fa fa-plus"></span> Add New Service</a> </div>						
					</div>
					<!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <div class="panel-heading">                                                              
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table datatable">
                                    <thead>
                                        <tr>
                                            <th class="text-center">S.No</th>
                                            <th class="text-center">Service Name</th>
                                            <th class="text-center" style="width:300px;">Description</th>
                                            <th class="text-center">Amount</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                  	<?php
									//print_r($serviceList);
									$len=count($serviceList);
									if($len>0){
										$i=0;
										foreach($serviceList as $row){
											$i++;
									?>
									    <tr>
                                            <td align="center"><?=$i?></td>
                                            <td align="left"><?=$row['service_name']?></td>
                                            <td align="left"><?=$row['service_description']?></td>
                                            <td align="right"><?=number_format($row['service_amount'],2)?></td>
                                            <td align="center">                                                      
                                                <a href="<?php echo site_url();?>editservice/<?=$row['service_id']?>" class="btn btn-success btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="fa fa-pencil"></span></a>                                                          
                                                <a href="javascript:removeservice(<?=$row['service_id']?>)" class="btn btn-danger btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Delete"><span class="fa fa-times"></span></a>
                                            </td>
                                        </tr>
									<?php
										}
									} else {
									?>
										<tr>
											<td colspan="5" align="center"><b>No Records Found</b></td>
										</tr>
									<?php	
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END DEFAULT DATATABLE -->
    			</div>
    		</div>
    	</div>
    </form>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>	
<?php
if($mode=="add"){
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>service">Service</a></li>
	<li class="active" >Add New Service</li>
</ul>
<div class="page-content-wrap" id="service">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('save_service',$attr); ?>
		<div class="row form-horizontal" style="padding-top:10px;">
			<div class="col-md-12">
				<div class="page-title">
					<div class="page-title">
                        <div class="col-md-6">
                            <h2>Add New Service<small></small></h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?=site_url()?>service" class="btn btn-info btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
                        </div>                   
                    </div>
				</div>
				<div class="panel panel-hidden-controls">
					<!-- <div class="panel-heading">
						<h1 class="panel-title">Add New Service</h1>
					</div> -->
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Service Name</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="service_name" id="service_name" placeholder="Enter Service Name"/>											
										</div>
									</div>
								</div>	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Amount</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="service_amount" id="service_amount" placeholder="Enter Amount"/>
										</div>
									</div>
								</div>
                                <div class="col-md-12"><br></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Description</label>
                                        <div class="col-md-8">
                                            <textarea placeholder="Enter Description" class="form-control" name="service_description" id="service_description" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>								
                                <div class="col-md-12"><br></div>
                                <div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
                                    <a href="<?=site_url()?>service" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
                                </div>
							</div>
						</div>
					</div>
				</div>											                                
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
} 
?>
<?php
if($mode=="edit"){
	//print_r($serviceDets);
	extract($serviceDets);
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>service">Service</a></li>
	<li class="active">Edit Service</li>
</ul>
<div class="page-content-wrap">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('update_service',$attr); ?>
		<div class="row form-horizontal">
			<div class="col-md-12">
				<div class="page-title">
					<h2>Service<small></small></h2>
				</div>
				<div class="panel panel-primary panel-hidden-controls">
					<div class="panel-heading">
						<h1 class="panel-title">Edit Service</h1>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Service Name</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="service_name" id="service_name" placeholder="Enter Service Name" value="<?=$service_name?>" />											
											<input type="hidden" class="form-control" name="service_id" id="service_id" value="<?=$service_id?>" />											
										</div>
									</div>
								</div>	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Amount</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="service_amount" id="service_amount" placeholder="Enter Amount" value="<?=$service_amount?>" />
										</div>
									</div>
								</div>
                                <div class="col-md-12"><br></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Description</label>
                                        <div class="col-md-8">
                                            <textarea placeholder="Enter Description" class="form-control" name="service_description" id="service_description" rows="3"><?=$service_description?></textarea>
                                        </div>
                                    </div>
                                </div>								
                                <div class="col-md-12"><br></div>
                                <div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
                                    <a href="<?=site_url()?>service" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
                                </div>
							</div>
						</div>
					</div>
				</div>												
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
}
include "footer.php"; 
?>
