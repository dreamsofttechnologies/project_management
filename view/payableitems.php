<?php
$main="transaction";
$sub="payitem";

include "header.php";
if($mode=="list"){
	$title="<strong>Category List</strong>";
	//include "titlebar.php";
	?>
<script>
function removePaymentItem(id){
	var r = confirm("Do you want to remove this Payable Items..?");
	if (r == true) {
		str="id="+id;	
		url="<?php echo site_url(); ?>delete_payableitems";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				if(data=='deleted'){
					alert("Payable Items Removed Successfully.");
					window.location.href="<?php echo site_url(); ?>payableitems";						
				}
			}		
		});			
	}
}

function addtoPayable(){
	var count=$('#hidCount').val();
	alert(count);
	var ids="";
	for(i=1;i<=count;i++){
		if($('#chk_itm'+i).is(":checked")){
			ids=ids+","+$('#chk_itm'+i).val();
		}
	}
	ids=ids.substr(1);
	alert(ids);
	str="ids="+ids;	
	url="<?php echo site_url(); ?>create_payables";
	$.ajax({	
		type: "POST",
		url: url,		
		data: str,		
		dataType: "html",
		success: function(data){
			alert(data);
		}		
	});				
}

</script>
<div id="payableitems">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Payable Items</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->		
		<?php echo validation_errors(); ?>
		<?php echo form_open("create_payables") ?>
			<div class="row" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<div class="col-md-6">
							<h2>Payable Items</h2>
						</div>						
						<div class="col-md-6 text-right">
							<!--<a href="<?php echo site_url();?>addpayableitems" class="btn btn-info btn-condensed"><span class="fa fa-plus"></span> Add New Payable Items</a> --> 				<button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-plus"></span> Add to Payable</button>
							
						</div>								
					</div>
					<!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <div class="panel-heading">                                                               
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table datatable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><input type="hidden" name="hidCount" id="hidCount" value="<?=count($payableitemsList)?>" /></th>
											<th class="text-center">S.No</th>
                                            <th class="text-center">Staff Name</th>
                                            <th class="text-center">Project Name</th>
                                            <th class="text-center">Budget Head</th>
                                            <th class="text-center">Date</th>
                                            <th class="text-center">Amount</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
									//print_r($payableitemsList);
									if(!empty($payableitemsList)){
										$i=0;
										foreach($payableitemsList as $row){
											$i++;
									?>    
										<tr>
                                            <td align="center">
											<?php
											if($row['payitm_status']=="Pending"){											
											?>
												<input type="checkbox" name="chk_itm<?=$i?>" id="chk_itm<?=$i?>" value="<?=$row['payitm_id']?>" />
											<?php
											}
											?>
											</td>
											<td align="center"><?=$i?></td>
                                            <td align="left"><?=$row['staff_name']?></td>
                                            <td align="left"><?=$row['project_name']?></td>
                                            <td align="left"><?=$row['budget_head_name']?></td>
                                            <td align="left"><?=date("d/m/Y",strtotime($row['payitm_date']))?></td>
                                            <td align="right"><?=number_format($row['payitm_amount'],2)?></td>
                                            <td align="center"><?=$row['payitm_status']?></td>
                                            <td align="center">   
                                                <a href="<?php echo site_url();?>editpayableitems/<?=$row['payitm_id']?>" class="btn btn-success btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="fa fa-pencil"></span></a>                                                    
												<?php
												if($row['payitm_status']=="Pending"){											
												?>      
                                                <a href="javascript:removePaymentItem(<?=$row['payitm_id']?>)" class="btn btn-danger btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Delete"><span class="fa fa-times"></span></a>
												<?php
												}
												?>
                                            </td>
                                        </tr>
									<?php
										}
									} else {
									?>
										<tr>
											<td align="center" colspan="8"><b>No Records Found</b></td>
										</tr>
									<?php
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END DEFAULT DATATABLE -->
    			</div>
    		</div>
    	</div>
    </form>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>	
<?php
if($mode=="add"){
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>category">Payable Items</a></li>
	<li class="active" >Add New Payable Items</li>
</ul>
<div class="page-content-wrap" id="payableitems">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('save_payableitems',$attr); ?>
		<div class="row form-horizontal" style="padding-top:10px;">
			<div class="col-md-12">
				<div class="page-title">
					<div class="page-title">
                        <div class="col-md-6">
                            <h2>Add New Payable Items</h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?=site_url()?>payableitems" class="btn btn-info btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
                        </div>                   
                    </div>
				</div>
				<div class="panel panel-hidden-controls">
					<!-- <div class="panel-heading">
						<h1 class="panel-title">Add New Category</h1>
					</div> -->
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">								
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Project</label>
										<div class="col-md-8">
											<label> : <?=$projectDets['project_name']?></label>
											<input type="hidden" name="payitm_project_id" id="payitm_project_id" value="<?=$projectDets['project_id']?>" />
										</div>
									</div>
								</div>	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Budget Head</label>
										<div class="col-md-8">
											<label> : <?=$budgetDets['budget_head_name']?></label>
											<input type="hidden" name="payitm_budget_id" id="payitm_budget_id" value="<?=$budgetDets['budget_budgethead_id']?>" />
										</div>
									</div>
								</div>	
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Staff</label>
										<div class="col-md-8">
											<label> : <?=$staffDets['staff_name']?></label>
											<input type="hidden" name="payitm_staff_id" id="payitm_staff_id" value="<?=$staffDets['staff_id']?>" />
										</div>
									</div>
								</div>
								
								<div class="col-md-6">                                
                                    <div class="form-group">
										<label class="col-md-4 control-label">Date</label>
										<div class="input-group col-md-8"> 
											<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
											<input type="text" name="payitm_date" id="payitm_date" class="form-control datepicker" value="<?= date("d/m/Y")?>" onchange="changeDate(this.value)">  
										</div>
									</div> 	
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">                                
									<div class="form-group">
										<label class="col-md-4 control-label">Amount</label>
										<div class="input-group col-md-8"> 
											<span class="input-group-addon"><span class="fa fa-inr"></span></span>
											<input type="text" name="payitm_amount" id="payitm_amount" class="form-control" placeholder="Enter Amount" value="<?=$actCost?>">  
										</div>
									</div>	
								</div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Remarks</label>
                                        <div class="col-md-8">
                                            <textarea placeholder="Enter Remarks" class="form-control" name="payitm_remarks" id="payitm_remarks" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>	
								<div class="col-md-12"><br></div>
								<!--<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Payable ID</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="category_order" id="category_order" placeholder="Payable ID"/>
										</div>
									</div>
								</div> -->
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Status</label>
										<div class="col-md-8">
											<select class="form-control select" name="payitm_status" id="payitm_status">
												<option value="Pending">Pending</option>
												<option value="Processed">Processed</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>
                                <div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
                                    <a href="<?=site_url()?>payableitems" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
                                    
                                </div>
							</div>
						</div>
					</div>
				</div>											                                
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
} 
?>
<?php
if($mode=="edit"){
	//print_r($payableitemsDets);	
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>category">Category</a></li>
	<li class="active">Edit Payable Items</li>
</ul>
<div class="page-content-wrap">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('update_payableitems',$attr); ?>
		<div class="row form-horizontal">
			<div class="col-md-12">
				<div class="page-title">
					<h2>Payable Items<small></small></h2>
				</div>
				<div class="panel panel-primary panel-hidden-controls">
					<div class="panel-heading">
						<h1 class="panel-title">Edit Payable Items</h1>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">								
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Project</label>
										<div class="col-md-8">
											<label> : <?=$payableitemsDets['project_name']?></label>
											<input type="hidden" name="payitm_project_id" id="payitm_project_id" value="<?=$payableitemsDets['payitm_project_id']?>" />
											<input type="hidden" name="payitm_id" id="payitm_id" value="<?=$payableitemsDets['payitm_id']?>" />
										</div>
									</div>
								</div>	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Budget Head</label>
										<div class="col-md-8">
											<label> : <?=$payableitemsDets['budget_head_name']?></label>
											<input type="hidden" name="payitm_budget_id" id="payitm_budget_id" value="<?=$payableitemsDets['payitm_budget_id']?>" />
										</div>
									</div>
								</div>	
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Staff</label>
										<div class="col-md-8">
											<label> : <?=$payableitemsDets['staff_name']?></label>
											<input type="hidden" name="payitm_staff_id" id="payitm_staff_id" value="<?=$payableitemsDets['payitm_staff_id']?>" />
										</div>
									</div>
								</div>
								
								<div class="col-md-6">                                
                                    <div class="form-group">
										<label class="col-md-4 control-label">Date</label>
										<div class="input-group col-md-8"> 
											<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
											<input type="text" name="payitm_date" id="payitm_date" class="form-control datepicker" value="<?= date("d/m/Y",strtotime($payableitemsDets['payitm_date']))?>" onchange="changeDate(this.value)">  
										</div>
									</div> 	
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">                                
									<div class="form-group">
										<label class="col-md-4 control-label">Amount</label>
										<div class="input-group col-md-8"> 
											<span class="input-group-addon"><span class="fa fa-inr"></span></span>
											<input type="text" name="payitm_amount" id="payitm_amount" class="form-control" placeholder="Enter Amount" value="<?=$payableitemsDets['payitm_amount']?>">  
										</div>
									</div>	
								</div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Remarks</label>
                                        <div class="col-md-8">
                                            <textarea placeholder="Enter Remarks" class="form-control" name="payitm_remarks" id="payitm_remarks" rows="3"><?=$payableitemsDets['payitm_remarks']?></textarea>
                                        </div>
                                    </div>
                                </div>	
								<div class="col-md-12"><br></div>								
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Status</label>
										<div class="col-md-8">
											<select class="form-control select" name="payitm_status" id="payitm_status">
												<option value="Pending">Pending</option>
												<option value="Processed">Processed</option>
											</select>
											<script>
												setSelectedIndex(document.getElementById('payitm_status'),"<?= $payableitemsDets['payitm_status'] ?>");
											</script>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>
                                <div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
                                    <a href="<?=site_url()?>payableitems" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
                                    
                                </div>
							</div>
						</div>
					</div>
				</div>												
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
}
include "footer.php"; 
?>
