<?php
$main="settings";
$sub="budgethead";

include "header.php";
if($mode=="list"){
	$title="<strong>Budget Head List</strong>";
	//include "titlebar.php";
	?>
<script>
function removebudgethead(id,catID){
	var r = confirm("Do you want to remove this Budget Head..?");
	if (r == true) {
		str="id="+id;	
		url="<?php echo site_url(); ?>delete_budgethead";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				if(data=='deleted'){
					alert("Budget Head Removed Successfully.");
					window.location.href="<?php echo site_url(); ?>budgethead";						
				}
			}		
		});			
	}
}

function editbudgethead(ind){
	var id=$('#budget_head_id'+ind).val();
	var name=$('#budget_head_name'+ind).val();
	$('#budget_head_id').val(id);
	$('#budget_head_name').val(name);
}

</script>
<div id="budgethead">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Budget Head</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->
			<div class="row form-horizontal" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<div class="col-md-6">
							<h2>Budget Head</h2>
						</div>		
					</div>
					<!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <div class="panel-heading">                                
                            <div class="row">
								<?php 
								$attr=array('onsubmit' => 'return checkInputs()');
								echo form_open('save_budgethead',$attr); ?>
									<div class="col-md-2 text-center"></div>
									<div class="col-md-8 text-center">
										<div class="form-group">
											<label class="col-md-4 control-label">Budget Head Name</label>
											<div class="col-md-8">
												<input type="text" class="form-control" name="budget_head_name" id="budget_head_name" placeholder="Enter Budget Head Name" required />
												<input type="hidden" class="form-control" name="budget_head_id" id="budget_head_id" />
											</div>
										</div>
									</div>
									<div class="col-md-2 text-center"></div>
									<div class="col-md-12"><br></div>
									<div class="col-md-12 text-center">
										<button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
																		   
									</div>
								</form>
							</div>                             
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<?php
								$action="budgethead";
								?>
								<?php echo validation_errors(); ?>
								<?php echo form_open($action) ?>							
									<table class="table datatable">
										<thead>
											<tr>
												<th class="text-center">S.No</th>
												<th class="text-center">Budget Head</th>
												<th class="text-center">Action</th>
											</tr>
										</thead>
										<tbody>
										<?php
										//print_r($budgetheadList);
										$len=count($budgetheadList);
										if($len>0){
											$i=0;
											foreach($budgetheadList as $row){
												$i++;
										?>	
											<tr>
												<td align="center"><?=$i?></td>
												<td align="left">
													<?=$row['budget_head_name']?>
													<input type="hidden" class="form-control" name="budget_head_id<?=$i?>" id="budget_head_id<?=$i?>" value="<?=$row['budget_head_id']?>" />
													<input type="hidden" class="form-control" name="budget_head_name<?=$i?>" id="budget_head_name<?=$i?>" value="<?=$row['budget_head_name']?>" />
												</td>
												<td align="center">                                                       
													<a href="javascript:editbudgethead(<?=$i?>)" class="btn btn-success btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="fa fa-pencil"></span></a>                                                          
													<a href="javascript:removebudgethead(<?=$row['budget_head_id']?>)" class="btn btn-danger btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Delete"><span class="fa fa-times"></span></a>
												</td>
											</tr>
										<?php
											}
										} else {
										?>
											<tr>
												<td colspan="3" align="center"><b>No Records Found</b></td>
											</tr>
										<?php
										}
										?>
										</tbody>
									</table>
								</form>
                            </div>
                        </div>
                    </div>
                    <!-- END DEFAULT DATATABLE -->
    			</div>
    		</div>
    	</div>
    
</div>   
<div style="clear:both;"></div>
<?php 
}
?>	
<?php
if($mode=="add"){
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>budgethead">Daily Task</a></li>
	<li class="active" >Add New Daily Task</li>
</ul>
<div class="page-content-wrap" id="budgethead">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('save_budgethead',$attr); ?>
		<div class="row form-horizontal" style="padding-top:10px;">
			<div class="col-md-12">
				<div class="page-title">
					<div class="page-title">
                        <div class="col-md-6">
                            <h2>Add New Budgethead<small></small></h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?=site_url()?>listbudgethead/<?=$catPastID?>" class="btn btn-info btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
                        </div>                   
                    </div>
				</div>
				<div class="panel panel-hidden-controls">
					<!-- <div class="panel-heading">
						<h1 class="panel-title">Add New Budget Head</h1>
					</div> -->
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Name</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="budgethead_name" id="budgethead_name" placeholder="Enter Your Budget Head Name"/>
											<input type="hidden" class="form-control" name="catID" id="catID" value="<?=$catID?>" />
										</div>
									</div>
								</div>	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Designation</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="budgethead_order" id="budgethead_order" placeholder="Enter Your Designation"/>
										</div>
									</div>
								</div>
                                <div class="col-md-12"><br></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Email ID</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="budgethead_order" id="budgethead_order" placeholder="Enter Your Email ID"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Mobile No</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="budgethead_order" id="budgethead_order" placeholder="Enter Your Mobile No"/>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-12"><br></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Username</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="budgethead_order" id="budgethead_order" placeholder="Enter Your Username"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Password</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="budgethead_order" id="budgethead_order" placeholder="Enter Your Password"/>
                                        </div>
                                    </div>
                                </div>									
								<div class="col-md-12"><br></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Address</label>
                                        <div class="col-md-8">
                                            <textarea placeholder="Enter Your Address" class="form-control" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>								
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Status</label>
										<div class="col-md-8">
											<select class="form-control select" name="budgethead_status" id="budgethead_status">
												<option value="Active">Active</option>
												<option value="Inactive">Inactive</option>
											</select>
										</div>
									</div>
								</div>
                                <div class="col-md-12"><br></div>
                                <div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
                                    <?php
                                    if($catID){
                                    ?>
                                    <a href="<?=site_url()?>listbudgethead/<?=$catID?>" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
                                    <?php
                                    } else {
                                    ?>
                                    <a href="javascript:goBack()" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
                                    <?php
                                    }
                                    ?>
                                </div>
							</div>
						</div>
					</div>
				</div>											                                
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
} 
?>
<?php
if($mode=="edit"){
	//print_r($budgethead);
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>budgethead">Budget Head</a></li>
	<li class="active">Edit Budget Head</li>
</ul>
<div class="page-content-wrap">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('update_budgethead',$attr); ?>
		<div class="row form-horizontal">
			<div class="col-md-12">
				<div class="page-title">
					<h2>Budget Head<small></small></h2>
				</div>
				<div class="panel panel-primary panel-hidden-controls">
					<div class="panel-heading">
						<h1 class="panel-title">Edit Budget Head</h1>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Budget Head Name</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="budgethead_name" id="budgethead_name" placeholder="Enter First Name" value="<?=$budgethead['budgethead_name']?>"/>
											<input type="hidden" class="form-control" name="budgethead_id" id="budgethead_id" placeholder="Enter Budget Head Code" value="<?=$budgethead['budgethead_id']?>"/>
											<input type="hidden" class="form-control" name="catID" id="catID" value="<?=$budgethead['budgethead_cat_id']?>"/>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Order Index</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="budgethead_order" id="budgethead_order" placeholder="Enter Order Index" value="<?=$budgethead['budgethead_order']?>"/>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Status</label>
										<div class="col-md-8">
											<select class="form-control select" name="budgethead_status" id="budgethead_status">
												<option value="Active">Active</option>
												<option value="Inactive">Inactive</option>
											</select>
										</div>
										<script>
											setSelectedIndex(document.getElementById('budgethead_status'),"<?= $budgethead['prd_status'] ?>");
										</script>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>				
				
				<div class="panel-footer"  style="text-align:center">
		   			<button type="submit" class="btn btn-warning"><span class="fa fa-check-circle"></span> Save</button> 
					<?php
					if($budgethead['prd_catid']){
					?>
					<a href="<?=site_url()?>listbudgethead/<?=$budgethead['prd_catid']?>" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
					<?php
					} else {
					?>
					<a href="javascript:goBack()" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
					<?php
					}
					?>
		   		</div>                                
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
}
include "footer.php"; 
?>
