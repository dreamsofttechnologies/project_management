<?php
$main="report";
$sub="production";

include "header.php";
if($mode=="list"){
	$title="<strong>Production List</strong>";
	//include "titlebar.php";
	?>
<div id="production">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Production</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->
		<?php echo validation_errors(); ?>
		<?php echo form_open("production") ?>
			<div class="row" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<div class="col-md-6">
							<h2>Production<small></small></h2>
						</div>		
					</div>
					<!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <div class="panel-heading">                                
                            <div class="row">
								<div class="col-md-3">                                
                                    <div class="form-group">
										<label class="control-label">Staff</label>
										<select class="form-control select" data-live-search="true" name="staffID" id="staffID">
											<option value="">Select Staff</option>
											<?php
											foreach($staffList as $staff){
												$sel=""; if($input['staffID']==$staff['staff_id']){ $sel="selected"; }
											?>
											<option value="<?=$staff['staff_id']?>" <?=$sel?>><?=$staff['staff_name']?></option>
											<?php
											}
											?>												
										</select>
									</div> 	
								</div>
								<div class="col-md-3">                                
                                <?php
								$from=date("d/m/Y");
								if($input['fromDate']!=""){
									$from=$input['fromDate'];
								}
								?>
								    <div class="form-group">
										<label class="control-label">From</label>
										<div class="input-group col-md-12"> 
											<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
											<input type="text" name="fromDate" id="fromDate" class="form-control datepicker" value="<?=$from?>">  
										</div>
									</div> 	
								</div>
								<div class="col-md-3">                                
                                 <?php
								$to=date("d/m/Y");
								if($input['toDate']!=""){
									$to=$input['toDate'];
								}
								?>
								    <div class="form-group">
										<label class="control-label">To</label>
										<div class="input-group col-md-12"> 
											<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
											<input type="text" name="toDate" id="toDate" class="form-control datepicker" value="<?=$to?>">  
										</div>
									</div> 	
								</div>
                                <div class="col-md-3 text-center">
									<br>
                                    <button type="submit" class="btn btn-info btn-condensed" style="width:100px;margin:5px 0px 0px 0px;"><span class="fa fa-list-alt"></span> &nbsp;Get</button> 
                                </div>
							</div>                             
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table" style="border-radius:5px;overflow:hidden;">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Project Name</th>
                                            <th colspan="2" class="text-center" style="border-left:1px solid #fff;">Period</th>
                                            <th colspan="2" class="text-center" style="border-left:1px solid #fff;">Estimated</th>
                                            <th colspan="2" class="text-center" style="border-left:1px solid #fff;">Actual</th>
                                            <th class="text-center" style="border-left:1px solid #fff;">Payables</th>
                                            <th class="text-center" style="border-left:1px solid #fff;">Action</th>
                                        </tr>
										<tr>
                                            <th></th>
                                            <th style="border-left:1px solid #fff;text-align:center;">Hrs</th>
                                            <th style="border-left:1px solid #fff;text-align:center;">Rs</th>
                                            <th style="border-left:1px solid #fff;text-align:center;">Hrs</th>
                                            <th style="border-left:1px solid #fff;text-align:center;">Rs</th>
                                            <th style="border-left:1px solid #fff;text-align:center;">Hrs</th>
                                            <th style="border-left:1px solid #fff;border-right:1px solid #fff;text-align:center;">Rs</th>
                                            <th colspan="2"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									//print_r($staffTaskList);
									if(!empty($staffTaskList)){
										$i=0;
										foreach($staffTaskList as $row){
											$i++;
									?>
                                        <tr>
                                            <td align="left"><?=$row['project_name']?></td>
                                            <td align="center"><?=$row['periodTime']?></td>
                                            <td align="right"><?=number_format($row['periodCost'],2)?></td>
                                            <td align="center"><?=$row['estTime']?></td>
                                            <td align="right"><?=number_format($row['estCost'],2)?></td>
                                            <td align="center"><?=$row['actTime']?></td>
                                            <td align="right"><?=number_format($row['actCost'],2)?></td>
                                            <td align="right"><?=number_format($row['periodCost'],2)?></td>
                                            <td align="center">
												<a href="<?php echo site_url();?>addpayableitems/<?=$row['task_project_id']?>/<?=$row['task_budget_id']?>/<?=$row['task_assigned']?>" class="btn btn-info btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Add Payable"><span class="fa fa-plus"></span></a>
											</td>
                                        </tr>
									<?php
										}
									} else {
									?>
										<tr>
											<td align="center" colspan="9"><b>No Records Found</b></td>
										</tr>
									<?php
									}
									?>										
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END DEFAULT DATATABLE -->
    			</div>
    		</div>
    	</div>
    </form>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>	
<?php 
include "footer.php"; 
?>
