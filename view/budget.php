<?php
$main="transaction";
$sub="project";

include "header.php";
if($mode=="list"){
	$title="<strong>Budget List</strong>";
	//include "titlebar.php";
	?>
<script>
function addHead(){
	id=$('#head_id').val();		
	name=$('#head_id option:selected').html();	
	//alert(id+" "+name);
	ind=parseInt($('#hidCount').val())+1;
	$row="";
	$row=$row+'<tr id="row'+ind+'">'
	$row=$row+'<td align="center">'+ind+'</td>'
	$row=$row+'<td align="left">'+name+'<input type="hidden" name="budget_head_id'+ind+'" id="budget_head_id'+ind+'" class="form-control" value="'+id+'"  style="font-size:15px;height:30px;"><input type="hidden" name="budget_id'+ind+'" id="budget_id'+ind+'" class="form-control" value=""></td>'
	$row=$row+'<td align="left"><input type="text" name="budget_per'+ind+'" id="budget_per'+ind+'" class="form-control" value="" placeholder="Enter Percentage" style="font-size:15px;height:30px;" onchange="setAmount('+ind+')"></td>'
	$row=$row+'<td align="left"><input type="text" name="budget_amt'+ind+'" id="budget_amt'+ind+'" class="form-control" value="" placeholder="Enter Amount" style="font-size:15px;height:30px;" onchange="setPer('+ind+')"></td>'
	$row=$row+'<td align="center"><a href="javascript:removeHead('+ind+')" class="btn btn-danger btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Delete"><span class="fa fa-trash-o"></span></a></td>'
	$row=$row+'</tr>';
	$('#hidCount').val(ind);
	if(ind==1){
		$('#budgetHeadList').html($row);
	} else {
		$('#budgetHeadList').append($row);
	}
}

function removeHead(ind){
	$('#row'+ind).empty();
}

function setAmount(ind){
	var amount=$('#projectAmt').val();
	var per=$('#budget_per'+ind).val();
	var amt=0;
	
	if(isNaN(per)==false){
		amt=((amount*per)/100);
	}
	$('#budget_amt'+ind).val(amt);	
}

function setPer(ind){
	var amount=$('#projectAmt').val();
	var per=0;
	var amt=$('#budget_amt'+ind).val();
	
	if(isNaN(per)==false){
		per=((100*amt)/amount);
	}
	$('#budget_per'+ind).val(per.toFixed(2));	
}


</script>
<div id="budget">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Budget</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->		
		<?php echo validation_errors(); ?>
		<?php echo form_open('save_budget') ?>
			<div class="row" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<div class="col-md-6">
							<h2><?=$projectName?> - Budget<small></small></h2>
						</div>
						<div class="col-md-6 text-right">
							<h3>Budget Amount : <?=number_format($projectAmt,2)?><small></small></h3>
						</div>
					</div>
					<!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <div class="panel-heading"> 
							<div class="row">
								<div class="col-md-3">                                
                                    <div class="form-group">
										<label class="control-label">Budget Head</label>
										<input type="hidden" name="projectAmt" id="projectAmt" value="<?=$projectAmt?>" />
										<input type="hidden" name="projectID" id="projectID" value="<?=$projectID?>" />
										<select class="form-control select" name="head_id" id="head_id">
											<option value="">Select Budget Head</option>
											<?php
											foreach($budgetHeadList as $head){
											?>
											<option value="<?=$head['budget_head_id']?>"><?=$head['budget_head_name']?></option>
											<?php
											}
											?>	
										</select>
									</div> 	
								</div>
								<div class="col-md-3">                                
                                    <br>
                                    <button type="button" onclick="addHead()" class="btn btn-info btn-condensed" style="width:100px;margin:5px 0px 0px 0px;"><span class="fa fa-plus"></span> &nbsp;Add</button> 
									<!--<div class="form-group">
										<label class="control-label">Percentage</label>
										<div class="input-group col-md-12"> 
											<span class="input-group-addon"><span class="">%</span></span>
											<input type="text" name="bill_date" id="bill_date" class="form-control" value="" placeholder="Enter Percentage">  
										</div>
									</div> --> 	
								</div>
								<div class="col-md-3">                                
                                    <!--<div class="form-group">
										<label class="control-label">Amount</label>
										<div class="input-group col-md-12"> 
											<span class="input-group-addon"><span class="fa fa-inr"></span></span>
											<input type="text" name="bill_date" id="bill_date" class="form-control" value="" placeholder="Enter Amount">
										</div>
									</div> --> 	
								</div>
                                <div class="col-md-3 text-center">
									<!--<br>
                                    <button type="submit" class="btn btn-info btn-condensed" style="width:100px;margin:5px 0px 0px 0px;"><span class="fa fa-plus"></span> &nbsp;Add</button>  -->
                                </div>
							</div> 	
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                            <?php
							$count=0;
							if(count($budgetList)>0){
								$count=count($budgetList);
							}
							?> 
							    <table class="table" style="border-radius:5px;overflow:hidden;margin:0px;">
                                    <thead>
                                        <tr>
                                            <th class="text-center">S.No<input type="hidden" name="hidCount" id="hidCount" value="<?=$count?>" /></th>
                                            <th class="text-center">Budget Head</th>
                                            <th class="text-center" style="width:200px;">Percentage</th>
                                            <th class="text-center" style="width:150px;">Amount</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="budgetHeadList">
									<?php
									//print_r($budgetList);
									if($count>0){
										$i=0;
										foreach($budgetList as $row){
											$i++;
									?>
										<tr id="row<?=$i?>">
											<td align="center"><?=$i?></td>
											<td align="left">
												<?=$row['budget_head_name']?>
												<input type="hidden" name="budget_head_id<?=$i?>" id="budget_head_id<?=$i?>" class="form-control" style="font-size:15px; height:30px;" value="<?=$row['budget_head_id']?>">
												<input type="hidden" name="budget_id<?=$i?>" id="budget_id<?=$i?>" class="form-control" value="<?=$row['budget_id']?>">
											</td>
											<td align="left">
												<input type="text" name="budget_per<?=$i?>" id="budget_per<?=$i?>" class="form-control" placeholder="Enter Percentage" style="font-size:15px; height:30px;" value="<?=$row['budget_percentage']?>" onchange="setAmount(<?=$i?>)">
											</td>
											<td align="left">
												<input type="h" name="budget_amt<?=$i?>" id="budget_amt<?=$i?>" class="form-control" placeholder="Enter Amount" style="font-size:15px; height:30px;" value="<?=$row['budget_amount']?>" onchange="setPer(<?=$i?>)">
											</td>
											<td align="center"><a href="javascript:removeHead(<?=$i?>)" class="btn btn-danger btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Delete"><span class="fa fa-trash-o"></span></a></td>
										</tr>
									<?php	
										}
									} else {
									?>
										<tr>
											<td align="center" colspan="5"><b>No Records Found</b></td>                                        
										</tr>
									<?php
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
					<div class="row">
						<div class="col-md-12" style="text-align:center;">
							<button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
							<a href="<?=site_url()?>project" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
							
						</div>
						<div class="col-md-12"><br></div>
					</div>
                    <!-- END DEFAULT DATATABLE -->
    			</div>
    		</div>
    	</div>
    </form>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>
<div style="clear:both;"></div>
<?php 
include "footer.php"; 
?>
