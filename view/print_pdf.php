<?php
if($mode=="order"){
	extract($order);
	extract($order_itms);
	extract($cus_det);		
	extract($cus_address);	
	//print_r($compDets);
	extract($compDets);	
	//echo $company['comp_name'];
?>
<style>
body{
	font-family:Arial, Helvetica, sans-serif;
	color:#000000;
}
#header{
	width:100%;
	margin-bottom:10px;
}
#hd_left{
	width:70%;
	float:left;
}
#hd_right{
	width:30%;
	float:left;
}

#title{
	font-size:14px;
	font-weight:bold;
}
#title_txt{
	font-size:10px;
	font-weight:bold;
}
#title_img{
	float:left;
	margin-bottom:10px;
}

#head{
	font-size:20px;
	font-weight:bold;
	padding:12px;
}
#head_o{
	font-size:20px;
	padding:12px;
}
#head_td{
	padding:8px;
}
#head_txtb{
	font-size:16px;
	font-weight:bold;
}
#head_txt{
	font-size:16px;
}

#itm{
	font-size:16px;
	font-weight:bold;
	text-align:center;
}
#itm_sub{
	font-size:12px;
	font-weight:bold;
}
#itm_txt{
	font-size:14px;
	/*font-weight:bold;*/	
	padding:8px;
}

table {
	width:100%;
    border-collapse: collapse;
	margin: 10px 0 10px 0;
}
table, th, td {
    border: 1px solid black;
}
tr{
	height:50px;
}
.header_part{
	margin-bottom:500px;
	width:100%;
}
@page {
	header: html_MyCustomHeader;
	footer: html_MyCustomFooter;
}
</style>
<body style="padding-top:200px;">
	<htmlpageheader name="MyCustomHeader">
		<div id="header" style="border-bottom:1px solid #999999;padding-bottom:5px;">
			<div id="hd_left">
				<div><img id="title_img" src="<?php echo site_url(); ?>images/logo/<?=$set_logo?>" width="200"></div>
			</div>
			<div id="hd_right">
				<div id="title"><?=$set_name?></div>
				<div id="title_txt"><?=$set_address?></div>
				<!--<div id="title_txt">Rahman,</div>
				<div id="title_txt">Kuala Lumpur, Malaysia- 50100.</div>
				<div id="title_txt">Ph : +6016 - 417 1514</div>	 -->											
				<div id="title_txt">Mob : <?=$set_mobileno?></div>												
				<div id="title_txt">
<?=$set_emailid?></div>																				
			</div>							
		</div>		
	</htmlpageheader>
	<div id="header">
		<div id="hd_left">
			<div id="title" style="padding-bottom:5px">Bill To:</div>
			<div id="title"><?=$add_cusname?></div>
			<div id="itm_sub"><?=$add_shipaddress?></div>
			<div id="itm_sub"><?=$add_landmark?></div>
			<div id="itm_sub"><?=$add_city?></div>
			<div id="itm_sub"><?=$add_state?> - <?=$add_zipcode?></div>
			<div id="itm_sub"><?=$add_mobileno?></div>
		</div>	
		<div id="hd_right">
			<div id="title" style="padding-bottom:5px">Bill Details:</div>
			<div id="itm_sub">Order No : #<?=$order_no?></div>
			<div id="itm_sub">Order Date : <?=date('d/m/Y',strtotime($order_date))?></div>
			<div id="itm_sub">Payment Mode : <?=$order_mode?></div>
			<div id="itm_sub">Payment Status : <?=$order_paymentstatus?></div>
		</div>	
	</div>
	<table cellpadding="10" cellspacing="5">
		<tbody>
			<tr>
				<th width="10%" id="itm_sub">S.No</th>
				<th width="32%" id="itm_sub">Product Name</th>
				<th width="13%" id="itm_sub">Size</th>
				<th width="15%" id="itm_sub">Qty</th>
				<th width="15%" id="itm_sub">Price</th>
				<th width="15%" id="itm_sub">Total</th>				
			</tr>
			<?php
			$i=0;
			foreach($order_itms as $row){
				$i++;
				extract($order_siz[$row['cart_sizeid']]);
			?>
			<tr height="50">
				<td align="center" id="itm_txt"><?=$i?></td>
				<td align="left" id="itm_txt"><?=ucfirst($row['cart_prdname'])?> <i>(<?=$row['cart_prdcode']?>)</i></td>
				<td align="center" id="itm_txt"><?=$size_name?></td>
				<td align="center" id="itm_txt"><?=$row['cart_quantity']?></td>
				<td align="right" id="itm_txt"><?=number_format($row['cart_prdprice'],2)?></td>
				<td align="right" id="itm_txt"><?=number_format($row['cart_total_price'],2)?></td>
			</tr>			
			<?php
			}
			?>
			<tr height="50">
				<td align="center" colspan="3"></td>
				<td align="center" id="itm_sub"><?=$order_subtotal?></td>
				<td></td>
				<td align="right" id="itm_sub"><?=number_format($order_total,2)?></td>
			</tr>
			<?php
			if($order_shipping){
			?>			
			<tr height="50">
				<td align="right" colspan="5">Shipping Charge</td>
				<td align="right" id="itm_sub"><?=number_format($order_shipping,2)?></td>
			</tr>			
			<?php
			}
			?>
			<tr height="50">
				<td align="right" colspan="5">Nett Total</td>
				<td align="right" id="itm_sub"><?=number_format($order_nett,2)?></td>
			</tr>			
		</tbody>
	</table>
	<!--<table>
		<tbody>
			<tr>
				<th width="5%">S.No</th>
				<th width="35%">Product Name</th>
				<th width="15%">Size</th>
				<th width="15%">Qty</th>
				<th width="15%">Price (RM)</th>
				<th width="15%">Total (RM)</th>				
			</tr>
			<?php
			/*$i=0;
			foreach($order_itms as $row){
				$i++;
				extract($order_siz[$row['cart_sizeid']]);
			?>
			<tr>
				<td id="itm_txt"><?=$i?></td>
				<td id="itm_txt"><?=ucfirst($cart_prdname)?></td>
				<td id="itm_txt"><?=$size_name?></td>
				<td id="itm_txt"><?=$cart_quantity?></td>
				<td id="itm_txt"><?=number_format($cart_total_price,2)?></td>
			</tr>			
			<?
			}*/
			?>
		</tbody>
	</table> -->
	
	<htmlpagefooter name="MyCustomFooter">
		<div id="header"  style="border-top:1px solid #999999;padding-top:5px;">
			<div align="center">
				<div align="center"><?=$set_name?>, Mob : <?=$set_mobileno?></div>
				<!--<div align="center">Email : sales@dullali.com | www.dullali.com</div> -->
			</div>
			<!--div id="hd_right">
				<div align="right" id="itm_sub">{PAGENO}/{nbpg}</div>
			</div-->				
		</div>			
	</htmlpagefooter> 	
</body>
<?php
}
?>