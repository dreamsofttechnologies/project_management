var morrisCharts = function() {

    Morris.Line({
      element: 'morris-line-example',
      data: [
        { y: $('#tdy7').val(), a: $('#cnt7').val() },
        { y: $('#tdy6').val(), a: $('#cnt6').val() },
        { y: $('#tdy5').val(), a: $('#cnt5').val() },
        { y: $('#tdy4').val(), a: $('#cnt4').val() },
        { y: $('#tdy3').val(), a: $('#cnt3').val() },
        { y: $('#tdy2').val(), a: $('#cnt2').val() },
        { y: $('#tdy1').val(), a: $('#cnt1').val() }
      ],
      xkey: 'y',
      ykeys: ['a'],
      labels: [ 'Patient Visits'],
      resize: true,
      lineColors: ['#95B75D']
    });
	
	 Morris.Line({
      element: 'morris-collection',
      data: [
        { y: $('#tdy7').val(), a: $('#cnt_col7').val() },
        { y: $('#tdy6').val(), a: $('#cnt_col6').val() },
        { y: $('#tdy5').val(), a: $('#cnt_col5').val() },
        { y: $('#tdy4').val(), a: $('#cnt_col4').val() },
        { y: $('#tdy3').val(), a: $('#cnt_col3').val() },
        { y: $('#tdy2').val(), a: $('#cnt_col2').val() },
        { y: $('#tdy1').val(), a: $('#cnt_col1').val() }
      ],
      xkey: 'y',
      ykeys: ['a'],
      labels: [ 'Collection'],
      resize: true,
      lineColors: ['#95B75D']
    });


    
}();