<?php
$main="main";
$sub="banner";

include "header.php";
if($mode=="list"){
	$title="<strong>Banner List</strong>";
	//include "titlebar.php";
	?>
<script>
function removebanner(id){
	var r = confirm("Do you want to remove this Banner..?");
	if (r == true) {
		str="id="+id;	
		url="<?php echo site_url(); ?>delete_banner";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				if(data=='deleted'){
					alert("Banner Removed Successfully.");
					window.location.href="<?php echo site_url(); ?>banner";	
				}
			}		
		});			
	}
}
function rowLimit(v){
	pg=$('#page').val();
	window.location.href="<?php echo site_url(); ?>banner/"+pg+"/"+v;					
}
</script>
<div id="labdiv">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Banner</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->
		<?php echo validation_errors(); ?>
		<?php echo form_open('banner') ?>
			<div class="row" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<h2>Banner<small></small></h2>
					</div>
					<div class="panel panel-hidden-controls">				
						<div class="panel-body">
							<div class="col-md-9">
								<div class="input-group">
									<input class="form-control" id="searching" name="searching" type="text" placehoinstrer="Search here" value="<?=$_REQUEST['searching']?>" >
									<div class="input-group-btn">
										<button type="submit" class="btn btn-info" name="btn_searchid" id="btn_searchid"><i class="fa fa-search"></i></button>
									</div>
								</div>                                            
							</div>						
							<div class="pull-right" style="margin-right:20px;">
								<a href="<?php echo site_url();?>addbanner" class="btn btn-warning pull-right" style="margin-right:0px;"><span class="fa fa-plus"></span> Add New Banner</a> 
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-primary">
									<div class="panel-body panel-body-table">
										<div class="table-responsive1">										
											<table class="table table-hover table-striped table-actions" >
												<thead>													
													<tr>
														<!--th class="table_header"><input type="checkbox" id="main_chk" name="main_chk" class="" onClick="javascript: selectAll('banner_check')"/></th-->
														<th class="table_header text-center">S.No. <input type="hidden" name="hidCount" id="hidCount" value="<?=$total_rows?>" /><input type="hidden" name="st_from" id="st_from" value="<?=$pg?>" /> </th>
														<th class="table_header text-center">Banner Name</th>																						
														<th class="table_header text-center">Status</th>
														<th class="table_header text-center">Action</th>
													</tr>
												</thead>
												<tbody id="tbl_dataload">
												<?php												
												$len=count($banner);
												//print_r($banner);
												if($len>0){
													foreach ($banner as $b):
														$j++;
														//print_r($banner);
														//print_r($unit);
													?>
													<tr>														
														<td align="center"><?=$j?></td>
														<td align="center"><?=$b['banner_name']?></td>														
														<td align="center"><?=$b['banner_status']?></td>														
														<td align="center">																														
															<a href="<?php echo site_url();?>editbanner/<?=$b['banner_id']?>" class="btn btn-success btn-sm btn-condensed" data-toogle="tooltip" title="Edit"><span class="fa fa-pencil"></span></a>															
															<a href="javascript:removebanner(<?=$b['banner_id']?>)" class="btn btn-danger btn-sm btn-condensed" data-toogle="tooltip" title="Delete"><span class="fa fa-times"></span></a>																
														</td>
													</tr>
													<?php 	
													endforeach; 														
												}else {	 
													?>	
													<tr >
														<td align="center" colspan="8"><strong><br />No Records Found<br /><br /></strong></td>
													</tr>
												<?php	
												}
												?>	
												</tbody>
											</table>
											<div class="panel-footer">
												<div class="col-md-12 ">																						
													<input type="hidden" name="rows" id="rows" value="<?=$row_limit?>" />
													<input type="hidden" name="page" id="page" value="<?=$pg?>" />
													<div class="col-md-3 text-left">
														<label> Rows Limit: &nbsp;
															<select name="row_show" id="row_show" onChange="javascript: rowLimit(this.value)" >
																<option value="<?=$def_rows?>" <?php if($def_rows==$rows){ echo 'selected="selected"'; }?>><?=$def_rows?></option>
																<option value="<?=($def_rows*2)?>" <?php if(($def_rows*2)==$rows){ echo 'selected="selected"'; }?>><?=($def_rows*2)?></option>
																<option value="<?=($def_rows*4)?>" <?php if(($def_rows*4)==$rows){ echo 'selected="selected"'; }?>><?=($def_rows*4)?></option>
															</select>
														</label>
													</div>
													<div class="col-md-7 text-center">
													<?php	
													
													if (!$pg) { 
														 $pagenum = 1; 
													} else {
														$pagenum = intval($pg/$row_limit)+1; 		
													}
			
													//echo "<br>".$total_rows."===".$row_limit."===".$pagenum;									
													$last = ceil($total_rows/$row_limit); 
													//this makes sure the page number isn't below one, or more than our maximum pages 
													if ($pagenum < 1) { 
														$pagenum = 1; 
													} else if ($pagenum > $last)  { 
														$pagenum = $last; 
													}
													$lower_limit = ($pagenum - 1) * $row_limit;
													//echo "<br>".$pagenum."===".$lower_limit."====".$last;
	
													$st_lp="";
													$ed_lp="";
													if ( ($pagenum-1) > 0) {
													?>	
													
													<a href="<?=site_url().'banner/0/'.$rows?>" class="btn btn-default active">First</a>
													<a href="<?=site_url().'banner/'.(($pagenum-2)*$row_limit).'/'.$rows?>" class="btn btn-default active">Previous</a>
													<?php
													}
													if($pagenum>=5){
														$st_lp=$pagenum-3;
														if($last>$pagenum+3){
															$ed_lp=$pagenum+3;
														} else if($pagenum<=$last && $pagenum>$last-4){
																$st_lp=$last-4;
																$ed_lp=$last;
															} else {
																$ed_lp=$last;
																}
													} else {
														$st_lp=1;
														if($last>4){
															$ed_lp=4;
														} else {
															$ed_lp=$last;
														}
													}
													//echo "<br>".$st_lp." == ".$ed_lp;
													
													//Show page links
													//$a=0;
													for($i=$st_lp; $i<=$ed_lp; $i++) {													
														if ((($i-1)*$row_limit) == $pg) {
												?>
													<a class="btn btn-primary" href="<?=site_url().'banner/'.(($i-1)*$row_limit).'/'.$rows?>"><?php echo $i ?></a>
												<?php		} else {  ?>
													<a href="<?=site_url().'banner/'.(($i-1)*$row_limit).'/'.$rows?>" class="btn btn-default active"><?php echo $i ?></a>
												<?php 
														}
													}
													 
													if ( ($pagenum+1) <= $last) {
												?>
													<a href="<?=site_url() . 'banner/'.(($pagenum)*$row_limit).'/'.$rows?>" class="btn btn-default active">Next</a>
												<?php 	} 
													if ( ($pagenum) != $last) { 
												?>	
													<a href="<?=site_url() . 'banner/'.(($last-1)*$row_limit).'/'.$rows?>" class="btn btn-default active">Last</a>
												<?php
													} 
												?>
													</div>
													<div class="col-md-2 text-right">
														Page <?php echo $pagenum; ?> of <?php echo $last; ?>														
													</div>													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>					
				</div>
			</div>
		</div>
	</div>
</form>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>	
<script>
$(document).ready(function() {	
	$('#banner_mobile1').on('keypress', function(key) {
        if((key.charCode < 48 || key.charCode > 57) && key.charCode != 43 && key.charCode > 31) {
			return false;
		}
    });
	$('#banner_mobile2').on('keypress', function(key) {
        if((key.charCode < 48 || key.charCode > 57) && key.charCode != 43 && key.charCode > 31) {
			return false;
		}
    });
});
</script>

<?php
if($mode=="add"){
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>banner">Banner</a></li>
	<li class="active" >Add New Banner</li>
</ul>
<div class="page-content-wrap">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('save_banner',$attr); ?>
		<div class="row form-horizontal">
			<div class="col-md-12">
				<div class="page-title">
					<h2>Banner<small></small></h2>
				</div>
				<div class="panel panel-primary panel-hidden-controls">
					<div class="panel-heading">
						<h1 class="panel-title">Add New Banner</h1>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Banner Name</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="banner_name" id="banner_name" placehoinstrer="Enter Banner Name"/>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label"> Image</label>
										<div class="col-md-8" id="fileInput">
											<input type="file" class="fileinput" name="banner_image" id="banner_image" /><br/>
										</div>
									</div>
								</div>								
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Banner Link</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="banner_link" id="banner_link" placehoinstrer="Enter Banner Link"/>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Status</label>
										<div class="col-md-8">
											<select class="form-control select" name="banner_status" id="banner_status">
												<option value="Active">Active</option>
												<option value="Inactive">Inactive</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>											
				<div class="panel-footer" style="text-align:center">		   			
					<button type="submit" class="btn btn-warning"><span class="fa fa-check-circle"></span> Save</button> 
					<a href="javascript:goBack()" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
		   		</div>                                
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
} 

if($mode=="edit"){
	//print_r($banner);
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>banner">Banner</a></li>
	<li class="active">Edit Banner</li>
</ul>
<div class="page-content-wrap">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('update_banner',$attr); ?>
		<div class="row form-horizontal">
			<div class="col-md-12">
				<div class="page-title">
					<h2>Banner<small></small></h2>
				</div>
				<div class="panel panel-primary panel-hidden-controls">
					<div class="panel-heading">
						<h1 class="panel-title">Edit Banner</h1>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Banner Name</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="banner_name" id="banner_name" placehoinstrer="Enter First Name" value="<?=$banner['banner_name']?>"/>
											<input type="hidden" class="form-control" name="banner_id" id="banner_id" placehoinstrer="Enter Banner Code" value="<?=$banner['banner_id']?>"/>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label"> Image</label>
										<div class="col-md-8" id="fileInput">
											<input type="file" class="fileinput" name="banner_image" id="banner_image" /><br/>
											<?php
											if($banner['banner_image']!=""){
											?>											
											<div class="col-md-12">
												<img src="<?php echo site_url().'images/banner/'.$banner['banner_image']; ?>" style="width:250px; padding:5px"/><br />
												<a href="javascript:removeCatImage(<?=$banner['banner_id']?>)" >Remove</a>
											</div>
											<?php
											}
											?>
										</div>
									</div>
								</div>								
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Banner Link</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="banner_link" id="banner_link" placehoinstrer="Enter Banner Link" value="<?=$banner['banner_link']?>" />
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Status</label>
										<div class="col-md-8">
											<select class="form-control select" name="banner_status" id="banner_status">
												<option value="Active">Active</option>
												<option value="Inactive">Inactive</option>
											</select>
										</div>
										<script>
											setSelectedIndex(document.getElementById('banner_status'),"<?= $banner['banner_status'] ?>");
										</script>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>				
				
				<div class="panel-footer"  style="text-align:center">
		   			<button type="submit" class="btn btn-warning"><span class="fa fa-check-circle"></span> Save</button> 
					<a href="javascript:goBack()" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
		   		</div>                                
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<script>
function removeCatImage(id){
	var r = confirm("Do you want to remove this Image..?");
	if (r == true) {
		str="id="+id;	
		url="<?php echo site_url(); ?>banner_imageremove";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				//alert(data);
				if(data==1){
					alert("Image Removed Successfully.");					
					window.location.href="<?php echo site_url(); ?>banner";	
				}
				
			}		
		});			
	}
}
</script>
<?php 
}

include "footer.php"; 
?>
