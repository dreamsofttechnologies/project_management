<?php
$main="master";
$sub="client";

include "header.php";
if($mode=="list"){
	$title="<strong>Client List</strong>";
	//include "titlebar.php";
	?>
<script>
function removeClient(id,catID){
	var r = confirm("Do you want to remove this Client..?");
	if (r == true) {
		str="id="+id;	
		url="<?php echo site_url(); ?>delete_client";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				if(data=='deleted'){
					alert("Client Removed Successfully.");
					window.location.href="<?php echo site_url(); ?>client";						
				}
			}		
		});			
	}
}
</script>
<div id="client">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Client</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->		
		<?php echo validation_errors(); ?>
		<?php echo form_open($action) ?>
			<div class="row" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<div class="col-md-6">
							<h2>Client<small></small></h2>
						</div>
						<div class="col-md-6 text-right">
							<a href="<?php echo site_url();?>addclient" class="btn btn-info btn-condensed"> <span class="fa fa-plus"></span> Add New Client</a> 
						</div>
						
					</div>
					<!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <div class="panel-heading">                                                            
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table datatable">
                                    <thead>
                                        <tr>
                                            <th class="text-center">S.No</th>
                                            <th class="text-center">Client Code</th>
                                            <th class="text-center">Client Name</th>
                                            <th class="text-center">Location</th>
                                            <th class="text-center">Type</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
									//print_r($clientList);
									$len=count($clientList);
									if($len>0){
										$i=0;
										foreach($clientList as $row){
											$i++;
									?>   <tr>
                                            <td align="center"><?=$i?></td>
                                            <td align="center"><?=$row['customer_code']?></td>
                                            <td align="left"><?=$row['customer_name']?></td>
                                            <td align="center"><?php echo $row['customer_address'].", ".$row['customer_city'].", ".$row['customer_country']; ?></td>
                                            <td align="left"><?=$row['customer_type']?></td>
                                            <td align="center"><?=$row['customer_status']?></td>
                                            <td align="center">
                                                <a href="<?php echo site_url();?>task/<?=$row['customer_id']?>" class="btn btn-info btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Project"><span class="fa fa-list-alt"></span></a>                                                          
                                                <a href="<?php echo site_url();?>editclient/<?=$row['customer_id']?>" class="btn btn-success btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="fa fa-pencil"></span></a>                                                          
                                                <a href="javascript:removeClient(<?=$row['customer_id']?>)" class="btn btn-danger btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Delete"><span class="fa fa-times"></span></a>
                                            </td>
                                        </tr>
									<?php
										}
									} else {
									?>
										<tr>
											<td colspan="8" align="center"><b>No Records Found</b></td>
										</tr>
									<?php
									}
									?>	
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END DEFAULT DATATABLE -->
    			</div>
    		</div>
    	</div>
    </form>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>	
<?php
if($mode=="add"){
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>client">Client</a></li>
	<li class="active" >Add New Client</li>
</ul>
<div class="page-content-wrap" id="client">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('save_client',$attr); ?>
		<div class="row form-horizontal" style="padding-top:10px;">
			<div class="col-md-12">
				<div class="page-title">
					<div class="page-title">
                        <div class="col-md-6">
                            <h2>Add New Client<small></small></h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?=site_url()?>client" class="btn btn-info btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
                        </div>                   
                    </div>
				</div>
				<div class="panel panel-hidden-controls">
					<!-- <div class="panel-heading">
						<h1 class="panel-title">Add New Client</h1>
					</div> -->
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Client Name <span style="color:#FF0000">*</span></label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="customer_name" id="customer_name" placeholder="Enter Client Name" required />										
										</div>
									</div>
								</div>	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Client Code <span style="color:#FF0000">*</span></label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="customer_code" id="customer_code" placeholder="Enter Client Code" required value="<?=$clientCode?>" />
										</div>
									</div>
								</div>
                                <div class="col-md-12"><br></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Email ID</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="customer_email" id="customer_email" placeholder="Enter Client Email ID"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Mobile No</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="customer_mobile" id="customer_mobile" placeholder="Enter Client Mobile No"/>
                                        </div>
                                    </div>
                                </div>	
								<div class="col-md-12"><br></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">GST No</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="customer_gstno" id="customer_gstno" placeholder="Enter Client GST No"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Address</label>
                                        <div class="col-md-8">
                                            <textarea placeholder="Enter Client Address" class="form-control" name="customer_address" id="customer_address" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>	
								<div class="col-md-12"><br></div>								
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">City</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="customer_city" id="customer_city" placeholder="Enter Client City"/>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Country</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="customer_country" id="customer_country" placeholder="Enter Client Country"/>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Remarks</label>
                                        <div class="col-md-8">
                                            <textarea placeholder="Enter Client Remarks" class="form-control" name="customer_remarks" id="customer_remarks" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Type</label>
										<div class="col-md-8">
											<select class="form-control select" name="customer_type" id="customer_type">
												<option value="Customer">Customer</option>
												<option value="Partner">Partner</option>
											</select>
										</div>
									</div>
								</div>	
                                <div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Status</label>
										<div class="col-md-8">
											<select class="form-control select" name="customer_status" id="customer_status">
												<option value="Active">Active</option>
												<option value="Inactive">Inactive</option>
											</select>
										</div>
									</div>
								</div>
                                <div class="col-md-12"><br></div>
								<div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
                                    <a href="<?=site_url()?>client" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
                                   
                                </div>
							</div>
						</div>
					</div>
				</div>											                                
			</div>
		</div>
	</form>
</div>
<?php 
} 
?>
<?php
if($mode=="edit"){
	//print_r($client);
	extract($clientDets);
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>client">Client</a></li>
	<li class="active">Edit Client</li>
</ul>
<div class="page-content-wrap">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('update_client',$attr); ?>
		<div class="row form-horizontal">
			<div class="col-md-12">
				<div class="page-title">
					<h2>Client<small></small></h2>
				</div>
				<div class="panel panel-primary panel-hidden-controls">
					<div class="panel-heading">
						<h1 class="panel-title">Edit Client</h1>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Client Name <span style="color:#FF0000">*</span></label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="customer_name" id="customer_name" placeholder="Enter Client Name" value="<?=$customer_name?>" required />										
											<input type="hidden" class="form-control" name="customer_id" id="customer_id" value="<?=$customer_id?>" />										
										</div>
									</div>
								</div>	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Client Code <span style="color:#FF0000">*</span></label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="customer_code" id="customer_code" placeholder="Enter Client Code" required value="<?=$customer_code?>" />
										</div>
									</div>
								</div>
                                <div class="col-md-12"><br></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Email ID</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="customer_email" id="customer_email" placeholder="Enter Client Email ID" value="<?=$customer_email?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Mobile No</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="customer_mobile" id="customer_mobile" placeholder="Enter Client Mobile No" value="<?=$customer_mobile?>"/>
                                        </div>
                                    </div>
                                </div>	
								<div class="col-md-12"><br></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">GST No</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="customer_gstno" id="customer_gstno" placeholder="Enter Client GST No" value="<?=$customer_gstno?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Address</label>
                                        <div class="col-md-8">
                                            <textarea placeholder="Enter Client Address" class="form-control" name="customer_address" id="customer_address" rows="3"><?=$customer_address?></textarea>
                                        </div>
                                    </div>
                                </div>	
								<div class="col-md-12"><br></div>								
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">City</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="customer_city" id="customer_city" placeholder="Enter Client City" value="<?=$customer_city?>"/>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Country</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="customer_country" id="customer_country" placeholder="Enter Client Country" value="<?=$customer_country?>"/>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Remarks</label>
                                        <div class="col-md-8">
                                            <textarea placeholder="Enter Client Remarks" class="form-control" name="customer_remarks" id="customer_remarks" rows="3"><?=$customer_remarks?></textarea>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Type</label>
										<div class="col-md-8">
											<select class="form-control select" name="customer_type" id="customer_type">
												<option value="Customer">Customer</option>
												<option value="Partner">Partner</option>
											</select>
										</div>
										<script>
											setSelectedIndex(document.getElementById('customer_type'),"<?=$customer_type?>");
										</script>
									</div>
								</div>	
                                <div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Status</label>
										<div class="col-md-8">
											<select class="form-control select" name="customer_status" id="customer_status">
												<option value="Active">Active</option>
												<option value="Inactive">Inactive</option>
											</select>
											<script>
												setSelectedIndex(document.getElementById('customer_status'),"<?=$customer_status?>");
											</script>
										</div>
									</div>
								</div>
                                <div class="col-md-12"><br></div>
								<div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
                                    <a href="<?=site_url()?>client" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 
                                   
                                </div>
							</div>
						</div>
					</div>
				</div>												
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
}
include "footer.php"; 
?>
