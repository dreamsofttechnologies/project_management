<?php
$main="transaction";
$sub="project";

include "header.php";
if($mode=="list"){
	$title="<strong>Invoice List</strong>";
	//include "titlebar.php";
	?>
<script>
function removeinvoice(id,catID){
	var r = confirm("Do you want to remove this Invoice..?");
	if (r == true) {
		str="id="+id;	
		url="<?php echo site_url(); ?>delete_invoice";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				if(data=='deleted'){
					alert("Invoice Removed Successfully.");
					window.location.href="<?php echo site_url(); ?>invoice";						
				}
			}		
		});			
	}
}
function rowLimit(v){
	pg=$('#page').val();
	window.location.href="<?php echo site_url(); ?>invoice/"+pg+"/"+v;					
}
</script>
<div id="invoice">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Invoice</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->		
		<?php echo validation_errors(); ?>
		<?php echo form_open("invoice") ?>
			<div class="row" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<div class="col-md-6">
							<h2><?=$projectName?> - Invoice<small></small></h2>
						</div>
						<div class="col-md-6 text-right">
							<a href="<?php echo site_url();?>addinvoice/<?=$projectID?>" class="btn btn-info btn-condensed"> <span class="fa fa-plus"></span> Add New Invoice</a> 
							<a href="<?=site_url()?>project" class="btn btn-danger btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
						</div>								
					</div>
					<!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <div class="panel-heading">                                                            
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table datatable">
                                    <thead>
                                        <tr>
                                            <th class="text-center">S.No</th>
                                            <th class="text-center">Invoice No</th>
                                            <th class="text-center">Date</th>
                                            <th class="text-center">Invoice Title</th>
                                            <th class="text-center">Client</th>
                                            <th class="text-center">Invoice Amount</th>
                                            <th class="text-center">Paid Amount</th>
                                            <th class="text-center">Balance Amount</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
									//print_r($invoiceList);
									$count=count($invoiceList);
									if(!empty($invoiceList)){
										$i=0;
										foreach($invoiceList as $row){
											$i++;
									?>    <tr>
                                            <td align="center"><?=$i?></td>
                                            <td align="center"><b>#IN<?=date("y",strtotime("-1 years",strtotime($row['invoice_date'])))?><?=date("y",strtotime($row['invoice_date']))?>-<?=$row['invoice_no']?></b></td>
                                            <td align="center"><?=date("d/m/Y",strtotime($row['invoice_date']))?></td>
                                            <td align="left"><?=$row['invoice_title']?></td>
                                            <td align="left"><?=$row['customer_name']?></td>
                                            <td align="right"><?=number_format($row['invoice_nett_amount'],2)?></td>
                                            <td align="right"><?=number_format($row['paid'],2)?></td>
                                            <td align="right"><?=number_format(($row['invoice_nett_amount']-$row['paid']),2)?></td>
                                            <td align="center"><?=$row['invoice_status']?></td>
                                            <td align="center" style="padding:5px 0px;">
												<div>
													<a href="<?php echo site_url();?>task/<?=$row['invoice_id']?>" class="btn btn-info btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Print"><span class="glyphicon glyphicon-print"></span></a>                                                          
													<a href="<?php echo site_url();?>paymentreceived/<?=$row['invoice_id']?>" class="btn btn-info btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Payment Received"><span class="fa fa-money"></span></a>
												</div> 
												<div style="padding-top:5px;">	
													<a href="<?php echo site_url();?>editinvoice/<?=$row['invoice_id']?>" class="btn btn-success btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="fa fa-pencil"></span></a>                                                          
													<a href="javascript:removeinvoice(<?=$row['invoice_id']?>)" class="btn btn-danger btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Delete"><span class="fa fa-times"></span></a>
												</div> 
                                            </td>
                                        </tr>
									<?php	
										}
									} else {
									?>
										<tr>
                                            <td align="center" colspan="10"><b>No Records Found</b></td>                                        
										</tr>
									<?php
									}
									?>   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END DEFAULT DATATABLE -->
    			</div>
    		</div>
    	</div>
    </form>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>	
<script>
function addRow(){
	var ind=parseInt($('#hidCount').val())+1;
	//alert(ind);
	var opts=document.getElementById('invoiceitm_title1').innerHTML;
	//alert(opts);
	var row="";
	row=row+'<tr id="tblRow'+ind+'">'
	row=row+'<td>'+ind+'</td>'
	row=row+'<td><select class="form-control select" data-live-search="true" name="invoiceitm_title'+ind+'" id="invoiceitm_title'+ind+'" onchange="getServiceDet('+ind+')">'+opts+'</select><br /><br /><textarea class="form-control" id="invoiceitm_description'+ind+'" name="invoiceitm_description'+ind+'" placeholder="Enter Item Description" value="" rows="3"></textarea></td>'
	row=row+'<td><input class="form-control"  type="text" style="margin-bottom:10px" id="invoiceitm_rate'+ind+'" name="invoiceitm_rate'+ind+'" placeholder="Rate" onchange="onRateChange('+ind+')"><input class="form-control"  type="text" style="margin-bottom:10px" id="invoiceitm_tax'+ind+'" name="invoiceitm_tax'+ind+'" placeholder="Tax (%)" onchange="onRateChange('+ind+')"><input class="form-control"  type="text"  id="invoiceitm_discount'+ind+'" name="invoiceitm_discount'+ind+'" placeholder="Discount" onchange="onRateChange('+ind+')"></td>'
	row=row+'<td align="right"><label id="txt_amt'+ind+'" name="txt_amt'+ind+'">0.00</label><input class="form-control" size="7" type="hidden" id="invoiceitm_amount'+ind+'" name="invoiceitm_amount'+ind+'"></td>'
	row=row+'<td><a href="javascript: addRow()" class="btn btn-success btn-condensed fa fa-plus"></a>&nbsp;<a href="javascript: removeRow('+ind+')" class="btn btn-danger btn-condensed fa fa-times"></a></td>'
	row=row+'</tr>';
	
	$('#hidCount').val(ind);
	$('#rowList').append(row);	
	$('.select').selectpicker('refresh');
}

function removeRow(){
	$('#tblRow'+ind).empty(opts);
}

function getServiceDet(ind){
	var serviceID=$('#invoiceitm_title'+ind).val();
	var service=($('#service'+serviceID).val()).split('|');
	
	$('#invoiceitm_description'+ind).val(service[2]);
	$('#invoiceitm_rate'+ind).val(service[3]);	
	onRateChange(ind);
}

function onRateChange(ind){
	var rate=parseFloat($('#invoiceitm_rate'+ind).val());
	var tax=parseInt($('#invoiceitm_tax'+ind).val());
	var disc=parseInt($('#invoiceitm_discount'+ind).val());
	
	var taxRate = 0;
	if(isNaN(tax)==false){
		taxRate = ((rate*tax)/100);
	}
	//alert(taxRate);
	
	var discRate = 0;
	if(isNaN(disc)==false){
		discRate = disc;
	}
	
	var total=rate+taxRate-discRate;
	$('#invoiceitm_amount'+ind).val(total);
	$('#txt_amt'+ind).html(total.toFixed(2));
	
	calc();
}

function calc(){
	var len=parseInt($('#hidCount').val());
	var totRate=0;
	var totTax=0;
	var totDisc=0;
	var totAmt=0;
	for(ind=1;ind<=len;ind++){
		var rate=parseFloat($('#invoiceitm_rate'+ind).val());
		var tax=parseInt($('#invoiceitm_tax'+ind).val());
		var disc=parseInt($('#invoiceitm_discount'+ind).val());
		var total=parseInt($('#invoiceitm_amount'+ind).val());
		
		if(isNaN(rate)==false){
			totRate=totRate+rate;
		}
		
		if(isNaN(tax)==false){
			totTax=totTax+((rate*tax)/100);
		}
		
		if(isNaN(disc)==false){
			totDisc=totDisc+disc;
		}
		
		if(isNaN(total)==false){
			totAmt=totAmt+total;
		}
	}
	
	$('#fn_gross').val(totRate);
	$('#fin_gross').html(totRate.toFixed(2));
	
	$('#fn_tax').val(totTax);
	$('#fin_tax').html(totTax.toFixed(2));
	
	$('#fn_dis').val(totDisc);
	$('#fin_dis').html(totDisc.toFixed(2));
	
	$('#fn_net').val(totAmt);
	$('#fin_net').html(totAmt.toFixed(2));
}
</script>
<?php
if($mode=="add"){
	?>
<ul class="breadcrumb">
    <li><a href="<?php echo site_url();?>dashboard">Home</a></li>
    <li><a href="<?php echo site_url();?>invoice">Invoice</a></li>
    <li class="active">Add New Invoice</li>
</ul>
<div class="page-content-wrap" id="invoice">
    <?php echo validation_errors(); ?>
    <?php 
        $attr=array('onsubmit' => 'return checkInputs()');
        echo form_open_multipart('save_invoice',$attr); ?>
    <div class="row form-horizontal" style="padding-top:10px;">
        <div class="col-md-12">
            <div class="page-title">
                <div class="page-title">
                    <div class="col-md-6">
                        <h2>Add New Invoice</h2>
                    </div>
                    <div class="col-md-6 text-right">
                        <a href="<?=site_url()?>invoice/<?=$projectID?>" class="btn btn-info btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="panel" style="border:1px solid #e1e1e1;border-radius:5px;">
                    <div class="panel-heading">
                        <h3 class="panel-title" style="color:#fff;">Client Details </h3>
                    </div>
                    <div class="panel-body">
                    	<div class="col-md-12">
                       		<div class="form-group">
                           		<label class="col-md-4">Project</label>
                                <div class="col-md-8">
                               		<label> : <?=$projectDets['project_name']?></label>
									<input type="hidden" name="invoice_project_id" id="invoice_project_id" value="<?=$projectDets['project_id']?>"> 
                                </div>
                           	</div>
						   	<div class="form-group">
                                <label class="col-md-4">Client Name</label>
                                <div class="col-md-8">
                                    <label> : <?=$projectDets['customer_name']?></label>
									<input type="hidden" name="invoice_client_id" id="invoice_client_id" value="<?=$projectDets['customer_id']?>">
                                </div>
                            </div>                            							
                            <div class="form-group">
                                <label class="col-md-4">Status</label>
                                <div class="col-md-8">
                                    <select class="form-control select" id="invoice_status" name="invoice_status" data-live-search="true" >
                                        <option value="">Select One</option>
                                        <option value="Draft">Draft</option>
                                        <option value="Confirmed">Confirmed</option>
                                        <option value="Reccuring">Reccuring</option>
                                        <option value="Completed">Completed</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="panel" style="border:1px solid #e1e1e1;border-radius:5px;">
                    <div class="panel-heading">
                        <h3 class="panel-title" style="color:#fff;">Sales Invoice Details </h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-5 ">Invoice No</label>
                                <div class="input-group col-md-7"> 
									<label><b>#IN<?=date("y",strtotime("-1 years"))?><?=date("y")?>-<?=$invoice_no?></b></label>
                                    <input type="hidden" class="form-control" name="invoice_no" id="invoice_no" value="<?=$invoice_no?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-5">Invoice Date</label>
                                <div class="input-group col-md-7"> 
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    <input type="text" name="invoice_date" id="invoice_date" class="form-control datepicker" value="<?=date("d/m/Y")?>">  
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h2>Invoice Item</h2>
                    </div>
                    <div class="pull-right">
                        <button type="button" class="btn  btn-info" onclick="addRow()"  style="margin-right:10px;"><span class="fa fa-plus"></span> Add New Row</button> 
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-md-2" style="font-weight:600;padding-top:3px;">Invoice Title</label>
                        <div class="col-md-10"> 
                            <input class="form-control" id="invoice_title" name="invoice_title" type="text" placeholder="Enter Invoice Title">
                        </div>
                    </div>
                    <div class="panel panel-primary" style="border-top:none;border-radius:5px;">
                        <div class="panel-body panel-body-table">
							<div class="row">							
								<table class="table table-bordered table-striped table-actions" style="text-align:center;" id="dynamicTable">
									<thead>
										<tr>
											<th style="text-align:center;width:2%">S.No</th>                                        
											<th style="text-align:center;width:45%">Item Title</th>
											<th style="text-align:center;width:15%"><input type="hidden" name="hidCount" id="hidCount" value="1"><span class="fa fa-rupee"></span>Rate</th>
											<th style="text-align:center;width:13%"><span class="fa fa-rupee"></span>Amount</th>
											<th style="text-align:center;width:10%;">Actions</th>
										</tr>
									</thead>
									<tbody id="rowList">
									<?php
									$i=1;
									?>
										<tr id="tblRow<?=$i?>">
											<td><?=$i?></td>
											<td>
											   <select class="form-control select" data-live-search="true" name="invoiceitm_title<?=$i?>" id="invoiceitm_title<?=$i?>" onchange="getServiceDet(<?=$i?>)">
													<option value="">Select Service</option>
													<?php
													foreach($serviceList as $service){
													?>
													<option value="<?=$service['service_id']?>"><?=$service['service_name']?></option>
													<?php
													}
													?>
												</select>
												<br /><br />
												<textarea class="form-control" id="invoiceitm_description<?=$i?>" name="invoiceitm_description<?=$i?>" placeholder="Enter Item Description" value="" rows="3"></textarea>
											</td>
											<td>
												<input class="form-control"  type="text" style="margin-bottom:10px" id="invoiceitm_rate<?=$i?>" name="invoiceitm_rate<?=$i?>" placeholder="Rate" onchange="onRateChange(<?=$i?>)">
												<input class="form-control"  type="text" style="margin-bottom:10px" id="invoiceitm_tax<?=$i?>" name="invoiceitm_tax<?=$i?>" placeholder="Tax (%)" onchange="onRateChange(<?=$i?>)">
												<input class="form-control"  type="text"  id="invoiceitm_discount<?=$i?>" name="invoiceitm_discount<?=$i?>" placeholder="Discount" onchange="onRateChange(<?=$i?>)">
											</td>
											<td align="right">
												<label id="txt_amt<?=$i?>" name="txt_amt<?=$i?>">0.00</label>
												<input class="form-control" size="7" type="hidden" id="invoiceitm_amount<?=$i?>" name="invoiceitm_amount<?=$i?>">
											</td>
											<td>
												<a href="javascript: addRow()" class="btn btn-success btn-condensed fa fa-plus"></a>
												<a href="javascript: removeRow(<?=$i?>)" class="btn btn-danger btn-condensed fa fa-times"></a>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <h3>Notes</h3>
                            <textarea class="form-control" id="invoice_note" name="invoice_note" placeholder="Enter Your Notes" rows="3"></textarea>
							<?php
							foreach($serviceList as $service){
								$list=$service['service_id']."|".$service['service_name']."|".$service['service_description']."|".$service['service_amount'];
							?>
							<input type="hidden" class="form-control" name="service<?=$service['service_id']?>" id="service<?=$service['service_id']?>" value="<?=$list?>"/>
							<?php
							}
							?>
                        </div>
                        <div class="col-md-6">
                            <div class="panel-body panel-body-table">
                                <table class="table table-bordered table-striped table-actions" style="width:90%; float:right;" id="extra_table">
                                    <tbody>
                                        <tr>
                                            <th style="text-align:center;background-color:#3385d9;color:#fff;">
                                                <label class="text-center"> Gross Amount</label>  
                                            </th>
                                            <th width="2" style="color:#3385d9;">
                                                <div class="text-left fa fa-rupee"></div>
                                            </th>
                                            <th style="text-align:right">
                                                <label class="text-right" id="fin_gross"> </label>
                                                <input class="form-control text-right" size="3" type="hidden" id="fn_gross" name="fn_gross">
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="text-align:center;background-color:#3385d9;color:#fff;">
                                                <label class="text-center"> Tax Amount</label>  
                                            </th>
                                            <th width="2" style="color:#3385d9;">
                                                <div class="text-left fa fa-rupee"></div>
                                            </th>
                                            <th style="text-align:right">
                                                <label id="fin_tax"> </label>
                                                <input class="form-control text-right" size="3" type="hidden" id="fn_tax" name="fn_tax">
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="text-align:center;background-color:#3385d9;color:#fff;">
                                                <label class="text-center"> Discount Amount </label>  
                                            </th>
                                            <th width="2" style="color:#3385d9;">
                                                <div class="text-left fa fa-rupee"></div>
                                            </th>
                                            <th style="text-align:right;width:50%;">
                                                <label id="fin_dis"> </label>
                                                <input class="form-control text-right" size="3" type="hidden" id="fn_dis" name="fn_dis">
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="text-align:center;background-color:#3385d9;color:#fff;">
                                                <label class="text-center"> Total Amount</label>  
                                            </th>
                                            <th width="2" style="color:#3385d9;">
                                                <div class="text-left fa fa-rupee"></div>
                                            </th>
                                            <th style="text-align:right">
                                                <label id="fin_net"> </label>
                                                <input class="form-control text-right" size="3" type="hidden" id="fn_net" name="fn_net">
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12"><br></div>
        <div class="col-md-12" style="text-align:center;">
            <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
            <a href="<?=site_url()?>invoice/<?=$projectID?>" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 				
        </div>
        <div class="col-md-12"><br></div>
    </div>
    </form>
</div>
<?php 
} 
?>
<?php
if($mode=="edit"){
	//print_r($invoiceDets);
	//print_r($invoiceItms);
	extract($invoiceDets);
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>invoice">Invoice</a></li>
	<li class="active">Edit Invoice</li>
</ul>
<div class="page-content-wrap">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('update_invoice',$attr); ?>
		<div class="row form-horizontal">
			<div class="col-md-12">
				<div class="page-title">
					<div class="page-title">
						<div class="col-md-6">
							<h2>Edit Invoice</h2>
						</div>
						<div class="col-md-6 text-right">
							<a href="<?=site_url()?>invoice/<?=$projectID?>" class="btn btn-info btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="panel" style="border:1px solid #e1e1e1;border-radius:5px;">
						<div class="panel-heading">
							<h3 class="panel-title" style="color:#fff;">Client Details </h3>
						</div>
						<div class="panel-body">
							<div class="col-md-12">
								<div class="form-group">
									<label class="col-md-4">Project</label>
									<div class="col-md-8">
										<label> : <?=$project_name?></label>
										<input type="hidden" name="invoice_project_id" id="invoice_project_id" value="<?=$invoice_project_id?>"> 
										<input type="hidden" name="invoice_id" id="invoice_id" value="<?=$invoice_id?>"> 
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4">Client Name</label>
									<div class="col-md-8">
										<label> : <?=$customer_name?></label>
										<input type="hidden" name="invoice_client_id" id="invoice_client_id" value="<?=$invoice_client_id?>">
									</div>
								</div>                            							
								<div class="form-group">
									<label class="col-md-4">Status</label>
									<div class="col-md-8">
										<select class="form-control select" id="invoice_status" name="invoice_status">
											<option value="">Select One</option>
											<option value="Draft">Draft</option>
											<option value="Confirmed">Confirmed</option>
											<option value="Reccuring">Reccuring</option>
											<option value="Completed">Completed</option>
										</select>
										<script>
											setSelectedIndex(document.getElementById('invoice_status'),"<?= $invoice_status ?>");
										</script>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-5">
					<div class="panel" style="border:1px solid #e1e1e1;border-radius:5px;">
						<div class="panel-heading">
							<h3 class="panel-title" style="color:#fff;">Sales Invoice Details </h3>
						</div>
						<div class="panel-body">
							<div class="col-md-12">
								<div class="form-group">
									<label class="col-md-5 ">Invoice No</label>
									<div class="input-group col-md-7"> 
										<label><b>#IN<?=date("y",strtotime("-1 years",strtotime($invoice_date)))?><?=date("y",strtotime($invoice_date))?>-<?=$invoice_no?></b></label>
										<input type="hidden" class="form-control" name="invoice_no" id="invoice_no" value="<?=$invoice_no?>"/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-5">Invoice Date</label>
									<div class="input-group col-md-7"> 
										<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
										<input type="text" name="invoice_date" id="invoice_date" class="form-control datepicker" value="<?=date("d/m/Y",strtotime($invoice_date))?>">  
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="panel-title">
							<h2>Invoice Item</h2>
						</div>
						<div class="pull-right">
							<button type="button" class="btn  btn-info" onclick="addRow()"  style="margin-right:10px;"><span class="fa fa-plus"></span> Add New Row</button> 
						</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-md-2" style="font-weight:600;padding-top:3px;">Invoice Title</label>
							<div class="col-md-10"> 
								<input class="form-control" id="invoice_title" name="invoice_title" type="text" placeholder="Enter Invoice Title" value="<?=$invoice_title?>" />
							</div>
						</div>
						<div class="panel panel-primary" style="border-top:none;border-radius:5px;">
							<div class="panel-body panel-body-table">
								<div class="row">			
								<?php
								$count=count($invoiceItms)+1;
								?>				
									<table class="table table-bordered table-striped table-actions" style="text-align:center;" id="dynamicTable">
										<thead>
											<tr>
												<th style="text-align:center;width:2%">S.No</th>                                        
												<th style="text-align:center;width:45%">Item Title</th>
												<th style="text-align:center;width:15%"><input type="hidden" name="hidCount" id="hidCount" value="<?=$count?>"><span class="fa fa-rupee"></span>Rate</th>
												<th style="text-align:center;width:13%"><span class="fa fa-rupee"></span>Amount</th>
												<th style="text-align:center;width:10%;">Actions</th>
											</tr>
										</thead>
										<tbody id="rowList">
										<?php
										$i=0;
										if(!empty($invoiceItms)){
											foreach($invoiceItms as $row){
											$i++;
										?>
											<tr id="tblRow<?=$i?>">
												<td><?=$i?></td>
												<td>
												   <select class="form-control select" data-live-search="true" name="invoiceitm_title<?=$i?>" id="invoiceitm_title<?=$i?>" onchange="getServiceDet(<?=$i?>)">
														<option value="">Select Service</option>
														<?php
														foreach($serviceList as $service){
															$sel="";
															if($service['service_id']==$row['invoiceitm_title']){
																$sel="selected";
															}
														?>
														<option value="<?=$service['service_id']?>" <?=$sel?>><?=$service['service_name']?></option>
														<?php
														}
														?>
													</select>
													<br /><br />
													<textarea class="form-control" id="invoiceitm_description<?=$i?>" name="invoiceitm_description<?=$i?>" placeholder="Enter Item Description" value="" rows="3"><?=$row['invoiceitm_description']?></textarea>
												</td>
												<td>
													<input class="form-control"  type="text" style="margin-bottom:10px" id="invoiceitm_rate<?=$i?>" name="invoiceitm_rate<?=$i?>" placeholder="Rate" onchange="onRateChange(<?=$i?>)" value="<?=$row['invoiceitm_rate']?>">
													<input class="form-control"  type="text" style="margin-bottom:10px" id="invoiceitm_tax<?=$i?>" name="invoiceitm_tax<?=$i?>" placeholder="Tax (%)" onchange="onRateChange(<?=$i?>)" value="<?=$row['invoiceitm_tax']?>">
													<input class="form-control"  type="text"  id="invoiceitm_discount<?=$i?>" name="invoiceitm_discount<?=$i?>" placeholder="Discount" onchange="onRateChange(<?=$i?>)" value="<?=$row['invoiceitm_discount']?>">
												</td>
												<td align="right">
													<label id="txt_amt<?=$i?>" name="txt_amt<?=$i?>"><?=number_format($row['invoiceitm_amount'],2)?></label>
													<input class="form-control" size="7" type="hidden" id="invoiceitm_amount<?=$i?>" name="invoiceitm_amount<?=$i?>" value="<?=$row['invoiceitm_amount']?>">
												</td>
												<td>
													<a href="javascript: addRow()" class="btn btn-success btn-condensed fa fa-plus"></a>
													<a href="javascript: removeRow(<?=$i?>)" class="btn btn-danger btn-condensed fa fa-times"></a>
												</td>
											</tr>
										<?php
											}
										}
										$i++;
										?>
											<tr id="tblRow<?=$i?>">
												<td><?=$i?></td>
												<td>
												   <select class="form-control select" data-live-search="true" name="invoiceitm_title<?=$i?>" id="invoiceitm_title<?=$i?>" onchange="getServiceDet(<?=$i?>)">
														<option value="">Select Service</option>
														<?php
														foreach($serviceList as $service){
														?>
														<option value="<?=$service['service_id']?>"><?=$service['service_name']?></option>
														<?php
														}
														?>
													</select>
													<br /><br />
													<textarea class="form-control" id="invoiceitm_description<?=$i?>" name="invoiceitm_description<?=$i?>" placeholder="Enter Item Description" value="" rows="3"></textarea>
												</td>
												<td>
													<input class="form-control"  type="text" style="margin-bottom:10px" id="invoiceitm_rate<?=$i?>" name="invoiceitm_rate<?=$i?>" placeholder="Rate" onchange="onRateChange(<?=$i?>)">
													<input class="form-control"  type="text" style="margin-bottom:10px" id="invoiceitm_tax<?=$i?>" name="invoiceitm_tax<?=$i?>" placeholder="Tax (%)" onchange="onRateChange(<?=$i?>)">
													<input class="form-control"  type="text"  id="invoiceitm_discount<?=$i?>" name="invoiceitm_discount<?=$i?>" placeholder="Discount" onchange="onRateChange(<?=$i?>)">
												</td>
												<td align="right">
													<label id="txt_amt<?=$i?>" name="txt_amt<?=$i?>">0.00</label>
													<input class="form-control" size="7" type="hidden" id="invoiceitm_amount<?=$i?>" name="invoiceitm_amount<?=$i?>">
												</td>
												<td>
													<a href="javascript: addRow()" class="btn btn-success btn-condensed fa fa-plus"></a>
													<a href="javascript: removeRow(<?=$i?>)" class="btn btn-danger btn-condensed fa fa-times"></a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<h3>Notes</h3>
								<textarea class="form-control" id="invoice_note" name="invoice_note" placeholder="Enter Your Notes" rows="3"><?=$invoice_note?></textarea>
								<?php
								foreach($serviceList as $service){
									$list=$service['service_id']."|".$service['service_name']."|".$service['service_description']."|".$service['service_amount'];
								?>
								<input type="hidden" class="form-control" name="service<?=$service['service_id']?>" id="service<?=$service['service_id']?>" value="<?=$list?>"/>
								<?php
								}
								?>
							</div>
							<div class="col-md-6">
								<div class="panel-body panel-body-table">
									<table class="table table-bordered table-striped table-actions" style="width:90%; float:right;" id="extra_table">
										<tbody>
											<tr>
												<th style="text-align:center;background-color:#3385d9;color:#fff;">
													<label class="text-center"> Gross Amount</label>  
												</th>
												<th width="2" style="color:#3385d9;">
													<div class="text-left fa fa-rupee"></div>
												</th>
												<th style="text-align:right">
													<label class="text-right" id="fin_gross"><?=number_format($invoice_gross_amount,2)?></label>
													<input class="form-control text-right" size="3" type="hidden" id="fn_gross" name="fn_gross" value="<?=$invoice_gross_amount?>">
												</th>
											</tr>
											<tr>
												<th style="text-align:center;background-color:#3385d9;color:#fff;">
													<label class="text-center"> Tax Amount</label>  
												</th>
												<th width="2" style="color:#3385d9;">
													<div class="text-left fa fa-rupee"></div>
												</th>
												<th style="text-align:right">
													<label id="fin_tax"><?=number_format($invoice_tax_amount,2)?></label>
													<input class="form-control text-right" size="3" type="hidden" id="fn_tax" name="fn_tax" value="<?=$invoice_tax_amount?>">
												</th>
											</tr>
											<tr>
												<th style="text-align:center;background-color:#3385d9;color:#fff;">
													<label class="text-center"> Discount Amount </label>  
												</th>
												<th width="2" style="color:#3385d9;">
													<div class="text-left fa fa-rupee"></div>
												</th>
												<th style="text-align:right;width:50%;">
													<label id="fin_dis"><?=number_format($invoice_discount_amount,2)?></label>
													<input class="form-control text-right" size="3" type="hidden" id="fn_dis" name="fn_dis" value="<?=$invoice_discount_amount?>">
												</th>
											</tr>
											<tr>
												<th style="text-align:center;background-color:#3385d9;color:#fff;">
													<label class="text-center"> Total Amount</label>  
												</th>
												<th width="2" style="color:#3385d9;">
													<div class="text-left fa fa-rupee"></div>
												</th>
												<th style="text-align:right">
													<label id="fin_net"><?=number_format($invoice_nett_amount,2)?></label>
													<input class="form-control text-right" size="3" type="hidden" id="fn_net" name="fn_net" value="<?=$invoice_nett_amount?>">
												</th>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12"><br></div>
			<div class="col-md-12" style="text-align:center;">
				<button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 
				<a href="<?=site_url()?>invoice/<?=$projectID?>" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 				
			</div>
			<div class="col-md-12"><br></div>
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
}
include "footer.php"; 
?>
