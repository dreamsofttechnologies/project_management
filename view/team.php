<?php
$main="transaction";
$sub="project";

include "header.php";
if($mode=="list"){
	$title="<strong>Team List</strong>";
	//include "titlebar.php";
	?>
<script>
function addStaff(){
	id=$('#staff_name').val();		
	name=$('#staff_name option:selected').html();	
	//alert(id+" "+name);
	ind=parseInt($('#hidCount').val())+1;
	$row="";
	$row=$row+'<tr id="row'+ind+'">'
	$row=$row+'<td align="center">'+ind+'</td>'
	$row=$row+'<td align="left">'+name+'</td>'
	$row=$row+'<td align="center"><input type="text" name="team_rate'+ind+'" id="team_rate'+ind+'" class="form-control" placeholder="Enter Rate" style="font-size:15px; height:30px;"><input type="hidden" name="team_staff_id'+ind+'" id="team_staff_id'+ind+'" class="form-control" value="'+id+'"><input type="hidden" name="team_id'+ind+'" id="team_id'+ind+'" class="form-control" value=""></td>'
	$row=$row+'<td align="center"><a href="javascript:removeStaff('+ind+')" class="btn btn-danger btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Delete"><span class="fa fa-trash-o"></span></a></td>'
	$row=$row+'</tr>';
	$('#hidCount').val(ind);
	if(ind==1){
		$('#staffList').html($row);
	} else {
		$('#staffList').append($row);
	}
}

function removeStaff(ind){
	$('#row'+ind).empty();
}
</script>
<div id="team">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Team</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->
		<?php
		$action="save_team";		
		?>
		<?php echo validation_errors(); ?>
		<?php echo form_open($action) ?>
			<div class="row" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<div class="col-md-6">
							<h2>Team<small></small></h2>
						</div>			
					</div>
					<!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
							<div class="row">
								<div class="col-md-3">                                
                                    <div class="form-group">
										<label class="control-label">Staff Name</label>
										<input type="hidden" name="projectID" id="projectID" value="<?=$projectID?>" />
										<select class="form-control select" name="staff_name" id="staff_name">
											<option value="">Select Staff</option>
											<?php
											foreach($staffList as $staff){
											?>
											<option value="<?=$staff['staff_id']?>"><?=$staff['staff_name']?></option>
											<?php
											}
											?>											
										</select>
									</div> 	
								</div>
								<div class="col-md-3">                                
                                    <!--<div class="form-group">
										<label class="control-label">Rate Per Hour</label>
										<div class="input-group col-md-12"> 
											<span class="input-group-addon"><span class="fa fa-inr"></span></span>
											<input type="text" name="bill_date" id="bill_date" class="form-control" value="" placeholder="Enter Percentage">  
										</div>
									</div> --> 	
								</div>
								<div class="col-md-3"></div>
                                <div class="col-md-3 text-center">
									<br>
                                    <button type="button" class="btn btn-info btn-condensed" onclick="addStaff()" style="width:100px;margin:5px 0px 0px 0px;"><span class="fa fa-plus"></span> &nbsp;Add</button> 
                                </div>
							</div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <?php
								$count=0;
								if(count($teamList)>0){
									$count=count($teamList);
								}
								?>
								<table class="table" style="border-radius:5px;overflow:hidden;margin:0px;">
                                    <thead>
                                        <tr>
                                            <th class="text-center">S.No<input type="hidden" name="hidCount" id="hidCount" value="<?=$count?>" /></th>
                                            <th class="text-center">Staff Name</th>
                                            <th class="text-center">Rate</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="staffList">
									<?php
									//print_r($teamList);
									if($count>0){
										$i=0;
										foreach($teamList as $row){
											$i++;
									?>
										<tr id="row<?=$i?>">
											<td align="center"><?=$i?></td>
											<td align="left"><?=$row['staff_name']?></td>
											<td align="center"><input type="text" name="team_rate<?=$i?>" id="team_rate<?=$i?>" class="form-control" placeholder="Enter Rate" style="font-size:15px; height:30px;" value="<?=$row['team_rate']?>"><input type="hidden" name="team_staff_id<?=$i?>" id="team_staff_id<?=$i?>" class="form-control" value="<?=$row['team_staff_id']?>"><input type="hidden" name="team_id<?=$i?>" id="team_id<?=$i?>" class="form-control" value="<?=$row['team_id']?>"></td>
											<td align="center"><a href="javascript:removeStaff(<?=$i?>)" class="btn btn-danger btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Delete"><span class="fa fa-trash-o"></span></a></td>
										</tr>
									<?php	
										}
									} else {
									?>
										<tr>
                                            <td align="center" colspan="4"><b>No Records Found</b></td>                                        
										</tr>
									<?php
									}
									?>                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
					<div class="row">
						<div class="col-md-12" style="text-align:center;">
							<button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button> 							
							<a href="<?=site_url()?>project" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a> 							
						</div>
						<div class="col-md-12"><br></div>
					</div>
                    <!-- END DEFAULT DATATABLE -->
    			</div>
    		</div>
    	</div>
    </form>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>	
<?php 
include "footer.php"; 
?>
