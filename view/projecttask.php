<?php
$main="transaction";
$sub="project";

include "header.php";
if($mode=="list"){
	$title="<strong>Project Task List</strong>";
	//include "titlebar.php";
	?>
<script>
function removeTask(id,projectID){
	var r = confirm("Do you want to remove this Project Task..?");
	if (r == true) {
		str="id="+id;	
		url="<?php echo site_url(); ?>delete_projecttask";
		$.ajax({	
			type: "POST",
			url: url,		
			data: str,		
			dataType: "html",
			success: function(data){
				if(data=='deleted'){
					alert("Project Task Removed Successfully.");
					window.location.href="<?php echo site_url(); ?>projecttask/"+projectID;	
				}
			}		
		});			
	}
}

</script>
<div id="projecttask">
	<ul class="breadcrumb">
		<li><a href="#dashboard">Home</a></li>
		<li class="active">Project Task</li>
	</ul>
	<div class="page-content-wrap">
		<!--<form class="form-horizontal" action=" " method="post"> -->
		
		<?php echo validation_errors(); ?>
		<?php echo form_open("projecttask") ?>
			<div class="row" style="padding-top:10px">
				<div class="col-md-12">
					<div class="page-title">
						<div class="col-md-6">
							<h2><?=$projectName?> - Project Task<small></small></h2>
						</div>
						<div class="col-md-6 text-right">
						<?php
						if($projectID){
						?>
							<a href="<?php echo site_url();?>addprojecttask/<?=$projectID?>" class="btn btn-info btn-condensed"><span class="fa fa-plus"></span> Add New Project Task</a> 
							<a href="<?=site_url()?>project" class="btn btn-danger btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
						<?php
						} else {
						?>
							<a href="<?=site_url()?>staff" class="btn btn-danger btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
						<?php
						}
						?>	
							
						</div>
								
					</div>
					<!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <div class="panel-heading">                                                               
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table datatable">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="min-width:75px;">S.No</th>
                                            <th style="min-width:95px;">Priority</th>
                                            <!--<th style="min-width:95px;">Project</th> -->
                                            <th class="text-center" style="min-width:95px;">Task</th>
                                            <th class="text-center" style="min-width:95px;">Type</th>
                                            <th class="text-center" style="min-width:150px;">Estimate Hours</th>
                                            <th class="text-center" style="min-width:135px;">Actual Hours</th>
                                            <th class="text-center" style="min-width:110px;">Due Date</th>
                                            <th class="text-center" style="min-width:125px;">Assigned To</th>
                                            <th class="text-center" style="min-width:95px;">Status</th>
                                            <th class="text-center" style="text-align:center;min-width:150px;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
									//print_r($taskList);
									$len=count($taskList);
									if($len>0){
										$i=0;
										foreach($taskList as $row){
											$i++;
									?>    <tr>
                                            <td align="center"><?=$i?></td>
                                            <td align="center">
											<?php
											if($row['task_priority']=='Low'){												
											?>
												<span class="priority-low"></span>
											<?php
											} else if($row['task_priority']=='Medium'){
											?>
												<span class="priority-medium"></span>
											<?php
											} else if($row['task_priority']=='High'){
											?>
												<span class="priority-high"></span>
											<?php
											}
											?>													
											</td>
                                           <!-- <td align="left">barbar</td> -->
                                            <td align="left"><?=$row['task_name']?></td>
                                           
                                            <td align="left"><?=$row['task_type_name']?></td>
                                            <td align="center"><?=$row['task_est_hours']?> Hrs</td>
											<td align="center"><?=$row['actTiming']?> Hrs</td>
                                            <td align="center"><?=date("d/m/Y",strtotime($row['task_due_date']))?></td>
                                            <td align="left"><?=$row['staff_name']?></td>
                                            <td align="center"><?=$row['task_status']?></td>
                                            <td align="center">   
												<a href="<?php echo site_url();?>dailytask/<?=$row['task_id']?>" class="btn btn-info btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Daily Task"><span class="fa fa-tasks"></span></a>
												
                                                <a href="<?php echo site_url();?>editprojecttask/<?=$row['task_id']?>" class="btn btn-success btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="fa fa-pencil"></span></a>                                                          
                                                <a href="javascript:removeTask('<?=$row['task_id']?>','<?=$projectID?>')" class="btn btn-danger btn-sm btn-condensed" data-toggle="tooltip" data-placement="bottom" title="Delete"><span class="fa fa-times"></span></a>
                                            </td>
                                        </tr>
									<?php
										}
									} else {
									?>
										<tr>
											<td align="center" colspan="10"><b>No Records Found</b></td>
										</tr>
									<?php
									}
									?>	
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END DEFAULT DATATABLE -->
    			</div>
    		</div>
    	</div>
    </form>
</div>   
<div style="clear:both;"></div>
<?php 
}
?>	
<?php
if($mode=="add"){
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>category">Project Task</a></li>
	<li class="active" >Add New Project Task</li>
</ul>
<div class="page-content-wrap" id="projecttask">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('save_projecttask',$attr); ?>
		<div class="row form-horizontal" style="padding-top:10px;">
			<div class="col-md-12">
				<div class="page-title">
					<div class="page-title">
                        <div class="col-md-6">
                            <h2>Add New Project Task<small></small></h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?=site_url()?>projecttask/<?=$projectID?>" class="btn btn-info btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
                        </div>                   
                    </div>
				</div>
				<div class="panel panel-hidden-controls">
					<!-- <div class="panel-heading">
						<h1 class="panel-title">Add New Project Task</h1>
					</div> -->
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Task Name</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="task_name" id="task_name" placeholder="Enter Task Name"/>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Project</label>
										<div class="col-md-8">
											<label class="control-label"><?=$projectName?></label>
											<input type="hidden" class="form-control" name="task_project_id" id="task_project_id" value="<?=$projectID?>" />
										</div>
									</div>
								</div>	
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Type</label>
										<div class="col-md-8">
											<select class="form-control select" name="task_type" id="task_type">
												<option value="">Select Type</option>
												<?php
												foreach($typeList as $type){
												?>
												<option value="<?=$type['task_type_id']?>"><?=$type['task_type_name']?></option>
												<?php
												}
												?>	
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Budget Head</label>
										<div class="col-md-8">
											<select class="form-control select" name="task_budget_id" id="task_budget_id">
												<option value="">Select Budget</option>
												<?php
												foreach($budgetHeadList as $budget){
												?>
												<option value="<?=$budget['budget_head_id']?>"><?=$budget['budget_head_name']?></option>
												<?php
												}
												?>	
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">                                
									<div class="form-group">
										<label class="col-md-4 control-label">Estimated Hours</label>
										<div class="input-group col-md-8"> 
											<span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
											<input type="text" name="task_est_hours" id="task_est_hours" class="form-control" placeholder="30 hrs">  
										</div>
									</div>	
								</div>																
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Priority</label>
										<div class="col-md-8">
											<select class="form-control select" name="task_priority" id="task_priority">
												<option value="Low">Low</option>
												<option value="Medium">Medium</option>
												<option value="High">High</option>																								
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">                                
                                    <div class="form-group">
										<label class="col-md-4 control-label">Due Date</label>
										<div class="input-group col-md-8"> 
											<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
											<input type="text" name="task_due_date" id="task_due_date" class="form-control datepicker" value="<?= date("d/m/Y")?>">  
										</div>
									</div> 	
								</div>								
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Assigned To</label>
										<div class="col-md-8">
											<select class="form-control select" name="task_assigned" id="task_assigned">
												<option value="">Select Staff</option>
												<?php
												foreach($staffList as $staff){
												?>
													<option value="<?=$staff['staff_id']?>"><?=$staff['staff_name']?></option>
												<?php
												}
												?>	
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Parent Task</label>
										<div class="col-md-8">
											<select class="form-control select" name="task_parent" id="task_parent">
												<option value="0">Select Parent Task</option>
												<?php
												foreach($parentList as $par){
												?>
												<option value="<?=$par['task_id']?>"><?=$par['task_name']?></option>
												<?php
												}
												?>	
											</select>
										</div>
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Status</label>
										<div class="col-md-8">
											<select class="form-control select" name="task_status" id="task_status">
												<option value="Active">Active</option>
												<option value="Inactive">Inactive</option>
											</select>
										</div>
									</div>
								</div>
                                <div class="col-md-12"><br></div>
                                <div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button>                                     
                                    <a href="<?=site_url()?>projecttask/<?=$projectID?>" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a>                                </div>
							</div>
						</div>
					</div>
				</div>									                                
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
} 
?>
<?php
if($mode=="edit"){
	//print_r($taskDets);
	extract($taskDets);
	?>
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>dashboard">Home</a></li>
	<li><a href="<?php echo site_url();?>category">Project Task</a></li>
	<li class="active">Edit Project Task</li>
</ul>
<div class="page-content-wrap">
	<?php echo validation_errors(); ?>
	<?php 
	$attr=array('onsubmit' => 'return checkInputs()');
	echo form_open_multipart('update_projecttask',$attr); ?>
		<div class="row form-horizontal">
			<div class="col-md-12">
				<div class="page-title">
					<div class="page-title">
                        <div class="col-md-6">
                            <h2>Add New Project Task<small></small></h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?=site_url()?>projecttask/<?=$projectID?>" class="btn btn-info btn-condensed"><span class="fa fa-arrow-left"></span> Back</a> 
                        </div>                   
                    </div>
				</div>
				<div class="panel panel-hidden-controls">
					<!-- <div class="panel-heading">
						<h1 class="panel-title">Add New Project Task</h1>
					</div> -->
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Task Name</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="task_name" id="task_name" value="<?=$task_name?>" placeholder="Enter Task Name"/>
											<input type="hidden" class="form-control" name="task_id" id="task_id" value="<?=$task_id?>" />
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Project</label>
										<div class="col-md-8">
											<label class="control-label"><?=$projectName?></label>
											<input type="hidden" class="form-control" name="task_project_id" id="task_project_id" value="<?=$projectID?>" />
										</div>
									</div>
								</div>	
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Type</label>
										<div class="col-md-8">
											<select class="form-control select" name="task_type" id="task_type">
												<option value="">Select Type</option>
												<?php
												foreach($typeList as $type){
													$sel="";
													if($type['task_type_id']==$task_type){
														$sel="selected";
													}
												?>
												<option value="<?=$type['task_type_id']?>" <?=$sel?>><?=$type['task_type_name']?></option>
												<?php
												}
												?>	
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Budget Head</label>
										<div class="col-md-8">
											<select class="form-control select" name="task_budget_id" id="task_budget_id">
												<option value="">Select Budget</option>
												<?php
												foreach($budgetHeadList as $budget){
													$sel="";
													if($budget['budget_head_id']==$task_budget_id){
														$sel="selected";
													}
												?>
												<option value="<?=$budget['budget_head_id']?>" <?=$sel?>><?=$budget['budget_head_name']?></option>
												<?php
												}
												?>	
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">                                
									<div class="form-group">
										<label class="col-md-4 control-label">Estimated Hours</label>
										<div class="input-group col-md-8"> 
											<span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
											<input type="text" name="task_est_hours" id="task_est_hours" class="form-control" placeholder="30 hrs" value="<?=$task_est_hours?>">  
										</div>
									</div>	
								</div>																
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Priority</label>
										<div class="col-md-8">
											<select class="form-control select" name="task_priority" id="task_priority">
												<option value="Low">Low</option>
												<option value="Medium">Medium</option>
												<option value="High">High</option>																								
											</select>
											<script>
												setSelectedIndex(document.getElementById('task_priority'),"<?=$task_priority?>");
											</script>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">                                
                                    <div class="form-group">
										<label class="col-md-4 control-label">Due Date</label>
										<div class="input-group col-md-8"> 
											<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
											<input type="text" name="task_due_date" id="task_due_date" class="form-control datepicker" value="<?= date("d/m/Y",strtotime($task_due_date))?>">  
										</div>
									</div> 	
								</div>								
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Assigned To</label>
										<div class="col-md-8">
											<select class="form-control select" name="task_assigned" id="task_assigned">
												<option value="">Select Staff</option>
												<?php
												foreach($staffList as $staff){
													$sel="";
													if($staff['staff_id']==$task_assigned){
														$sel="selected";
													}
												?>
													<option value="<?=$staff['staff_id']?>" <?=$sel?>><?=$staff['staff_name']?></option>
												<?php
												}
												?>	
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Parent Task</label>
										<div class="col-md-8">
											<select class="form-control select" name="task_parent" id="task_parent">
												<option value="0">Select Parent Task</option>
												<?php
												foreach($parentList as $par){
													$sel="";
													if($par['task_id']==$task_parent){
														$sel="selected";
													}
												?>
												<option value="<?=$par['task_id']?>" <?=$sel?>><?=$par['task_name']?></option>
												<?php
												}
												?>	
											</select>
										</div>
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-4 control-label">Status</label>
										<div class="col-md-8">
											<select class="form-control select" name="task_status" id="task_status">
												<option value="Active">Active</option>
												<option value="Inactive">Inactive</option>
											</select>
											<script>
												setSelectedIndex(document.getElementById('task_status'),"<?=$task_status?>");
											</script>
										</div>
									</div>
								</div>
                                <div class="col-md-12"><br></div>
                                <div class="col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn btn-info btn-condensed"><span class="fa fa-check-circle"></span> Save</button>                                     
                                    <a href="<?=site_url()?>projecttask/<?=$projectID?>" class="btn btn-link"><span class="fa fa-arrow-left"></span>Back</a>                                </div>
							</div>
						</div>
					</div>
				</div>									                                
			</div>			
		</div>
	</form>
</div>
<div style="clear:both;"></div>
<?php 
}
include "footer.php"; 
?>
