<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projecttask extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('projecttask_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);									
			$data['mode']="list";
			$data['projectID']=$param;			
			$data['projectName']=$this->projecttask_model->projectName($param);
			$data['taskList']=$this->projecttask_model->get_projecttask($param);
			//print_r($data);
			$this->load->view('projecttask',$data);						
		}else{
			$this->load->view('login');
		}
	}
	
	public function stafftask(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);									
			$data['mode']="list";
			$data['staffID']=$param;	
			$data['projectName']=$this->projecttask_model->staffName($param);
			$data['taskList']=$this->projecttask_model->get_stafftask($param);
			//print_r($data);
			$this->load->view('projecttask',$data);						
		} else {
			$this->load->view('login');
		}
	}
	
	public function add_projecttask(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="add";
			$data['projectID']=$param;
			$data['projectName']=$this->projecttask_model->projectName($param);
			$data['budgetHeadList']=$this->projecttask_model->budgetHeadList($param);
			$data['typeList']=$this->projecttask_model->typeList();
			$data['staffList']=$this->projecttask_model->staffList();
			$data['parentList']=$this->projecttask_model->parentList();				
			$this->load->view('projecttask',$data);								
		} else {
			$this->load->view('login');
		}
	}
	
	public function edit_projecttask(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="edit";
			$data['taskDets']=$this->projecttask_model->get_projecttaskDetails($param);
			$projectID=$data['taskDets']['task_project_id'];
			
			$data['projectID']=$projectID;
			$data['projectName']=$this->projecttask_model->projectName($projectID);
			$data['budgetHeadList']=$this->projecttask_model->budgetHeadList($projectID);
			$data['typeList']=$this->projecttask_model->typeList();
			$data['staffList']=$this->projecttask_model->staffList();
			$data['parentList']=$this->projecttask_model->parentList($param);	
						
			$this->load->view('projecttask',$data);
		} else {
			$this->load->view('login');
		}
	}			
	
	public function save_projecttask(){
		$projectID=$this->input->post('task_project_id');
		$this->projecttask_model->save_projecttask();
		redirect('projecttask/'.$projectID);		
	}
	
	public function update_projecttask(){
		$projectID=$this->input->post('task_project_id');
		$this->projecttask_model->update_projecttask();
		redirect('projecttask/'.$projectID);
	}
	
	public function delete_projecttask(){
		$this->projecttask_model->delete_projecttask();
	}				
	
}
?>