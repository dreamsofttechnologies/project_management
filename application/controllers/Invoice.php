<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('invoice_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){
			$data['mode']="list";
			$param = $this->uri->segment(2, 0);	
			$data['projectID']=$param;
			$data['projectName']=$this->invoice_model->projectName($param);
			$data['invoiceList']=$this->invoice_model->get_invoice($param);
			//print_r($data);
			$this->load->view('invoice',$data);						
		}else{
			$this->load->view('login');
		}
	}
	
	public function add_invoice(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="add";
			$data['projectID']=$param;		
			$data['projectDets']=$this->invoice_model->projectDets($param);		
			$data['serviceList']=$this->invoice_model->serviceList();		
			$data['invoice_no']=$this->invoice_model->get_nextcode();		
			$this->load->view('invoice',$data);								
		}else{
			$this->load->view('login');
		}
	}
	
	public function edit_invoice(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="edit";
			$data['serviceList']=$this->invoice_model->serviceList();
			$data['invoiceDets']=$this->invoice_model->get_invoiceDetails($param);
			$data['invoiceItms']=$this->invoice_model->get_invoiceItmDetails($param);	
			$data['projectID']=$data['invoiceDets']['invoice_project_id'];			
			$this->load->view('invoice',$data);
		} else {
			$this->load->view('login');
		}
	}			
	
	public function save_invoice(){
		$projectID=$this->input->post('invoice_project_id');
		$this->invoice_model->save_invoice();
		redirect('invoice/'.$projectID);
	}
	
	public function update_invoice(){
		$projectID=$this->input->post('invoice_project_id');
		$this->invoice_model->update_invoice();
		redirect('invoice/'.$projectID);		
	}
	
	public function delete_invoice(){
		$this->invoice_model->delete_invoice();
	}				
	
}
?>