<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotation extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('quotation_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){
			$data['mode']="list";
			$param = $this->uri->segment(2, 0);	
			$data['projectID']=$param;
			$data['projectName']=$this->quotation_model->projectName($param);
			$data['quotationList']=$this->quotation_model->get_quotation($param);
			//print_r($data);
			$this->load->view('quotation',$data);						
		}else{
			$this->load->view('login');
		}
	}
	
	public function add_quotation(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="add";
			$data['projectID']=$param;		
			$data['projectDets']=$this->quotation_model->projectDets($param);		
			$data['serviceList']=$this->quotation_model->serviceList();		
			$data['quotation_no']=$this->quotation_model->get_nextcode();		
			$this->load->view('quotation',$data);								
		}else{
			$this->load->view('login');
		}
	}
	
	public function edit_quotation(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="edit";
			$data['serviceList']=$this->quotation_model->serviceList();
			$data['quotationDets']=$this->quotation_model->get_quotationDetails($param);
			$data['quotationItms']=$this->quotation_model->get_quotationItmDetails($param);	
			$data['projectID']=$data['quotationDets']['quotation_project_id'];			
			$this->load->view('quotation',$data);
		} else {
			$this->load->view('login');
		}
	}			
	
	public function save_quotation(){
		$projectID=$this->input->post('quotation_project_id');
		$this->quotation_model->save_quotation();
		redirect('quotation/'.$projectID);
	}
	
	public function update_quotation(){
		$projectID=$this->input->post('quotation_project_id');
		$this->quotation_model->update_quotation();
		redirect('quotation/'.$projectID);		
	}
	
	public function delete_invoice(){
		$this->invoice_model->delete_invoice();
	}				
	
}
?>