<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payableitems extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('payableitems_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){
			
			$data['mode']="list";
			$data['payableitemsList']=$this->payableitems_model->get_payableitems($param,$search_data);			
			$this->load->view('payableitems',$data);						
		}else{
			$this->load->view('login');
		}
	}
	
	public function add_payableitems(){
		if($this->session->userdata('ccusr_id')){
			$projectID = $this->uri->segment(2, 0);
			$budgetID = $this->uri->segment(3, 0);
			$staffID = $this->uri->segment(4, 0);
			
			$data['mode']="add";
			$data['projectDets']=$this->payableitems_model->projectDets($projectID);
			$data['budgetDets']=$this->payableitems_model->budgetDets($projectID,$budgetID);
			$data['staffDets']=$this->payableitems_model->staffDets($staffID);
			$data['actCost']=$this->payableitems_model->getActCost($projectID,$budgetID,$staffID);
				
			$this->load->view('payableitems',$data);								
		}else{
			$this->load->view('login');
		}
	}
	
	public function edit_payableitems(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="edit";
			$data['payableitemsDets']=$this->payableitems_model->get_payableitemsDetails($param);			
			$this->load->view('payableitems',$data);
		}else{
			$this->load->view('login');
		}
	}			
	
	public function save_payableitems(){		
		$this->payableitems_model->save_payableitems();
		redirect('payableitems');
	}
	
	public function update_payableitems(){		
		$this->payableitems_model->update_payableitems();
		redirect('payableitems');		
	}
	public function delete_payableitems(){
		$this->payableitems_model->delete_payableitems();
	}				
	
}
?>