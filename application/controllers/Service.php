<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('service_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){
			$data['mode']="list";
			$data['param']=$param;
			$data['serviceList']=$this->service_model->get_service($param,$search_data);			
			$this->load->view('service',$data);						
		}else{
			$this->load->view('login');
		}
	}
	
	public function add_service(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="add";
			$this->load->view('service',$data);								
		}else{
			$this->load->view('login');
		}
	}
	
	public function edit_service(){
		if($this->session->userdata('ccusr_id')){
			$par = $this->uri->segment(2, 0);
			$data['mode']="edit";
			$data['serviceDets']=$this->service_model->get_serviceDetails($par);			
			$this->load->view('service',$data);
		}else{
			$this->load->view('login');
		}
	}			
	
	public function save_service(){
		$this->service_model->save_service();
		redirect('service');		
	}
	
	public function update_service(){
		$this->service_model->update_service();
		redirect('service');		
	}
	public function delete_service(){
		$this->service_model->delete_service();
	}				
	
}
?>