<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('team_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="list";
			$data['projectID']=$param;
			$data['staffList']=$this->team_model->staffList();
			$data['teamList']=$this->team_model->get_team($param);
			//print_r($data);
			$this->load->view('team',$data);						
		}else{
			$this->load->view('login');
		}
	}
	
	public function add_team(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="add";			
			//$data['params']=$this->team_model->get_productParam($param);		
			$this->load->view('team',$data);								
		}else{
			$this->load->view('login');
		}
	}
	
	public function edit_team(){
		if($this->session->userdata('ccusr_id')){
			$par = $this->uri->segment(2, 0);
			$data['mode']="edit";
			$data['team']=$this->team_model->get_teamDetails($par);			
			$this->load->view('team',$data);
		} else {
			$this->load->view('login');
		}
	}			
	
	public function save_team(){
		$param = $this->input->post('projectID');
		$this->team_model->save_team();
		redirect('team/'.$param);		
	}
	
	public function update_team(){
		$this->team_model->update_team();
		redirect('team');
	}
	
	public function delete_team(){
		$this->team_model->delete_team();
	}				
	
}
?>