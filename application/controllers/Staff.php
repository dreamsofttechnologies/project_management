<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('staff_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){						
			$param = $this->uri->segment(2, 0);
									
			$data['mode']="list";
			$data['staffList']=$this->staff_model->get_staff($param,$search_data);
			//print_r($data);
			$this->load->view('staff',$data);						
		}else{
			$this->load->view('login');
		}
	}
	
	public function add_staff(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="add";
			$data['typeList']=$this->staff_model->typeList();	
			$data['staffCode']=$this->staff_model->get_nextcode();		
			$this->load->view('staff',$data);								
		}else{
			$this->load->view('login');
		}
	}
	
	public function edit_staff(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="edit";
			$data['typeList']=$this->staff_model->typeList();
			$data['staffDets']=$this->staff_model->get_staffDetails($param);			
			$this->load->view('staff',$data);
		}else{
			$this->load->view('login');
		}
	}			
	
	public function save_staff(){		
		$this->staff_model->save_staff();
		redirect('staff');		
	}
	
	public function update_staff(){		
		$this->staff_model->update_staff();
		redirect('staff');		
	}
	public function delete_staff(){
		$this->staff_model->delete_staff();
	}				
	
}
?>