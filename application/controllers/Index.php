<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','security','string'));
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('encryption');		
		$this->load->model('index_model');
	}
	public function index(){
		//$this->session->set_userdata('ccusr_id',1);
		if($this->session->userdata('ccusr_id')){
			$data['usr_id']=$this->session->userdata();
			$this->load->view('index',$data);						
		} else {
			$key=md5(uniqid(rand(), true));
			$this->session->set_userdata('tok_key',$key);
			$data['token_key']=$key;
			$this->load->view('login',$data);
		}
	}
	
	public function login(){
		$key=md5(uniqid(rand(), true));
		$this->session->set_userdata('tok_key',$key);
		$data['token_key']=$key;
		$this->load->view('login',$data);		
	}
	
	public function login_check(){	
		//print_r($_REQUEST);	
		if($this->input->post('token_key')==$this->session->userdata('tok_key')){
			$data['adm_username'] = $this->input->post('usrname');
			$data['adm_password'] = $this->input->post('usrpwd');			
			//echo $this->index_model->check_login($data);
			if($this->index_model->check_login($data) === TRUE){
				//echo "IF";
				redirect('dashboard');	
			} else {
				//echo "ELSE";
				$this->session->set_flashdata("error","Invalid Username or Password");
				redirect('login');
			}					
		} else {
			$this->session->set_flashdata("error","User Login is Failed, Please Try Again");
			redirect('login');
		}
	}
	
	public function logout(){
		session_destroy();
		redirect(''); 
	}
	
	public function encodePwd(){		
		$msg="admin";
		$password = $this->encryption->encrypt($msg);				
		echo $password;
		//$password = "";
		//$msg = $this->encryption->decrypt($password);
		//echo $msg;
	}	
	
}
?>
