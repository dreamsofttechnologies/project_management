<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('client_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){
			$data['mode']="list";
			$data['clientList']=$this->client_model->get_client($param,$search_data);
			//print_r($data);
			$this->load->view('client',$data);						
		}else{
			$this->load->view('login');
		}
	}
	
	public function add_client(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="add";			
			$data['clientCode']=$this->client_model->get_nextcode();			
			$this->load->view('client',$data);								
		}else{
			$this->load->view('login');
		}
	}
	
	public function edit_client(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="edit";
			$data['clientDets']=$this->client_model->get_clientDetails($param);			
			$this->load->view('client',$data);
		}else{
			$this->load->view('login');
		}
	}			
	
	public function save_client(){
		$this->client_model->save_client();
		redirect('client');
	}
	
	public function update_client(){
		$this->client_model->update_client();
		redirect('client');		
	}
	public function delete_client(){
		$this->client_model->delete_client();
	}				
	
}
?>