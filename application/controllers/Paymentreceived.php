<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paymentreceived extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('paymentreceived_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){
			$data['mode']="list";
			$param = $this->uri->segment(2, 0);
			$data['invoiceID']=$param;			
			$data['invoiceDets']=$this->paymentreceived_model->get_invoiceDetails($param);
			$this->load->view('paymentreceived',$data);						
		} else {
			$this->load->view('login');
		}
	}
	
	
	public function save_paymentreceived(){
		$projectID=$this->input->post('pr_project_id');
		$this->paymentreceived_model->save_paymentreceived();
		redirect('invoice/'.$projectID);		
	}
		
}
?>