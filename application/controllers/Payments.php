<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('payments_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);												
			$data['mode']="list";
			$data['payableID']=$param;
			$data['paymentList']=$this->payments_model->get_payments();
			$this->load->view('payments',$data);						
		}else{
			$this->load->view('login');
		}
	}
	
	public function add_payments(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="add";			
			$data['payableID']=$param;
			$data['payableDet']=$this->payments_model->payableDet($param);		
			$data['staffList']=$this->payments_model->staffList();		
			$this->load->view('payments',$data);								
		}else{
			$this->load->view('login');
		}
	}
	
	public function edit_payments(){
		if($this->session->userdata('ccusr_id')){
			$par = $this->uri->segment(2, 0);
			$data['mode']="edit";
			$data['staffList']=$this->payments_model->staffList($param);	
			$data['paymentDet']=$this->payments_model->get_paymentsDetails($par);			
			$this->load->view('payments',$data);
		}else{
			$this->load->view('login');
		}
	}			
	
	public function save_payments(){		
		$this->payments_model->save_payments();
		redirect('payments');		
	}
	
	public function update_payments(){
		$this->payments_model->update_payments();
		redirect('payments');		
	}
	public function delete_payments(){
		$this->payments_model->delete_payments();
	}				
	
}
?>