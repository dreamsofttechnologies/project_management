<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('products_model');
		$this->load->model('category_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){
			//print_r($_REQUEST);
			$param = $this->uri->segment(2, 0);
			//$pageno = $this->uri->segment(3, 0);
			//$rownos = $this->uri->segment(4, 0);
			
			//$total_rows=$this->products_model->total_records($param);
			//$inp=function_helper();	
						
			$cat_search=""; 
			if($this->input->post('cat_search')!=""){
				$cat_search=$this->input->post('cat_search');
				//$pageno = $this->input->post('page');
				//$rownos = $this->input->post('rows');
			}
			
			$prod_search=""; 
			if($this->input->post('prod_search')!=""){
				$prod_search=$this->input->post('prod_search');				
			}
			
			/*$per_pg=$inp['record_per_page'];						
			if($per_pg < $rownos){ $per_pg=$rownos; }			
			
			if($rownos==""){
				$rownos=$per_pg;
			}
			
			if($pageno>0){
				$page = $pageno;
			} else {
				$page = 0;
			}*/			
			
			//$data['def_rows']=$inp['default_record_per_page'];
			//$data['total_rows']=$total_rows;
			//$data['row_limit']=$rownos;
			//$data['pg']=$pageno;
			//$data['rows']=$rownos;
			$data['search']=$search_data;			
			$data['mode']="list";
			$data['param']=$param;
			
			$data['category']=$this->category_model->get_category($param,$cat_search);			
			$data['products']=$this->products_model->get_products($param,$prod_search);
			$data['backID']=$this->products_model->get_prodBackID($param);
			//$data['products']=$this->products_model->get_products($param,$rownos,$page,$search_data);
			//print_r($data);
			$this->load->view('products',$data);						
		}else{
			$this->load->view('login');
		}
	}
	
	public function add_products(){
		if($this->session->userdata('ccusr_id')){
			$data['mode']="add";						
			$param = $this->uri->segment(2, 0);
			$data['param']=$param;
			$data['autoc'] = $this->products_model->get_autocomplete();
			$data['size']=$this->products_model->get_productsSize();
			$data['variants']=$this->products_model->get_productsVariants();
			$data['tags']=$this->products_model->get_productTags();
			//print_r($data['tags']);
			$tag_id=$this->products_model->get_tagID($param);
			$data['tag_id']=$tag_id;			
			$data['params']=$this->products_model->get_productParams($tag_id);
			
			$this->load->view('products',$data);								
		}else{
			$this->load->view('login');
		}
	}
	
	public function edit_products(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="edit";
			$data['size']=$this->products_model->get_productsSize();			
			$data['autoc'] = $this->products_model->get_autocomplete();
			$data['variants']=$this->products_model->get_productsVariants();
			
			$data['products_edit'] = $this->products_model->get_editDetails($param);
			$data['products_size'] = $this->products_model->get_productsize($param);
			$data['products_image'] = $this->products_model->get_productimage($param);
			$data['products_relat'] = $this->products_model->get_productrelated($param);
			$i=0;
			foreach($data['products_relat'] as $pr){
				$i++;
				$data['products_relatsub'][$i] = $this->products_model->get_productrelprd($pr['rp_relprdid']);
			}
			
			$data['tags']=$this->products_model->get_productTags();
			//print_r($data['tags']);
			$tag_id=$this->products_model->get_tagID($data['products_edit']['prd_catid']);
			$data['tag_id']=$tag_id;			
			$data['params']=$this->products_model->get_productParams($tag_id,$param);
			
			$this->load->view('products',$data);
		}else{
			$this->load->view('login');
		}
	}
	
	public function products_attimageremove(){
		$pg_par=$this->input->post('id');
		$st=$this->products_model->products_attimageremove($pg_par);	
		echo $st;
	}
	
	public function view_products(){
		if($this->session->userdata('ccusr_id')){
			$par = $this->uri->segment(2, 0);
			$data['mode']="view";
			$data['products']=$this->products_model->get_productsDetails($par);
			$this->load->view('products',$data);
		}else{
			$this->load->view('login');
		}
	}
	
	
	public function save_products(){
		//print_r($_REQUEST);
		$param=$this->input->post('category_id');
		$this->products_model->save_products();
		redirect('listproducts/'.$param);
	}
	public function update_products(){
		$param=$this->input->post('category_id');
		$this->products_model->update_products();
		redirect('listproducts/'.$param);
	}
	public function delete_products(){
		$this->products_model->delete_products();
	}				
	
}
?>