<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Budget extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('budget_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){
			$data['mode']="list";
			$param = $this->uri->segment(2, 0);
			$data['projectID']=$param;
			$data['projectName']=$this->budget_model->projectName($param);
			$data['projectAmt']=$this->budget_model->projectAmt($param);
			$data['budgetHeadList']=$this->budget_model->budgetHeadList();
			$data['budgetList']=$this->budget_model->get_budget($param,$search_data);
			//print_r($data);
			$this->load->view('budget',$data);						
		}else{
			$this->load->view('login');
		}
	}
	
	public function add_budget(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="add";
			if($param!=""){
				$data['catID']=$param;	
			} else {
				$data['catID']=0;	
			}
			//$data['params']=$this->budget_model->get_productParam($param);		
			$this->load->view('budget',$data);								
		}else{
			$this->load->view('login');
		}
	}
	
	public function edit_budget(){
		if($this->session->userdata('ccusr_id')){
			$par = $this->uri->segment(2, 0);
			$data['mode']="edit";
			$data['budget']=$this->budget_model->get_budgetDetails($par);			
			$this->load->view('budget',$data);
		}else{
			$this->load->view('login');
		}
	}			
	
	public function save_budget(){
		$projectID=$this->input->post('projectID');
		$this->budget_model->save_budget();
		redirect('budget/'.$projectID);		
	}
	
	public function update_budget(){
		$catID=$this->input->post('catID');
		$this->budget_model->update_budget();
		//redirect('budget');
		if($catID==0){
			redirect('budget');
		} else {
			redirect('listbudget/'.$catID);
		}
	}
	public function delete_budget(){
		$this->budget_model->delete_budget();
	}				
	
}
?>