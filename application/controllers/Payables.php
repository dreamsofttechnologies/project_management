<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payables extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('payables_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){			
			$data['mode']="list";
			$data['payablesList']=$this->payables_model->get_payable();
			//print_r($data);
			$this->load->view('payables',$data);						
		}else{
			$this->load->view('login');
		}
	}
	
	public function create_payables(){
		if($this->session->userdata('ccusr_id')){
			//print_r($_REQUEST);		
			$data['mode']="add";	
			$count=$this->input->post('hidCount');
			$param="";
			for($i=1;$i<=$count;$i++){
				if($this->input->post('chk_itm'.$i)!=""){
					$param=$param.','.$this->input->post('chk_itm'.$i);
				}
			}		
			$param=substr($param,1);			
			$data['itemsIds']=$param;
			$data['itemsDets']=$this->payables_model->itemsDets($param);		
			$this->load->view('payables',$data);						
		}else{
			$this->load->view('login');
		}
	}
		
	public function edit_payables(){
		if($this->session->userdata('ccusr_id')){
			$par = $this->uri->segment(2, 0);
			$data['mode']="edit";
			$data['payableDets']=$this->payables_model->get_payableDetails($par);			
			$this->load->view('payables',$data);
		}else{
			$this->load->view('login');
		}
	}			
	
	public function save_payables(){
		$catID=$this->input->post('catID');
		$this->payables_model->save_payables();
		redirect('payables');
	}
	
	public function update_payables(){
		$catID=$this->input->post('catID');
		$this->payables_model->update_payables();
		redirect('payables');		
	}
	public function delete_payables(){
		$this->payables_model->delete_payables();
	}				
	
}
?>