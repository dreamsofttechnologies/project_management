<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Budgethead extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('budgethead_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){
			$data['mode']="list";
			$data['budgetheadList']=$this->budgethead_model->get_budgethead($param,$search_data);
			$this->load->view('budgethead',$data);						
		} else {
			$this->load->view('login');
		}
	}		
	
	public function save_budgethead(){
		$this->budgethead_model->save_budgethead();
		redirect('budgethead');		
	}
		
	public function delete_budgethead(){
		$this->budgethead_model->delete_budgethead();
	}				
	
}
?>