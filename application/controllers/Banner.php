<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('banner_model');
	}
	
	public function index(){
		if($this->session->userdata('ccusr_id')){
			//print_r($_REQUEST);
			$pageno = $this->uri->segment(2, 0);
			$rownos = $this->uri->segment(3, 0);
			
			$total_rows=$this->banner_model->total_records();
			$inp=function_helper();	
						
			$search_data=""; 
			if($this->input->post('searching')!=""){
				$search_data=$this->input->post('searching');
				$pageno = $this->input->post('page');
				$rownos = $this->input->post('rows');
			}
			
			$per_pg=$inp['record_per_page'];						
			//if($per_pg < $rownos){ $per_pg=$rownos; }			
			
			if($rownos==""){
				$rownos=$per_pg;
			}
			
			if($pageno>0){
				$page = $pageno;
			} else {
				$page = 0;
			}			
			
			$data['def_rows']=$inp['default_record_per_page'];
			$data['total_rows']=$total_rows;
			$data['row_limit']=$rownos;
			$data['pg']=$pageno;
			$data['rows']=$rownos;
			$data['search']=$search_data;			
			$data['mode']="list";
			$data['banner']=$this->banner_model->get_banner(false,$rownos,$page,$search_data);
			//print_r($data);
			$this->load->view('banner',$data);						
		}else{
			$this->load->view('login');
		}
	}
	
	public function add_banner(){
		if($this->session->userdata('ccusr_id')){
			$data['mode']="add";	
			$this->load->view('banner',$data);								
		}else{
			$this->load->view('login');
		}
	}
	
	public function edit_banner(){
		if($this->session->userdata('ccusr_id')){
			$par = $this->uri->segment(2, 0);
			$data['mode']="edit";
			$data['banner']=$this->banner_model->get_bannerDetails($par);
			$this->load->view('banner',$data);
		}else{
			$this->load->view('login');
		}
	}
	
	public function banner_imageremove(){
		$pg_par=$this->input->post('id');
		$st=$this->banner_model->banner_imageremove($pg_par);	
		echo $st;
	}		
	
	public function save_banner(){
		$this->banner_model->save_banner();
		redirect('banner');
	}
	
	public function update_banner(){
		$this->banner_model->update_banner();
		redirect('banner');
	}
	
	public function delete_banner(){
		$this->banner_model->delete_banner();
	}							
}
?>