<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Production extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('production_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){
			$data['mode']="list";
			$data['input']=$this->input->post();
			$data['staffList']=$this->production_model->staffList();
			$data['staffTaskList']=$this->production_model->staffTaskList();
			$this->load->view('production',$data);						
		}else{
			$this->load->view('login');
		}
	}		
}
?>