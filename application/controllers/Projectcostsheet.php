<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projectcostsheet extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('projectcostsheet_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
									
			$data['mode']="list";
			$data['projectID']=$param;
			$data['projectName']=$this->projectcostsheet_model->projectName($param);
			$data['projectBudgetList']=$this->projectcostsheet_model->budgetList($param);			
			$this->load->view('projectcostsheet',$data);						
		} else {
			$this->load->view('login');
		}
	}
	
					
	
}
?>