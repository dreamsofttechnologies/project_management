<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('project_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){			
			$data['mode']="list";
			$data['projectList']=$this->project_model->get_project();
			//print_r($data);
			$this->load->view('project',$data);						
		}else{
			$this->load->view('login');
		}
	}
	
	public function add_project(){
		if($this->session->userdata('ccusr_id')){
			$data['mode']="add";
			$data['clientList']=$this->project_model->clientList();		
			$data['parentList']=$this->project_model->parentList();	
			$data['typeList']=$this->project_model->typeList();		
			$this->load->view('project',$data);								
		} else {
			$this->load->view('login');
		}
	}
		
	public function edit_project(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="edit";
			$data['clientList']=$this->project_model->clientList();		
			$data['parentList']=$this->project_model->parentList($param);	
			$data['typeList']=$this->project_model->typeList();
			$data['projectDets']=$this->project_model->get_projectDetails($param);			
			$this->load->view('project',$data);
		} else {
			$this->load->view('login');
		}
	}			
	
	public function save_project(){
		$this->project_model->save_project();
		redirect('project');		
	}
	
	public function update_project(){
		$this->project_model->update_project();
		redirect('project');		
	}
	
	public function delete_project(){
		$this->project_model->delete_project();
	}
						
}
?>