<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dailytask extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('dailytask_model');
	}
	
	public function index(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
									
			$data['mode']="list";
			$data['param']=$param;
			$data['dailyTaskList']=$this->dailytask_model->dailyTaskList($param);
			$data['taskDets']=$this->dailytask_model->taskDets($param);
			//print_r($data);
			$this->load->view('dailytask',$data);						
		}else{
			$this->load->view('login');
		}
	}
	
	public function add_dailytask(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="add";			
			//$data['params']=$this->dailytask_model->get_productParam($param);		
			$this->load->view('dailytask',$data);								
		}else{
			$this->load->view('login');
		}
	}
	
	public function edit_dailytask(){
		if($this->session->userdata('ccusr_id')){
			$par = $this->uri->segment(2, 0);
			$data['mode']="edit";
			$data['dailytask']=$this->dailytask_model->get_dailytaskDetails($par);			
			$this->load->view('dailytask',$data);
		}else{
			$this->load->view('login');
		}
	}			
	
	public function save_dailytask(){
		$taskID=$this->input->post('dailytask_task_id');
		$this->dailytask_model->save_dailytask();
		redirect('dailytask/'.$taskID);
	}
	
	public function update_dailytask(){
		$taskID=$this->input->post('dailytask_task_id');
		$this->dailytask_model->update_dailytask();
		redirect('dailytask/'.$taskID);
	}
	public function delete_dailytask(){
		$this->dailytask_model->delete_dailytask();
	}				
	
}
?>