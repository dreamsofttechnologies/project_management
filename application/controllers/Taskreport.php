<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Taskreport extends CI_Controller {
	function __construct(){
		parent::__construct();
		//echo "const";	
		$this->load->helper(array('form','url','nextcode_helper','function_helper'));
		$this->load->library(array('session','pagination'));
		$this->load->model('category_model');
	}
	public function index(){
		if($this->session->userdata('ccusr_id')){
			//print_r($_REQUEST);
			/*$pageno = $this->uri->segment(2, 0);
			$rownos = $this->uri->segment(3, 0);
			
			$total_rows=$this->category_model->total_records();
			$inp=function_helper();	
						
			$search_data=""; 
			if($this->input->post('searching')!=""){
				$search_data=$this->input->post('searching');
				//$pageno = $this->input->post('page');
				//$rownos = $this->input->post('rows');
			}
			
			
			$per_pg=$inp['record_per_page'];						
			//if($per_pg < $rownos){ $per_pg=$rownos; }			
			
			if($rownos==""){
				$rownos=$per_pg;
			}
			
			if($pageno>0){
				$page = $pageno;
			} else {
				$page = 0;
			}			
			
			$data['def_rows']=$inp['default_record_per_page'];
			$data['total_rows']=$total_rows;
			$data['row_limit']=$rownos;
			$data['pg']=$pageno;
			$data['rows']=$rownos;
			$data['search']=$search_data;			
			$data['mode']="list";
			//$data['category']=$this->category_model->get_category(false,$rownos,$page,$search_data);
			$data['category']=$this->category_model->get_category(false,$search_data);
			//print_r($data);
			$this->load->view('category',$data);*/
			
			
			$param = $this->uri->segment(2, 0);
									
			$search_data=""; 
			if($this->input->post('searching')!=""){
				$search_data=$this->input->post('searching');				
			}
			$data['mode']="list";
			$data['param']=$param;
			$data['catPastID']=$this->category_model->get_categoryID($param);
			$data['taskreport']=$this->category_model->get_category($param,$search_data);
			//print_r($data);
			$this->load->view('taskreport',$data);						
		}else{
			$this->load->view('login');
		}
	}
	
	public function add_taskreport(){
		if($this->session->userdata('ccusr_id')){
			$param = $this->uri->segment(2, 0);
			$data['mode']="add";
			if($param!=""){
				$data['catID']=$param;	
			} else {
				$data['catID']=0;	
			}
			//$data['params']=$this->category_model->get_productParam($param);		
			$this->load->view('taskreport',$data);								
		}else{
			$this->load->view('login');
		}
	}
	
	public function edit_category(){
		if($this->session->userdata('ccusr_id')){
			$par = $this->uri->segment(2, 0);
			$data['mode']="edit";
			$data['category']=$this->category_model->get_categoryDetails($par);			
			$this->load->view('category',$data);
		}else{
			$this->load->view('login');
		}
	}			
	
	public function save_category(){
		$catID=$this->input->post('catID');
		$this->category_model->save_category();
		if($catID==0){
			redirect('category');
		} else {
			redirect('listcategory/'.$catID);
		}
	}
	
	public function update_category(){
		$catID=$this->input->post('catID');
		$this->category_model->update_category();
		//redirect('category');
		if($catID==0){
			redirect('category');
		} else {
			redirect('listcategory/'.$catID);
		}
	}
	public function delete_category(){
		$this->category_model->delete_category();
	}				
	
}
?>