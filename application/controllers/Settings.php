<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Settings extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','form','function_helper'));
    	$this->load->model('settings_model');	
		$dat=$this->settings_model->get_settings();
		date_default_timezone_set($dat['set_timezone']);
		//echo date("l j \of F Y h:i:s A");
	}	
	
	public function index(){
		$data['mode']="list";
		//$data['currency']=$this->settings_model->get_currency();
		//$data['settings']=$this->settings_model->get_settings();
		$this->load->view('settings',$data);	
	}	
	
	public function save_settings(){
		$this->settings_model->save_settings();	
		redirect('settings');
	}
	
	public function setting_imageremove(){
		$pg_par=$this->input->post('id');
		$st=$this->settings_model->setting_imageremove($pg_par);	
		echo $st;
	}
}
