<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Changepassword extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','form','function_helper'));
		$this->load->library('session');
		$this->load->library('encryption');
    	$this->load->library(array('form_validation'));
		$this->load->model('changepassword_model');		
	}	
	
	//// Default Calling Method 
	public function index(){
		if($this->session->userdata('ccusr_id')){
			$data['mode']='list';
			$data['usr_id']=$this->session->userdata('ccusr_id');
			$this->load->view('changepassword',$data);	
		} else {
			$this->load->view('login');
		}					
	}	
	public function change_save(){		
		$data['result']=$this->changepassword_model->save_password();
		$data['mode']='list';
		$data['usr_id']=$this->session->userdata('ccusr_id');
		//redirect('changepassword');
		//$pg = $this->load->view('changepassword',$data,TRUE);	
		//echo $pg;
	}	
}
