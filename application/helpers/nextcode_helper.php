<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('GetNextCode'))
{
   function GetNextCode($code){
	preg_match('{(\d+)}', $code, $matches, PREG_OFFSET_CAPTURE); // Spilt Text and Number
	$code_prefix=substr($code,0,$matches[0][1]); // Get Prefix Text of Code
	$len=strlen($matches[0][0]);
	$next_number= str_pad(($matches[0][0]+1),$len,"0",STR_PAD_LEFT); // Format the Increment Number with Prefix 0
	$next_code=$code_prefix.$next_number;
	return ($next_code);
} 
}