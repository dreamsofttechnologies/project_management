<?php
class Project_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	//public function get_project($slug, $limit, $start, $search){
	public function get_project(){	
		$this->db->join('tbl_customer','customer_id=project_customer_id','left');				
		$this->db->join('tbl_project_type','project_type_id=project_type','left');				
		$qry=$this->db->select('project_id, project_name, customer_name, project_type_name, project_due_date, project_status')->get_where('tbl_project');
		$result=$qry->result_array();		
		return $result;        
	}
	
	public function clientList(){
		$qry=$this->db->select('customer_id, customer_name')->get_where('tbl_customer');
		return $qry->result_array();
	}
	
	public function parentList($param=NULL){
		if($param!=""){
			$this->db->where('project_id!=',$param);
		}
		$qry=$this->db->select('project_id, project_name')->get_where('tbl_project');
		return $qry->result_array();
	}
	
	public function typeList(){
		$qry=$this->db->select('project_type_id, project_type_name')->get_where('tbl_project_type');
		return $qry->result_array();
	}
	
	public function get_projectDetails($id){
		$qry=$this->db->select('*')->get_where('tbl_project',array('project_id'=>$id));
		return $qry->row_array();
	}		
	
	public function save_project(){
		//print_r($_REQUEST);
		//print_r($_FILES);
		$date=date('Y-m-d H:i:s');		
		
		$start=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('project_start_date'))));
		$due=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('project_due_date'))));
		$delivery=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('project_delivery_date'))));
		
		$data=array(
			'project_name' => $this->input->post('project_name'),
			'project_customer_id' => $this->input->post('project_customer_id'),
			'project_type' => $this->input->post('project_type'),			
			'project_start_date' => $start,
			'project_due_date' => $due,
			'project_parent' => $this->input->post('project_parent'),			
			'project_description' => $this->input->post('project_description'),
			'project_delivery_date' => $delivery,
			'project_delivery_description' => $this->input->post('project_delivery_description'),			
			'project_status' => $this->input->post('project_status'),			
			'project_created_on' => $date,			
			'project_mod_on' => $date			
		);
		$this->db->insert('tbl_project',$data);				
	}
	
	public function update_project(){
		//print_r($_REQUEST);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$id=$this->input->post('project_id');
		
		$start=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('project_start_date'))));
		$due=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('project_due_date'))));
		$delivery=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('project_delivery_date'))));
		
		$data=array(
			'project_name' => $this->input->post('project_name'),
			'project_customer_id' => $this->input->post('project_customer_id'),
			'project_type' => $this->input->post('project_type'),			
			'project_start_date' => $start,
			'project_due_date' => $due,
			'project_parent' => $this->input->post('project_parent'),			
			'project_description' => $this->input->post('project_description'),
			'project_delivery_date' => $delivery,
			'project_delivery_description' => $this->input->post('project_delivery_description'),			
			'project_status' => $this->input->post('project_status'),			
			'project_created_on' => $date,			
			'project_mod_on' => $date	
		);
		$this->db->update('tbl_project',$data,array('project_id'=>$id));					
	}
	
	public function delete_project(){
		$id=$this->input->post('id');			
		$this->db->delete('tbl_project', array('project_id' => $id));
		echo 'deleted';
	}
						
}
?>