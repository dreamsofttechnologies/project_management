<?php
class Projectcostsheet_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  	
	
	public function projectName($param=NULL){
		$qry=$this->db->select('project_name')->get_where('tbl_project',array('project_id'=>$param));
		$r=$qry->row_array();
		return $r['project_name'];
	}
				
	public function budgetList($param){
		$this->db->join('tbl_budget_head','budget_head_id=budget_budgethead_id','left');
		//$this->db->join('tbl_project_task','task_project_id=budget_project_id and task_budget_id=budget_head_id','left');
		$qry=$this->db->select('budget_head_id, budget_project_id, budget_head_name, budget_amount')->get_where('tbl_budget',array('budget_project_id'=>$param));
		$result=$qry->result_array();		
		foreach($result as $ind=>$row){
			$result[$ind]['estTime']=$this->getEstTime($row['budget_project_id'],$row['budget_head_id']);
			$result[$ind]['actTime']=$this->getActTime($row['budget_project_id'],$row['budget_head_id']);
			$result[$ind]['actCost']=$this->getActCost($row['budget_project_id'],$row['budget_head_id']);
			$result[$ind]['staffList']=$this->staffList($row['budget_project_id'],$row['budget_head_id']);
		}
		return $result;        
	}
	
	public function staffList($projectID,$bedgetID){
		$this->db->join('tbl_staff','staff_id=task_assigned','left');
		$qry=$this->db->select('task_project_id, task_budget_id, task_assigned, sum(task_est_hours) as staff_estTime, staff_name, task_assigned')->group_by('task_assigned')->get_where('tbl_project_task',array('task_project_id'=>$projectID,'task_budget_id'=>$bedgetID));
		$result=$qry->result_array();		
		foreach($result as $ind=>$row){
			$result[$ind]['staff_estCost']=$this->getEstCost($projectID,$bedgetID,$row['task_assigned']);
			$result[$ind]['staff_actTime']=$this->getActTime($projectID,$bedgetID,$row['task_assigned']);
			$result[$ind]['staff_actCost']=$this->getActCost($projectID,$bedgetID,$row['task_assigned']);
		}
		return $result; 
	}
	
	public function getActTime($projectID,$bedgetID,$staffID=NULL){
		if($staffID!=""){
			$this->db->where('task_assigned',$staffID);
		}
		
		$this->db->join('tbl_daily_task','dailytask_task_id=task_id','left');
		$qry=$this->db->select('sum(dailytask_hours) as sm')->get_where('tbl_project_task',array('task_project_id'=>$projectID,'task_budget_id'=>$bedgetID));
		$r=$qry->row_array();
		if($r['sm']>0){
			return $r['sm'];
		} else {
			return 0;
		}
	}
	
	public function getEstTime($projectID,$bedgetID){
		$qry=$this->db->select('sum(task_est_hours) as sm')->get_where('tbl_project_task',array('task_project_id'=>$projectID,'task_budget_id'=>$bedgetID));
		$r=$qry->row_array();
		if($r['sm']>0){
			return $r['sm'];
		} else {
			return 0;
		}
	}
	
	public function getActCost($projectID,$bedgetID,$staffID=NULL){
		if($staffID!=""){
			$this->db->where('task_assigned',$staffID);
		}
		$this->db->join('tbl_daily_task','dailytask_task_id=task_id','left');
		$qry=$this->db->select('dailytask_hours, dailytask_staff_id')->get_where('tbl_project_task',array('task_project_id'=>$projectID,'task_budget_id'=>$bedgetID));
		$result=$qry->result_array();
		$cost=0;	
		foreach($result as $row){
			$qry2=$this->db->select('team_rate')->get_where('tbl_team',array('team_project_id'=>$projectID,'team_staff_id'=>$row['dailytask_staff_id']));
			$r2=$qry2->row_array();
			$cost=$cost+($row['dailytask_hours']*$r2['team_rate']);
		}
		return $cost;
	}
	
	public function getEstCost($projectID,$bedgetID,$staffID){		
		$qry=$this->db->select('task_est_hours, task_assigned')->get_where('tbl_project_task',array('task_project_id'=>$projectID,'task_budget_id'=>$bedgetID,'task_assigned'=>$staffID));
		$result=$qry->result_array();
		$cost=0;	
		foreach($result as $row){
			$qry2=$this->db->select('team_rate')->get_where('tbl_team',array('team_project_id'=>$projectID,'team_staff_id'=>$row['task_assigned']));
			$r2=$qry2->row_array();
			$cost=$cost+($row['task_est_hours']*$r2['team_rate']);
		}
		return $cost;
	}			
}
?>