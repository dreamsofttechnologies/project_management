<?php
class Team_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	//public function get_team($slug, $limit, $start, $search){
	public function get_team($param=NULL){			
		$this->db->join('tbl_staff','staff_id=team_staff_id','left');
		$qry=$this->db->select('team_id, team_staff_id, staff_name, team_rate')->get_where('tbl_team',array('team_project_id'=>$param));
		$result=$qry->result_array();		
		return $result;        
	}
	
	public function staffList(){	
		$this->db->join('tbl_stafftype','staff_type_id=staff_type','left');					
		$qry=$this->db->select('staff_id, staff_name, staff_mobile, staff_type_name')->get_where('tbl_staff');
		$result=$qry->result_array();		
		return $result;        
	}				
	
	public function save_team(){
		//print_r($_REQUEST);
		//print_r($_FILES);
		
		$projectID=$this->input->post('projectID');
		$hidCount=$this->input->post('hidCount');
		
		$this->db->delete('tbl_team', array('team_project_id' => $projectID));
		for($i=1;$i<=$hidCount;$i++){
			/*if($this->input->post('team_id'.$i)!=""){
				$data=array(			
					'team_project_id' => $projectID,
					'team_staff_id' => $this->input->post('team_staff_id'.$i),
					'team_rate' => $this->input->post('team_rate'.$i)
				);
				$param=array(			
					'team_id' => $this->input->post('team_id'.$i),
				);
				$this->db->update('tbl_team',$data,$param);
			} else {*/
				$data=array(			
					'team_project_id' => $projectID,
					'team_staff_id' => $this->input->post('team_staff_id'.$i),
					'team_rate' => $this->input->post('team_rate'.$i)
				);
				$this->db->insert('tbl_team',$data);
			//}
		}
	}			
}
?>