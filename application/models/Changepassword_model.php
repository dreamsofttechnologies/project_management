<?php
class Changepassword_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
		$this->_salt = "123456789987654321";
	}
	public function save_password(){			
		//print_r($_REQUEST);
		$date=date("Y-m-d");		
		$id=$this->input->post('usr_id');
		
		$cp = $this->input->post('usr_currpwd');
		$this->db->select('admin_id, admin_password')->from('tbl_admin')->where(array('admin_id'=>$id));
		$cst_qry = $this->db->get();		
		$cr=$cst_qry->row_array();
		//echo $cr['adm_password'].'chk'.$pwd;
		$msg = $cr['admin_password'];		
		$pwd = $this->encryption->decrypt($msg);
		//echo $pwd;
		//echo $cp.'=='.$pwd;
		if($cp==$pwd){
		
			$np = $this->input->post('usr_newpwd');			
			$nppwd = $this->encryption->encrypt($np);
			$data = array(
				'admin_password' => $nppwd,
			);		
			$this->db->update('tbl_admin', $data,array('admin_id'=>$cr['admin_id']));
			$this->session->set_flashdata("message","Password Changed Successfully.");	
		} else {
			$this->session->set_flashdata("message","Old Password is Wrong.");	
		}
	}
}	
?>
