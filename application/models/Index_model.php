<?php
class Index_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
		$this->_salt = "123456789987654321";
	}  
	
	public function check_login($data){
		$this->db->where('admin_username', $data['adm_username']);
		$query = $this->db->get('tbl_admin');
		$result = $query->row_array();
		
		$msg = $result['admin_password'];		
		$password = $this->encryption->decrypt($msg);		
		
		//echo $password." == ".$data['adm_password'];
		if($password == $data['adm_password']){			
			$session_data = array(
				'ccusr_id' => $result['admin_id'],
				'ccusr_name' => $result['admin_name']							
			);
			$this->session->set_userdata($session_data);
			
			$data=array(
				'admin_last_login' => date('Y-m-d H:i:s')
			);
			$this->db->update('tbl_admin',$data,array('admin_id'=>$result['admin_id']));
			
			return TRUE;
		} else 	{
			return FALSE;
		}
	}
	
	
	public function total_orderrecords($slug){
		if($slug!=""){
			$this->db->select('*')->from('tbl_orders')->where('order_status',$slug);
		    $cnt_order = $this->db->get();
			//echo $this->db->last_query();
			$data['count']=$cnt_order->num_rows();;
			
			$this->db->select('*')->from('tbl_orders')->where(array('order_status' => $slug,'order_paymentstatus' =>'Paid'));
		    $cnt_paid = $this->db->get();
			//echo $this->db->last_query();			
			$data['paid']=$cnt_paid->num_rows();;			
			return $data;
		}
	}
	
	public function get_bestproducts($limit){			
		$this->db->select(array('*','count(cart_prdid) as cnt'))->from('tbl_cart')->order_by('cnt','desc')->group_by('cart_prdid')->limit(8);
		$query=$this->db->get();
        //echo $this->db->last_query();
		return $query->result_array();
   	}
	
	public function get_attachment($prd_id){
		$this->db->select('att_name')->from('tbl_attachment')->order_by('att_id','asc')->where('att_prdid',$prd_id)->limit(1);
		$query=$this->db->get();
        //echo $this->db->last_query();
		return $query->result_array();
   	}
	
}
?>