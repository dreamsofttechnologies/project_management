<?php
class Taskreport_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	//public function get_category($slug, $limit, $start, $search){
	public function get_category($slug, $search){
		$ser_data=$search;
		if($ser_data!=""){
			$this->db->like('category_name', $ser_data);
            $this->db->or_like('category_status', $ser_data);					
		}
		//$this->db->limit($limit, $start);
		if($slug){
			$this->db->where('category_cat_id',$slug);
		} else {
			$this->db->where('category_cat_id',0);
		} 						
		$qry=$this->db->select('category_id, category_cat_id, category_name, category_status')->get_where('tbl_category');
		$result=$qry->result_array();		
		return $result;        
	}
	
	
	public function total_records($slug){
		if($slug){
			$this->db->where('category_cat_id',$slug);
		} else {
			$this->db->where('category_cat_id',0);
		} 
		$ser_data=$this->input->post('searching');
		if($ser_data!=""){			
			$this->db->like('category_name', $ser_data);
            $this->db->or_like('category_status', $ser_data);	
			$this->db->select('category_id, category_name, category_status')->from('tbl_category');
		    $query = $this->db->get();
			return $query->num_rows(); 			
		} else {
			return $this->db->count_all_results('tbl_category');	
		}	
 	}
	
	public function get_categoryDetails($id){
		$qry=$this->db->select('category_id, category_cat_id, category_name, category_order, category_status')->get_where('tbl_category',array('category_id'=>$id));
		return $qry->row_array();
	}
	
	public function get_categoryID($id){
		$qry=$this->db->select('category_cat_id')->get_where('tbl_category',array('category_id'=>$id));
		$r=$qry->row_array();
		return $r['category_cat_id'];
	}
	
	public function save_category(){
		//print_r($_REQUEST);
		//print_r($_FILES);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$data=array(
			'category_cat_id' => $this->input->post('catID'),
			'category_name' => $this->input->post('category_name'),
			'category_order' => $this->input->post('category_order'),			
			'category_status' => $this->input->post('category_status'),
			'category_created_on' => $date,			
			'category_mod_on' => $date			
		);
		$this->db->insert('tbl_category',$data);				
	}
	public function update_category(){
		//print_r($_REQUEST);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$id=$this->input->post('category_id');
		$data=array(
			'category_name' => $this->input->post('category_name'),
			'category_order' => $this->input->post('category_order'),			
			'category_status' => $this->input->post('category_status'),
			'category_mod_on' => $date
		);
		$this->db->update('tbl_category',$data,array('category_id'=>$id));					
	}
	
	public function delete_category(){
		$id=$this->input->post('id');			
		$this->db->delete('tbl_category', array('category_id' => $id));
		echo 'deleted';
	}
	
	public function get_nextcode(){
		$this->db->select('category_code')->from('tbl_category')->order_by('category_id','desc')->limit(1);
		$query = $this->db->get();
		$rw=$query->row_array();	
		if($rw['category_code']==""){
			$bno="CUS001";
		} else {
			$bno=GetNextCode($rw['category_code']);
		}
		return $bno;
	}						
}
?>