<?php
class Banner_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	public function get_banner($slug, $limit, $start, $search){
		$ser_data=$search;
		if($ser_data!=""){
			$this->db->like('banner_name', $ser_data);
            $this->db->or_like('banner_status', $ser_data);					
		}
		$this->db->limit($limit, $start);
		if ($slug === FALSE){						
			$qry=$this->db->select('banner_id, banner_name, banner_status')->get_where('tbl_banner');
			$result=$qry->result_array();			
			return $result;
        }
	}	
	
	public function total_records(){
		$ser_data=$this->input->post('searching');
		if($ser_data!=""){
			$this->db->like('banner_name', $ser_data);
            $this->db->or_like('banner_status', $ser_data);	
			$this->db->select('banner_id, banner_name, banner_status')->from('tbl_banner');
		    $query = $this->db->get();
			return $query->num_rows(); 			
		}else{			
			return $this->db->count_all_results('tbl_banner');	
		}	
 	}
	
	public function get_bannerDetails($id){
		$qry=$this->db->select('banner_id, banner_name, banner_image, banner_link, banner_status')->get_where('tbl_banner',array('banner_id'=>$id));
		return $qry->row_array();
	}
				
	public function banner_imageremove($slug){			
		$query = $this->db->get_where('tbl_banner', array('banner_id' => $slug));
		$prd=$query->row_array();
		unlink('./images/banner/'.$prd['banner_image']);		
		
		$data = array(
			'banner_image' => ''
		);									
		return $this->db->update('tbl_banner', $data, array('banner_id' => $slug));
	}				
	
	public function save_banner(){
		//print_r($_REQUEST);
		//print_r($_FILES);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$data=array(
			'banner_name' => $this->input->post('banner_name'),
			'banner_link' => $this->input->post('banner_link'),
			'banner_status' => $this->input->post('banner_status')			
		);
		$this->db->insert('tbl_banner',$data);
		$id=$this->db->insert_id();
		
		if($_FILES['banner_image']['name']!=""){			
			$config['upload_path']          = './images/banner/';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$config['max_size']             = 1024000;
			$config['max_width']            = 2000;
			$config['max_height']           = 1000;
			//print_r($config);
			$this->load->library('upload', $config);			
			
			$new_name=$id."_".$_FILES['banner_image']['name'];
		 	$_FILES['banner_image']['name']=$new_name;
			
			$this->upload->do_upload('banner_image');
			//echo $this->upload->display_errors(); 
			$data11 = array('upload_data' => $this->upload->data());
			$upload_data = $this->upload->data();
			
			//$value=$new_name;
			$data1 = array(
				'banner_image' => $upload_data['file_name']			   
			);				
			$this->db->update('tbl_banner', $data1, array('banner_id' => $id));						
		}		
	}
	
	public function update_banner(){
		//print_r($_REQUEST);		
		$id=$this->input->post('banner_id');
		$data=array(
			'banner_name' => $this->input->post('banner_name'),
			'banner_link' => $this->input->post('banner_link'),
			'banner_status' => $this->input->post('banner_status')			
		);
		$this->db->update('tbl_banner',$data,array('banner_id'=>$id));			
		if($_FILES['banner_image']['name']!=""){
			$nam='cat_image';
			$config['upload_path']          = './images/banner/';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$config['max_size']             = 1024000;
			$config['max_width']            = 2000;
			$config['max_height']           = 1000;
			//print_r($config);
			$this->load->library('upload', $config);
			
			$new_name=$id."_".$_FILES['banner_image']['name'];
		 	$_FILES['banner_image']['name']=$new_name;
			
			$this->upload->do_upload('banner_image');
			//echo $this->upload->display_errors(); 
			$data11 = array('upload_data' => $this->upload->data());
			$upload_data = $this->upload->data();			
			//print_r($upload_data);
			$data1 = array(
				'banner_image' => $upload_data['file_name']			   
			);				
			$this->db->update('tbl_banner', $data1, array('banner_id' => $id));						
		}
	}
	
	public function delete_banner(){
		$id=$this->input->post('id');
		$query = $this->db->get_where('tbl_banner', array('banner_id' => $id));
		$prd=$query->row_array();
		unlink('./images/banner/'.$prd['banner_image']);		
		
		$data = array(
			'banner_image' => ''
		);									
		$this->db->update('tbl_banner', $data, array('banner_id' => $id));
		
		$this->db->delete('tbl_banner', array('banner_id' => $id));
		echo 'deleted';
	}
		
}
?>