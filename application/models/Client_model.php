<?php
class Client_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	//public function get_client($slug, $limit, $start, $search){
	public function get_client(){		
		$qry=$this->db->select('customer_id, customer_code, customer_name, customer_address, customer_city, customer_country, customer_type, customer_status')->get_where('tbl_customer');
		$result=$qry->result_array();		
		return $result;        
	}
			
	
	public function get_clientDetails($id){
		$qry=$this->db->select('*')->get_where('tbl_customer',array('customer_id'=>$id));
		return $qry->row_array();
	}
		
	
	public function save_client(){
		//print_r($_REQUEST);
		//print_r($_FILES);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$data=array(
			'customer_code' => $this->input->post('customer_code'),
			'customer_name' => $this->input->post('customer_name'),
			'customer_address' => $this->input->post('customer_address'),			
			'customer_city' => $this->input->post('customer_city'),
			'customer_country' => $this->input->post('customer_country'),
			'customer_email' => $this->input->post('customer_email'),
			'customer_mobile' => $this->input->post('customer_mobile'),			
			'customer_gstno' => $this->input->post('customer_gstno'),
			'customer_type' => $this->input->post('customer_type'),			
			'customer_remarks' => $this->input->post('customer_remarks'),
			'customer_status' => $this->input->post('customer_status'),
			'customer_created_on' => $date,			
			'customer_mod_on' => $date			
		);
		$this->db->insert('tbl_customer',$data);				
	}
	public function update_client(){
		//print_r($_REQUEST);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$id=$this->input->post('customer_id');
		$data=array(
			'customer_code' => $this->input->post('customer_code'),
			'customer_name' => $this->input->post('customer_name'),
			'customer_address' => $this->input->post('customer_address'),			
			'customer_city' => $this->input->post('customer_city'),
			'customer_country' => $this->input->post('customer_country'),
			'customer_email' => $this->input->post('customer_email'),
			'customer_mobile' => $this->input->post('customer_mobile'),			
			'customer_gstno' => $this->input->post('customer_gstno'),
			'customer_type' => $this->input->post('customer_type'),			
			'customer_remarks' => $this->input->post('customer_remarks'),
			'customer_status' => $this->input->post('customer_status'),	
			'customer_mod_on' => $date		
		);
		$this->db->update('tbl_customer',$data,array('customer_id'=>$id));					
	}
	
	public function delete_client(){
		$id=$this->input->post('id');			
		$this->db->delete('tbl_customer', array('customer_id' => $id));
		echo 'deleted';
	}
	
	public function get_nextcode(){
		$this->db->select('customer_code')->from('tbl_customer')->order_by('customer_id','desc')->limit(1);
		$query = $this->db->get();
		$rw=$query->row_array();	
		if($rw['customer_code']==""){
			$bno="CLIENT001";
		} else {
			$bno=GetNextCode($rw['customer_code']);
		}
		return $bno;
	}						
}
?>