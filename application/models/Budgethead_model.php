<?php
class Budgethead_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	//public function get_budget_head($slug, $limit, $start, $search){
	public function get_budgethead(){
		$qry=$this->db->select('budget_head_id, budget_head_name')->get_where('tbl_budget_head');
		$result=$qry->result_array();		
		return $result;        
	}				
	
	public function save_budgethead(){
		//print_r($_REQUEST);
		//print_r($_FILES);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		if($this->input->post('budget_head_id')!=""){
			$data=array(			
				'budget_head_name' => $this->input->post('budget_head_name'),					
			);
			$param=array(			
				'budget_head_id' => $this->input->post('budget_head_id'),					
			);
			$this->db->update('tbl_budget_head',$data,$param);					
		} else {
			$data=array(			
				'budget_head_name' => $this->input->post('budget_head_name'),					
			);
			$this->db->insert('tbl_budget_head',$data);
		}
		
	}
		
	public function delete_budgethead(){
		$id=$this->input->post('id');			
		$this->db->delete('tbl_budget_head', array('budget_head_id' => $id));
		echo 'deleted';
	}	
}	
?>