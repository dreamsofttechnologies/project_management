<?php
class Production_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	public function staffList(){
		$qry=$this->db->select('staff_id, staff_name')->get_where('tbl_staff');
		return $qry->result_array();
	}
	
	public function staffTaskList(){
		$staffID=$this->input->post('staffID');
		$fromDate=$this->input->post('fromDate');
		$toDate=$this->input->post('toDate');
		
		if($staffID!=""){
			$this->db->where('task_assigned',$staffID);
		}
		$this->db->join('tbl_project','project_id=task_project_id','left');
		//$this->db->join('tbl_budget','budget_project_id=task_project_id and budget_budgethead_id=task_budget_id','left');
		$qry=$this->db->select('project_name, task_project_id, task_budget_id, task_assigned, sum(task_est_hours) as estTime')->group_by('task_project_id')->get_where('tbl_project_task');
		//echo $this->db->last_query();
		$result=$qry->result_array();		
		foreach($result as $ind=>$row){
			//$result[$ind]['estTime']=$this->getEstTime($row['task_project_id'],$row['task_budget_id']);
			$result[$ind]['estCost']=$this->getEstCost($row['task_project_id'],$row['task_assigned']);
			
			$result[$ind]['actTime']=$this->getActTime($row['task_project_id'],$row['task_assigned']);
			$result[$ind]['actCost']=$this->getActCost($row['task_project_id'],$row['task_assigned']);
			
			$result[$ind]['periodTime']=$this->getPeriodTime($row['task_project_id'],$row['task_assigned'],$fromDate,$toDate);
			$result[$ind]['periodCost']=$this->getPeriodCost($row['task_project_id'],$row['task_assigned'],$fromDate,$toDate);			
		}
		return $result;        
	}
	
	
	public function getActTime($projectID,$staffID=NULL){
		if($staffID!=""){
			$this->db->where('task_assigned',$staffID);
		}
		
		$this->db->join('tbl_daily_task','dailytask_task_id=task_id','left');
		$qry=$this->db->select('sum(dailytask_hours) as sm')->get_where('tbl_project_task',array('task_project_id'=>$projectID));
		$r=$qry->row_array();
		if($r['sm']>0){
			return $r['sm'];
		} else {
			return 0;
		}
	}
	
	public function getEstTime($projectID,$bedgetID){
		$qry=$this->db->select('sum(task_est_hours) as sm')->get_where('tbl_project_task',array('task_project_id'=>$projectID,'task_budget_id'=>$bedgetID));
		$r=$qry->row_array();
		if($r['sm']>0){
			return $r['sm'];
		} else {
			return 0;
		}
	}
	
	public function getActCost($projectID,$staffID=NULL){
		if($staffID!=""){
			$this->db->where('task_assigned',$staffID);
		}
		$this->db->join('tbl_daily_task','dailytask_task_id=task_id','left');
		$qry=$this->db->select('dailytask_hours, dailytask_staff_id')->get_where('tbl_project_task',array('task_project_id'=>$projectID));
		$result=$qry->result_array();
		$cost=0;	
		foreach($result as $row){
			$qry2=$this->db->select('team_rate')->get_where('tbl_team',array('team_project_id'=>$projectID,'team_staff_id'=>$row['dailytask_staff_id']));
			$r2=$qry2->row_array();
			$cost=$cost+($row['dailytask_hours']*$r2['team_rate']);
		}
		return $cost;
	}
	
	public function getEstCost($projectID,$staffID){		
		$qry=$this->db->select('task_est_hours, task_assigned')->get_where('tbl_project_task',array('task_project_id'=>$projectID,'task_assigned'=>$staffID));
		$result=$qry->result_array();
		$cost=0;	
		foreach($result as $row){
			$qry2=$this->db->select('team_rate')->get_where('tbl_team',array('team_project_id'=>$projectID,'team_staff_id'=>$row['task_assigned']));
			$r2=$qry2->row_array();
			$cost=$cost+($row['task_est_hours']*$r2['team_rate']);
		}
		return $cost;
	}
	
	public function getPeriodTime($projectID,$staffID,$from,$to){
		if($staffID!=""){
			$this->db->where('dailytask_staff_id',$staffID);
		}
		
		if($from!="" && $to!=""){
			$from=date("Y-m-d",strtotime(str_replace("/","-",$from)));
			$to=date("Y-m-d",strtotime(str_replace("/","-",$to)));
			
			$this->db->where('dailytask_date>=',$from);
			$this->db->where('dailytask_date<=',$to);
		}				
		
		$qry=$this->db->select('sum(dailytask_hours) as sm')->get_where('tbl_daily_task',array('dailytask_project_id'=>$projectID));
		//return $this->db->last_query();		
		$r=$qry->row_array();
		if($r['sm']>0){
			return $r['sm'];
		} else {
			return 0;
		}
	}
	
	public function getPeriodCost($projectID,$staffID=NULL,$from,$to){
		if($staffID!=""){
			$this->db->where('dailytask_staff_id',$staffID);
		}
		
		if($from!="" && $to!=""){
			$from=date("Y-m-d",strtotime(str_replace("/","-",$from)));
			$to=date("Y-m-d",strtotime(str_replace("/","-",$to)));
			
			$this->db->where('dailytask_date>=',$from);
			$this->db->where('dailytask_date<=',$to);
		}
				
		$qry=$this->db->select('dailytask_hours, dailytask_staff_id')->get_where('tbl_daily_task',array('dailytask_project_id'=>$projectID));
		$result=$qry->result_array();
		$cost=0;	
		foreach($result as $row){
			$qry2=$this->db->select('team_rate')->get_where('tbl_team',array('team_project_id'=>$projectID,'team_staff_id'=>$row['dailytask_staff_id']));
			$r2=$qry2->row_array();
			$cost=$cost+($row['dailytask_hours']*$r2['team_rate']);
		}
		return $cost;
	}
							
}
?>