<?php
class Quotation_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	//public function get_quotation($slug, $limit, $start, $search){
	public function get_quotation($param){		
		$this->db->join('tbl_customer','customer_id=quotation_client_id','left');				
		$qry=$this->db->select('quotation_id, quotation_no, quotation_date, customer_name, quotation_title, quotation_nett_amount, quotation_status')->get_where('tbl_quotation',array('quotation_project_id'=>$param));
		$result=$qry->result_array();		
		return $result;        
	}
	
	public function projectDets($param=NULL){		
		$this->db->join('tbl_customer','customer_id=project_customer_id','left');			
		$qry=$this->db->select('project_id, project_name, customer_id, customer_name')->get_where('tbl_project',array('project_id'=>$param));
		$result=$qry->row_array();		
		return $result;        
	}
	
	public function projectName($param){
		$qry=$this->db->select('project_name')->get_where('tbl_project',array('project_id'=>$param));
		$r=$qry->row_array();
		return $r['project_name'];
	}
	
	public function clientList(){
		$qry=$this->db->select('customer_id, customer_name')->get_where('tbl_customer');
		return $qry->result_array();
	}
	
	public function serviceList(){
		$qry=$this->db->select('service_id, service_name, service_description, service_amount')->get_where('tbl_service');
		return $qry->result_array();
	}
	
	public function get_quotationDetails($id){
		$this->db->join('tbl_project','project_id=quotation_project_id','left');
		$this->db->join('tbl_customer','customer_id=quotation_client_id','left');
		$qry=$this->db->select('tbl_quotation.*, project_name, customer_name')->get_where('tbl_quotation',array('quotation_id'=>$id));
		return $qry->row_array();
	}
	
	public function get_quotationItmDetails($id){		
		$qry=$this->db->select('*')->get_where('tbl_quotation_items',array('quotationitm_quotation_id'=>$id));
		return $qry->result_array();
	}
	
	public function get_quotationID($id){
		$qry=$this->db->select('quotation_cat_id')->get_where('tbl_quotation',array('quotation_id'=>$id));
		$r=$qry->row_array();
		return $r['quotation_cat_id'];
	}
	
	public function save_quotation(){
		//print_r($_REQUEST);
		//print_r($_FILES);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$dat=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('quotation_date'))));
		$data=array(
			'quotation_no' => $this->input->post('quotation_no'),
			'quotation_date' => $dat,
			'quotation_client_id' => $this->input->post('quotation_client_id'),			
			'quotation_project_id' => $this->input->post('quotation_project_id'),
			'quotation_title' => $this->input->post('quotation_title'),			
			'quotation_gross_amount' => $this->input->post('fn_gross'),
			'quotation_tax_amount' => $this->input->post('fn_tax'),			
			'quotation_discount_amount' => $this->input->post('fn_dis'),			
			'quotation_nett_amount' => $this->input->post('fn_net'),						
			'quotation_note' => $this->input->post('quotation_note'),
			'quotation_status' => $this->input->post('quotation_status'),						
			'quotation_created_on' => $date,			
			'quotation_mod_on' => $date			
		);
		$this->db->insert('tbl_quotation',$data);	
		$quotationID=$this->db->insert_id();
		
		$hidCount=$this->input->post('hidCount');
		
		for($i=1;$i<=$hidCount;$i++){
			$data_itm=array(
				'quotationitm_quotation_id' => $this->input->post('quotation_no'),				
				'quotationitm_title' => $this->input->post('quotationitm_title'.$i),			
				'quotationitm_description' => $this->input->post('quotationitm_description'.$i),
				'quotationitm_rate' => $this->input->post('quotationitm_rate'.$i),			
				'quotationitm_tax' => $this->input->post('quotationitm_tax'.$i),
				'quotationitm_discount' => $this->input->post('quotationitm_discount'.$i),			
				'quotationitm_amount' => $this->input->post('quotationitm_amount'.$i)				
			);			
			$this->db->insert('tbl_quotation_items',$data_itm);	
		}			
	}
	
	public function update_quotation(){
		//print_r($_REQUEST);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$id=$this->input->post('quotation_id');
		$dat=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('quotation_date'))));
		$data=array(
			'quotation_no' => $this->input->post('quotation_no'),
			'quotation_date' => $dat,
			'quotation_client_id' => $this->input->post('quotation_client_id'),			
			'quotation_project_id' => $this->input->post('quotation_project_id'),
			'quotation_title' => $this->input->post('quotation_title'),			
			'quotation_gross_amount' => $this->input->post('fn_gross'),
			'quotation_tax_amount' => $this->input->post('fn_tax'),			
			'quotation_discount_amount' => $this->input->post('fn_dis'),			
			'quotation_nett_amount' => $this->input->post('fn_net'),						
			'quotation_note' => $this->input->post('quotation_note'),
			'quotation_status' => $this->input->post('quotation_status'),											
			'quotation_mod_on' => $date			
		);
		$param=array(
			'quotation_id' => $id
		);
		$this->db->update('tbl_quotation',$data,$param);	
		
		$hidCount=$this->input->post('hidCount');
		$this->db->delete('tbl_quotation_items', array('quotationitm_quotation_id' => $id));
		for($i=1;$i<=$hidCount;$i++){
			$data_itm=array(
				'quotationitm_quotation_id' => $this->input->post('quotation_no'),				
				'quotationitm_title' => $this->input->post('quotationitm_title'.$i),			
				'quotationitm_description' => $this->input->post('quotationitm_description'.$i),
				'quotationitm_rate' => $this->input->post('quotationitm_rate'.$i),			
				'quotationitm_tax' => $this->input->post('quotationitm_tax'.$i),
				'quotationitm_discount' => $this->input->post('quotationitm_discount'.$i),			
				'quotationitm_amount' => $this->input->post('quotationitm_amount'.$i)				
			);			
			$this->db->insert('tbl_quotation_items',$data_itm);	
		}
	}
	
	public function delete_quotation(){
		$id=$this->input->post('id');			
		$this->db->delete('tbl_quotation', array('quotation_id' => $id));
		$this->db->delete('tbl_quotation_items', array('quotationitm_quotation_id' => $id));
		echo 'deleted';
	}
	
	public function get_nextcode(){
		$this->db->select('quotation_no')->from('tbl_quotation')->order_by('quotation_id','desc')->limit(1);
		$query = $this->db->get();
		$rw=$query->row_array();	
		if($rw['quotation_no']==""){
			$bno="1";
		} else {
			$bno=GetNextCode($rw['quotation_no']);
		}
		return $bno;
	}						
}
?>