<?php
class Invoice_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	//public function get_invoice($slug, $limit, $start, $search){
	public function get_invoice($param){		
		$this->db->join('tbl_customer','customer_id=invoice_client_id','left');				
		//$this->db->join('tbl_payment_received','pr_invoice_id=invoice_id','left');				
		$qry=$this->db->select('invoice_id, invoice_no, invoice_date, customer_name, invoice_title, invoice_nett_amount, invoice_status')->get_where('tbl_invoice',array('invoice_project_id'=>$param));
		//return $this->db->last_query(); 
		$result=$qry->result_array();
		foreach($result as $ind=>$row){
			$result[$ind]['paid']=$this->paymentDets($row['invoice_id']);
		}		
		return $result;        
	}
	
	public function paymentDets($param=NULL){					
		$qry=$this->db->select('sum(pr_amount) as paid')->get_where('tbl_payment_received',array('pr_invoice_id'=>$param));
		$result=$qry->row_array();		
		return $result['paid'];        
	}
	
	public function projectDets($param=NULL){		
		$this->db->join('tbl_customer','customer_id=project_customer_id','left');			
		$qry=$this->db->select('project_id, project_name, customer_id, customer_name')->get_where('tbl_project',array('project_id'=>$param));
		$result=$qry->row_array();		
		return $result;        
	}
	
	public function projectName($param){
		$qry=$this->db->select('project_name')->get_where('tbl_project',array('project_id'=>$param));
		$r=$qry->row_array();
		return $r['project_name'];
	}
	
	public function clientList(){
		$qry=$this->db->select('customer_id, customer_name')->get_where('tbl_customer');
		return $qry->result_array();
	}
	
	public function serviceList(){
		$qry=$this->db->select('service_id, service_name, service_description, service_amount')->get_where('tbl_service');
		return $qry->result_array();
	}
	
	public function get_invoiceDetails($id){
		$this->db->join('tbl_project','project_id=invoice_project_id','left');
		$this->db->join('tbl_customer','customer_id=invoice_client_id','left');
		$qry=$this->db->select('tbl_invoice.*, project_name, customer_name')->get_where('tbl_invoice',array('invoice_id'=>$id));
		return $qry->row_array();
	}
	
	public function get_invoiceItmDetails($id){		
		$qry=$this->db->select('*')->get_where('tbl_invoice_items',array('invoiceitm_invoice_id'=>$id));
		return $qry->result_array();
	}
	
	public function get_invoiceID($id){
		$qry=$this->db->select('invoice_cat_id')->get_where('tbl_invoice',array('invoice_id'=>$id));
		$r=$qry->row_array();
		return $r['invoice_cat_id'];
	}
	
	public function save_invoice(){
		//print_r($_REQUEST);
		//print_r($_FILES);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$dat=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('invoice_date'))));
		$data=array(
			'invoice_no' => $this->input->post('invoice_no'),
			'invoice_date' => $dat,
			'invoice_client_id' => $this->input->post('invoice_client_id'),			
			'invoice_project_id' => $this->input->post('invoice_project_id'),
			'invoice_title' => $this->input->post('invoice_title'),			
			'invoice_gross_amount' => $this->input->post('fn_gross'),
			'invoice_tax_amount' => $this->input->post('fn_tax'),			
			'invoice_discount_amount' => $this->input->post('fn_dis'),			
			'invoice_nett_amount' => $this->input->post('fn_net'),						
			'invoice_note' => $this->input->post('invoice_note'),
			'invoice_status' => $this->input->post('invoice_status'),						
			'invoice_created_on' => $date,			
			'invoice_mod_on' => $date			
		);
		$this->db->insert('tbl_invoice',$data);	
		$invoiceID=$this->db->insert_id();
		
		$hidCount=$this->input->post('hidCount');
		
		for($i=1;$i<=$hidCount;$i++){
			$data_itm=array(
				'invoiceitm_invoice_id' => $this->input->post('invoice_no'),				
				'invoiceitm_title' => $this->input->post('invoiceitm_title'.$i),			
				'invoiceitm_description' => $this->input->post('invoiceitm_description'.$i),
				'invoiceitm_rate' => $this->input->post('invoiceitm_rate'.$i),			
				'invoiceitm_tax' => $this->input->post('invoiceitm_tax'.$i),
				'invoiceitm_discount' => $this->input->post('invoiceitm_discount'.$i),			
				'invoiceitm_amount' => $this->input->post('invoiceitm_amount'.$i)				
			);			
			$this->db->insert('tbl_invoice_items',$data_itm);	
		}			
	}
	
	public function update_invoice(){
		//print_r($_REQUEST);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$id=$this->input->post('invoice_id');
		$dat=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('invoice_date'))));
		$data=array(
			'invoice_no' => $this->input->post('invoice_no'),
			'invoice_date' => $dat,
			'invoice_client_id' => $this->input->post('invoice_client_id'),			
			'invoice_project_id' => $this->input->post('invoice_project_id'),
			'invoice_title' => $this->input->post('invoice_title'),			
			'invoice_gross_amount' => $this->input->post('fn_gross'),
			'invoice_tax_amount' => $this->input->post('fn_tax'),			
			'invoice_discount_amount' => $this->input->post('fn_dis'),			
			'invoice_nett_amount' => $this->input->post('fn_net'),						
			'invoice_note' => $this->input->post('invoice_note'),
			'invoice_status' => $this->input->post('invoice_status'),											
			'invoice_mod_on' => $date			
		);
		$param=array(
			'invoice_id' => $id
		);
		$this->db->update('tbl_invoice',$data,$param);	
		
		$hidCount=$this->input->post('hidCount');
		$this->db->delete('tbl_invoice_items', array('invoiceitm_invoice_id' => $id));
		for($i=1;$i<=$hidCount;$i++){
			$data_itm=array(
				'invoiceitm_invoice_id' => $this->input->post('invoice_no'),				
				'invoiceitm_title' => $this->input->post('invoiceitm_title'.$i),			
				'invoiceitm_description' => $this->input->post('invoiceitm_description'.$i),
				'invoiceitm_rate' => $this->input->post('invoiceitm_rate'.$i),			
				'invoiceitm_tax' => $this->input->post('invoiceitm_tax'.$i),
				'invoiceitm_discount' => $this->input->post('invoiceitm_discount'.$i),			
				'invoiceitm_amount' => $this->input->post('invoiceitm_amount'.$i)				
			);			
			$this->db->insert('tbl_invoice_items',$data_itm);	
		}
	}
	
	public function delete_invoice(){
		$id=$this->input->post('id');			
		$this->db->delete('tbl_invoice', array('invoice_id' => $id));
		$this->db->delete('tbl_invoice_items', array('invoiceitm_invoice_id' => $id));
		echo 'deleted';
	}
	
	public function get_nextcode(){
		$this->db->select('invoice_no')->from('tbl_invoice')->order_by('invoice_id','desc')->limit(1);
		$query = $this->db->get();
		$rw=$query->row_array();	
		if($rw['invoice_no']==""){
			$bno="1";
		} else {
			$bno=GetNextCode($rw['invoice_no']);
		}
		return $bno;
	}						
}
?>