<?php
class Products_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	//public function get_products($slug, $limit, $start, $search){	
	public function get_products($slug, $search){
		$ser_data=$search;
		if($ser_data!=""){
			$this->db->like('prd_name', $ser_data);
            $this->db->or_like('prd_code', $ser_data);
			$this->db->or_like('prd_avail', $ser_data);
			$this->db->or_like('prd_status', $ser_data);					
		}
		//$this->db->limit($limit, $start);
		if($slug){
			$this->db->where('prd_catid',$slug);
		}
		
		$qry=$this->db->select('prd_id, prd_name, prd_code, prd_avail, prd_status')->get_where('tbl_products',array('prd_catchk'=>0));
		//echo $this->db->last_query();
		$result=$qry->result_array();			
		return $result;
	}
	
	public function get_prodBackID($id){
		$qry=$this->db->select('prd_catid')->get_where('tbl_products',array('prd_id'=>$id));
		$r=$qry->row_array();
		return $r['prd_catid'];
	}
	
	public function get_productsCount($id){
		$qry=$this->db->select('count("prd_id") as cnt')->get_where('tbl_products',array('prd_catid'=>$id));
		$r=$qry->row_array();
		return $r['cnt'];
	}
	
	public function total_records($param){
		$ser_data=$this->input->post('searching');
		if($ser_data!=""){
			$this->db->like('prd_name', $ser_data);
			$this->db->or_like('prd_code', $ser_data);
			$this->db->or_like('prd_avail', $ser_data);
            $this->db->or_like('prd_status', $ser_data);
			$this->db->where('prd_catid', $param);	
			$this->db->select('prd_id, prd_name, prd_code, prd_avail, prd_status')->from('tbl_products');
		    $query = $this->db->get();
			return $query->num_rows(); 			
		}else{
			$this->db->where('prd_catid', $param);	
			return $this->db->count_all_results('tbl_products');	
		}	
 	}
	
	public function get_autocomplete(){
		$query = $this->db->get_where('tbl_products', array('prd_catchk' => 0));
		return $query->result_array();
	}
	
	public function get_editDetails($id){
		$qry=$this->db->get_where('tbl_products',array('prd_id'=>$id));
		return $qry->row_array();
	}
	
	public function get_productsize($id){
		$query = $this->db->get_where('tbl_prod_size', array('siz_prdid' => $id));
		//echo $this->db->last_query();
	    return $query->result_array();
	}
	
	public function get_productimage($id){
		$query = $this->db->get_where('tbl_attachment', array('att_prdid' => $id));
	    return $query->result_array();
	}
	
	public function get_productrelated($id){	
		$query = $this->db->get_where('tbl_relatedproducts', array('rp_prdid' => $id));
		return $query->result_array();
	}
	
	public function get_productrelprd($id){	
		$query = $this->db->get_where('tbl_products', array('prd_id' => $id));
		return $query->row_array();
	}
	
	public function get_productsSize($id){
		$qry=$this->db->select('size_id, size_name')->order_by('size_order','ASC')->get_where('tbl_size');
		return $qry->result_array();
	}
	
	public function get_productsVariants(){
		$qry=$this->db->select('siz_size')->group_by('siz_size')->get_where('tbl_prod_size');
		return $qry->result_array();
	}
	
	public function get_productParams($tags,$param){
		$qry=$this->db->select('fparam_id, fparam_ftags_id, fparam_name')->order_by('fparam_order','ASC')->get_where('tbl_field_params',array('fparam_ftags_id'=>$tags));
		$result=$qry->result_array();
		foreach($result as $key=>$row){
			$result[$key]['value']=$this->get_productParamVales($row['fparam_id'],$param);
		}
		return $result;
	}
	
	public function get_productParamVales($tags,$param){
		$qry=$this->db->select('prodp_value')->get_where('tbl_product_params',array('prodp_param_id'=>$tags,'prodp_prd_id'=>$param));
		$r=$qry->row_array();
		return $r['prodp_value'];
	}
	
	public function get_productTags(){
		$qry=$this->db->select('ftags_id, ftags_name')->order_by('ftags_order','ASC')->get_where('tbl_field_tags');
		
		return $qry->result_array();
	}
	
	public function get_tagID($id){
		$qry=$this->db->select('prd_tags_id')->where('prd_id',$id)->get_where('tbl_products');
		$r=$qry->row_array();
		return $r['prd_tags_id'];
	}
	
	public function check_randno($no){
		$result = $this->db->get_where('tbl_products',array('prd_refid' => $no));
		if($result->num_rows()>0){
			$rand_no=mt_rand(1000000,99999999);
			$this->check_randno($rand_no);			
			return 1;
		} else {
			return $no;
		}
	}
	
	public function products_attimageremove($slug){			
		$query = $this->db->get_where('tbl_attachment', array('att_id' => $slug));
		$prd=$query->row_array();
		unlink('./images/products/n/'.$prd['att_name']);															
		unlink('./images/products/o/'.$prd['att_name']);															
		unlink('./images/products/t/'.$prd['att_name']);															
		return $this->db->delete('tbl_attachment', array('att_id' => $slug));				
	}
		
		
	
	public function save_products(){
		//print_r($_REQUEST);
		//print_r($_FILES);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		
		$rand_no=mt_rand(1000000,99999999);
		$ref_no=$this->check_randno($rand_no);
		
		$inv=0;
		if($this->input->post('invent')){
			$inv=$this->input->post('invent');
		}
		$hot=0;
		if($this->input->post('prd_hot')){
			$hot=$this->input->post('prd_hot');
		}
		
		$size=0;
		if($this->input->post('prd_size')){
			$size=$this->input->post('prd_size');
		}
		
		/*if($this->input->post('prd_qty')>0){
			$avail='Available';
		} else {
			$avail='Out of Stock';
		}*/
		$avail=$this->input->post('prd_avail');
		
		$category_id=$this->input->post('category_id');
			
		$data = array(
			'prd_refid'=> $ref_no,
			'prd_name' => trim($this->input->post('prd_name')),
			'prd_code'=> $this->input->post('prd_code'),
			'prd_brand'=> $this->input->post('prd_brand'),			
			'prd_weight'=> $this->input->post('prd_weight'),
			'prd_length'=> $this->input->post('prd_length'),
			'prd_height'=> $this->input->post('prd_height'),
			'prd_breadth'=> $this->input->post('prd_breadth'),			
			'prd_short_description'=> $this->input->post('prd_short_description'),
			'prd_long_description'=> $this->input->post('prd_long_description'),
			'prd_inventory'=> $inv,
			'prd_avail'=> $avail, 
			'prd_hotproduct'=> $hot, 
			'prd_size'=> $size, 
			'prd_catid'=> $category_id,
			'prd_catchk'=>  0,
			'prd_createddate' => $date,
			'prd_modifydate' => $date,
			'prd_status' => $this->input->post('prd_status')
		);
		$this->db->insert('tbl_products', $data);		
		$id=$this->db->insert_id();
		
		
		if($category_id!=""){
			$qry = $this->db->get_where('tbl_products', array('prd_id' => $category_id));
			$productcat=$qry->row_array();
			$pcstatus=1;
			if($productcat['prd_status']=="Inactive"){
				$pcstatus=0;
			}
			$this->db->update('tbl_products', array('prd_pcstatus' => $pcstatus), array('prd_id' => $id));
		}
		
		$config = array(
			'upload_path'   => './images/products/o/',
			'allowed_types' => 'jpg|jpeg|gif|png',
			'overwrite'     => 1,   
			'max_size'      => 1024000, 
			'max_width'     => 1000, 
			'max_height'	=> 1000
		);
		$this->load->library('upload', $config);
		$images = array();
		$files=$_FILES['prd_image'];
		// print_r($files);
		
		foreach ($files['name'] as $key => $image) {
			$_FILES['images[]']['name']= $files['name'][$key];
			$_FILES['images[]']['type']= $files['type'][$key];
			$_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
			$_FILES['images[]']['error']= $files['error'][$key];
			$_FILES['images[]']['size']= $files['size'][$key];

			$fileName = $id .'~'. $image;
			//echo "\n".$fileName;
			$images[] = $fileName;

			$config['file_name'] = $fileName;

			$this->upload->initialize($config);

			if($this->upload->do_upload('images[]')){
				$upload_data = $this->upload->data();
				$data1 = array(
					'att_prdid' => $id,			   
					'att_name' => $upload_data['file_name']			   
				);
				$this->db->insert('tbl_attachment', $data1);
										
				/** Image Resize Part Start **/								
			
				/*$src_path=IMG_URL.'product/o/';
				$nor_path=IMG_URL.'product/n/';
				$sml_path=IMG_URL.'product/t/';
				$img=$fileName;
				
				$this->load->library('image_lib');
				
				if (copy($src_path.$img,$nor_path.$img)) {
					$config_nor['source_image'] = $nor_path.$img;
					$config_nor['maintain_ratio'] = FALSE;
					$config_nor['width'] = IMG_NOR_W;
					$config_nor['height'] = IMG_NOR_H;													
					//$config['max_size']	= '100';// in KB						
					
					$this->image_lib->initialize($config_nor);
					$this->image_lib->resize();
					$this->image_lib->clear();										
				}						
				if (copy($src_path.$img,$sml_path.$img)) {
					$config_sml['source_image'] = $sml_path.$img;
					$config_sml['maintain_ratio'] = FALSE;
					$config_sml['width'] = IMG_SMALL_W;
					$config_sml['height'] = IMG_SMALL_H;													
					//$config['max_size']	= '100';// in KB						
					
					$this->image_lib->initialize($config_sml);
					$this->image_lib->resize();
					$this->image_lib->clear();
				}*/						
				
				//$src_path='./import/'.str_replace("/","",$prodName).'/';
				$src_path='./images/products/o/';
				$nor_path='./images/products/n/';
				$sml_path='./images/products/t/';
				$img=$fileName;
				
				$this->load->library('image_lib');
				//echo " <br> ".$src_path.$file." === ".$org_path.$img;
				/*if (copy($src_path.$img,$org_path.$img)) {
					$config_org['source_image'] = $org_path.$img;
					$config_org['maintain_ratio'] = FALSE;
					//$config_nor['width'] = IMG_NOR_W;
					//$config_nor['height'] = IMG_NOR_H;													
					//$config['max_size']	= '100';// in KB						
					
					$this->image_lib->initialize($config_org);
					$this->image_lib->resize();
					$this->image_lib->clear();										
				}*/	
										
				if (copy($src_path.$img,$nor_path.$img)) {
					$config_nor['source_image'] = $nor_path.$img;
					$config_nor['maintain_ratio'] = FALSE;
					$config_nor['width'] = IMG_NOR_W;
					$config_nor['height'] = IMG_NOR_H;													
					//$config['max_size']	= '100';// in KB						
					
					$this->image_lib->initialize($config_nor);
					$this->image_lib->resize();
					$this->image_lib->clear();										
				}						
				
				if (copy($src_path.$img,$sml_path.$img)) {
					$config_sml['source_image'] = $sml_path.$img;
					$config_sml['maintain_ratio'] = FALSE;
					$config_sml['width'] = IMG_SMALL_W;
					$config_sml['height'] = IMG_SMALL_H;													
					//$config['max_size']	= '100';// in KB						
					
					$this->image_lib->initialize($config_sml);
					$this->image_lib->resize();
					$this->image_lib->clear();
				}
				
				/** Image Resize Part End **/					
			}									
		}	
		
		$hidcnt=$this->input->post('hidCount');
		for($j=1;$j<=$hidcnt;$j++){
			if($size==0){
				$cur_siz='';
				$cur_qty=$this->input->post('prd_qty');
				$cur_pri=floatval($this->input->post('prd_price'));
				$cur_dispri=floatval($this->input->post('prd_discount'));
			} else {
				$cur_siz=$this->input->post('size'.$j);
				$cur_qty=$this->input->post('qty'.$j);
				$cur_pri=$this->input->post('price'.$j);
				$cur_dispri=$this->input->post('disprice'.$j);
			}
			$datasize = array(
				'siz_prdid' => $id,
				'siz_size' => $cur_siz,
				//'siz_qty'=> $cur_qty,
				'siz_balanceqty'=> $cur_qty,
				'siz_price'=> $cur_pri,
				'siz_discount'=>$cur_dispri 
			);
			//if(!$cur_siz==""){
				$this->db->insert('tbl_prod_size', $datasize);
			//}
		}
		
		$this->db->delete('tbl_relatedproducts', array('rp_prdid' => $id));
		$hidrelcnt=$this->input->post('hidRelCount');
		for($k=1;$k<=$hidrelcnt;$k++){
			$dataprd = array(
				'rp_prdid' => $id,
				'rp_relprdid'=> $this->input->post('relid'.$k),
			);
			if(!$this->input->post('relid'.$k)==""){
				$this->db->insert('tbl_relatedproducts', $dataprd);
			}
		}
		
		$paramID=explode(",",$this->input->post('paramID'));
		for($p=0;$p<count($paramID);$p++){
			$dataparam = array(
				'prodp_prd_id' => $id,
				'prodp_param_id'=> $paramID[$p],
				'prodp_value'=> $this->input->post('param_value'.$paramID[$p])
			);
			$this->db->insert('tbl_product_params', $dataparam);			
		}	
	}
	public function update_products(){
		//print_r($_REQUEST);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$prd_id=$this->input->post('prd_id');
		
		$inv=0;
		if($this->input->post('invent')){
			$inv=$this->input->post('invent');
		}
		
		$hot=0;
		if($this->input->post('prd_hot')){
			$hot=$this->input->post('prd_hot');
		}
		
		$size=0;
		if($this->input->post('prd_size')){
			$size=$this->input->post('prd_size');
		}
		
		$avail=$this->input->post('prd_avail');
			
		$data = array(				
			'prd_name' => trim($this->input->post('prd_name')),
			'prd_code'=> $this->input->post('prd_code'),
			'prd_brand'=> $this->input->post('prd_brand'),
			'prd_weight'=> $this->input->post('prd_weight'),
			'prd_length'=> $this->input->post('prd_length'),
			'prd_height'=> $this->input->post('prd_height'),
			'prd_breadth'=> $this->input->post('prd_breadth'),
			'prd_short_description'=> $this->input->post('prd_short_description'),
			'prd_long_description'=> $this->input->post('prd_long_description'),
			'prd_inventory'=> $inv,
			'prd_avail'=> $avail, 
			'prd_hotproduct'=> $hot, 
			'prd_size'=> $size, 
			'prd_modifydate' => $date,
			'prd_status' => $this->input->post('prd_status')
		);
		$this->db->update('tbl_products', $data, array('prd_id' => $prd_id));
		//echo $this->db->last_query();
		$id=$prd_id;
		
		$config = array(
			'upload_path'   => './images/products/o/',
			'allowed_types' => 'jpg|jpeg|gif|png',
			'overwrite'     => 1,   
			'max_size'      => 1024000, 
			'max_width'     => 1000, 
			'max_height'	=> 1000
		);
		$this->load->library('upload', $config);
		$images = array();
		$files=$_FILES['prd_image'];
		//print_r($files);
		foreach ($files['name'] as $key => $image) {
			$_FILES['images[]']['name']= $files['name'][$key];
			$_FILES['images[]']['type']= $files['type'][$key];
			$_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
			$_FILES['images[]']['error']= $files['error'][$key];
			$_FILES['images[]']['size']= $files['size'][$key];

			$fileName = $id .'~'. $image;

			$images[] = $fileName;

			$config['file_name'] = $fileName;
			//echo "<br>";
			//print_r($config);
			//print_r($images);
			$this->upload->initialize($config);

			if ($this->upload->do_upload('images[]')) {
				//echo $fileName;
				$upload_data = $this->upload->data();
				$data1 = array(
					'att_prdid' => $id,			   
					'att_name' => $upload_data['file_name']			   
				);
				$this->db->insert('tbl_attachment', $data1);
				
				/** Image Resize Part Start **/		
				
				//$src_path='./import/'.str_replace("/","",$prodName).'/';
				$src_path='./images/products/o/';
				$nor_path='./images/products/n/';
				$sml_path='./images/products/t/';
				$img=$upload_data['file_name'];
				
				$this->load->library('image_lib');
				//echo " <br> ".$src_path.$file." === ".$org_path.$img;
				/*if (copy($src_path.$file,$org_path.$img)) {
					$config_org['source_image'] = $org_path.$img;
					$config_org['maintain_ratio'] = FALSE;
					//$config_nor['width'] = IMG_NOR_W;
					//$config_nor['height'] = IMG_NOR_H;													
					//$config['max_size']	= '100';// in KB						
					
					$this->image_lib->initialize($config_org);
					$this->image_lib->resize();
					$this->image_lib->clear();										
				}*/	
										
				if (copy($src_path.$img,$nor_path.$img)) {
					$config_nor['source_image'] = $nor_path.$img;
					$config_nor['maintain_ratio'] = FALSE;
					$config_nor['width'] = IMG_NOR_W;
					$config_nor['height'] = IMG_NOR_H;													
					//$config['max_size']	= '100';// in KB						
					
					$this->image_lib->initialize($config_nor);
					$this->image_lib->resize();
					$this->image_lib->clear();										
				}						
				
				if (copy($src_path.$img,$sml_path.$img)) {
					$config_sml['source_image'] = $sml_path.$img;
					$config_sml['maintain_ratio'] = FALSE;
					$config_sml['width'] = IMG_SMALL_W;
					$config_sml['height'] = IMG_SMALL_H;													
					//$config['max_size']	= '100';// in KB						
					
					$this->image_lib->initialize($config_sml);
					$this->image_lib->resize();
					$this->image_lib->clear();
				}
				
				/** Image Resize Part Start **/						
																												
			} else {
				//print_r($this->upload->display_errors());
			}
		}
		$this->db->delete('tbl_prod_size', array('siz_prdid' => $id));
		
		if($this->input->post('prd_size')){
			$hidcnt=$this->input->post('hidCount');
			//echo $hidcnt;
			//$dat=$_POST;
			for($j=1;$j<=$hidcnt;$j++){				
				$cur_siz=$this->input->post('size'.$j);
				//$cur_qty=$this->input->post('qty'.$j);
				$cur_bqty=$this->input->post('balqty'.$j);
				$cur_pri=$this->input->post('price'.$j);
				$cur_dispri=$this->input->post('disprice'.$j);
				if($cur_siz){					
					$datasize = array(
						'siz_prdid' => $id,
						'siz_size' => $cur_siz,
						//'siz_qty'=> $cur_qty,
						'siz_balanceqty'=> $cur_bqty,
						'siz_price'=> $cur_pri,
						'siz_discount'=>$cur_dispri 
					);
					//print_r($datasize);
					$this->db->insert('tbl_prod_size', $datasize);					
				}
			}
		} else {
			$cur_siz='';
			//$cur_qty=$this->input->post('prd_qty');
			$cur_bqty=$this->input->post('prd_balqty');
			$cur_pri=floatval($this->input->post('prd_price'));
			$cur_dispri=floatval($this->input->post('prd_discount'));
			
			$datasize = array(
				'siz_prdid' => $id,
				'siz_size' => $cur_siz,
				//'siz_qty'=> $cur_qty,
				'siz_balanceqty'=> $cur_bqty,
				'siz_price'=> $cur_pri,
				'siz_discount'=>$cur_dispri 
			);
			//print_r($datasize);
			$this->db->insert('tbl_prod_size', $datasize);					
		} 
		
		$this->db->delete('tbl_relatedproducts', array('rp_prdid' => $id));
		$hidrelcnt=$this->input->post('hidRelCount');
		for($k=1;$k<=$hidrelcnt;$k++){
			$dataprd = array(
				'rp_prdid' => $id,
				'rp_relprdid'=> $this->input->post('relid'.$k),
			);
			if(!$this->input->post('relid'.$k)==""){
				$this->db->insert('tbl_relatedproducts', $dataprd);
			}
		}
		
		$this->db->delete('tbl_product_params', array('prodp_prd_id' => $id));
		$paramID=explode(",",$this->input->post('paramID'));
		for($p=0;$p<count($paramID);$p++){
			$dataparam = array(
				'prodp_prd_id' => $id,
				'prodp_param_id'=> $paramID[$p],
				'prodp_value'=> $this->input->post('param_value'.$paramID[$p])
			);
			$this->db->insert('tbl_product_params', $dataparam);			
		}
	}
	
	public function delete_products(){
		$id=$this->input->post('id');						
		$query = $this->db->get_where('tbl_attachment', array('att_prdid' => $id));		
		foreach($query->result_array() as $prd){
			unlink('./images/products/o/'.$prd['att_name']);				
			unlink('./images/products/t/'.$prd['att_name']);				
			unlink('./images/products/n/'.$prd['att_name']);				
			$this->db->delete('tbl_attachment1', array('att_id' => $prd['att_id']));
		}		
		$this->db->delete('tbl_products', array('prd_id' => $id));
		echo 'deleted';
	}		
}
?>