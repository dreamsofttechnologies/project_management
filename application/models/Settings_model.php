<?php
class Settings_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
		$this->load->library('session');
	}
	public function get_currency(){	
	     $this->db->select('cur_id,cur_country,cur_currcode');
         $query = $this->db->get('tbl_currency');
		 return $query->result_array();
	}
	public function get_settings(){
		$query = $this->db->get_where('tbl_settings', array('set_id' => '1'));
		return $query->row_array();
	}
	public function save_settings(){
		$id=$this->input->post('set_id');
		$data = array(
			'set_name' => $this->input->post('set_name'),
			'set_address' => $this->input->post('set_address'),
			'set_mobileno' => $this->input->post('set_mobileno'),
			'set_emailid' => $this->input->post('set_emailid'),						
			'set_curid' => $this->input->post('currency'),
			'set_timezone' => $this->input->post('timezonenew')
		);	
		
		if($id){
			$this->db->update('tbl_settings', $data, array('set_id' => $id));			
		} else {
			$this->db->insert('tbl_settings', $data);			
		}
		
		if($_FILES['set_logo']['name']!=""){			
			$config['upload_path']          = './images/logo/';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$config['max_size']             = 1024000;
			$config['max_width']            = 600;
			$config['max_height']           = 600;
			//print_r($config);
			$this->load->library('upload', $config);			
			
			$new_name=$_FILES['set_logo']['name'];
		 	$_FILES['set_logo']['name']=$new_name;
			
			$this->upload->do_upload('set_logo');
			//echo $this->upload->display_errors(); 
			$data11 = array('upload_data' => $this->upload->data());
			$upload_data = $this->upload->data();
			
			//$value=$new_name;
			$data1 = array(
				'set_logo' => $upload_data['file_name']			   
			);				
			$this->db->update('tbl_settings', $data1, array('set_id' => $id));						
		}
	}
	
	public function setting_imageremove($slug){			
		$query = $this->db->get_where('tbl_settings', array('set_id' => $slug));
		$prd=$query->row_array();
		unlink('./images/logo/'.$prd['set_logo']);		
		
		$data = array(
			'set_logo' => ''
		);									
		return $this->db->update('tbl_settings', $data, array('set_id' => $slug));
	}
}
?>
