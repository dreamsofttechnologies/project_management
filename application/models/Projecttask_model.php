<?php
class Projecttask_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	//public function get_projecttask($slug, $limit, $start, $search){
	public function get_projecttask($param=NULL){	
		$this->db->join('tbl_task_type','task_type_id=task_type','left');	
		$this->db->join('tbl_staff','staff_id=task_assigned','left');		
		$qry=$this->db->select('task_id, task_name, task_project_id, task_est_hours, task_priority, task_status, task_due_date, task_type_name, staff_name')->get_where('tbl_project_task',array('task_project_id'=>$param));
		$result=$qry->result_array();		
		foreach($result as $ind=>$row){
			$result[$ind]['actTiming']=$this->taskTiming($row['task_project_id'],$row['task_id']);
		}
		return $result;        
	}
	
	public function get_stafftask($param=NULL){	
		$this->db->join('tbl_task_type','task_type_id=task_type','left');	
		$this->db->join('tbl_staff','staff_id=task_assigned','left');		
		$qry=$this->db->select('task_id, task_name, task_project_id, task_est_hours, task_priority, task_status, task_due_date, task_type_name, staff_name')->get_where('tbl_project_task',array('task_assigned'=>$param));
		$result=$qry->result_array();		
		foreach($result as $ind=>$row){
			$result[$ind]['actTiming']=$this->taskTiming($row['task_project_id'],$row['task_id']);
		}
		return $result;        
	}
	
	public function taskTiming($projectID=NULL,$taskID=NULL){
		$qry=$this->db->select('sum(dailytask_hours) as sm')->get_where('tbl_daily_task',array('dailytask_project_id'=>$projectID,'dailytask_task_id'=>$taskID));
		$r=$qry->row_array();
		if($r['sm']>0){
			return $r['sm'];
		} else {
			return 0;
		}
	}
	
	public function projectName($param=NULL){
		$qry=$this->db->select('project_name')->get_where('tbl_project',array('project_id'=>$param));
		$r=$qry->row_array();
		return $r['project_name'];
	}
	
	public function staffName($param=NULL){
		$qry=$this->db->select('staff_name')->get_where('tbl_staff',array('staff_id'=>$param));
		$r=$qry->row_array();
		return $r['staff_name'];
	}
	
	public function budgetHeadList($param=NULL){
		$this->db->join('tbl_budget_head','budget_head_id=budget_budgethead_id','left');
		$qry=$this->db->select('budget_head_id, budget_head_name')->get_where('tbl_budget',array('budget_project_id'=>$param));
		$result=$qry->result_array();		
		return $result;        
	}
	
	public function typeList(){				
		$qry=$this->db->select('task_type_id, task_type_name')->get_where('tbl_task_type');
		$result=$qry->result_array();		
		return $result;        
	}
	
	public function staffList(){
		$qry=$this->db->select('staff_id, staff_name')->get_where('tbl_staff');
		return $qry->result_array();
	}
	
	public function parentList($param=NULL){
		if($param!=""){
			$this->db->where('task_id!=',$param);
		}
		$qry=$this->db->select('task_id, task_name')->get_where('tbl_project_task',array('task_parent'=>'0'));
		return $qry->result_array();
	}
			
	public function get_projecttaskDetails($id){
		$qry=$this->db->select('*')->get_where('tbl_project_task',array('task_id'=>$id));
		return $qry->row_array();
	}
		
	
	public function save_projecttask(){
		//print_r($_REQUEST);
		//print_r($_FILES);
		$dat=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('task_due_date'))));
		
		$data=array(
			'task_name' => $this->input->post('task_name'),
			'task_project_id' => $this->input->post('task_project_id'),
			'task_type' => $this->input->post('task_type'),			
			'task_budget_id' => $this->input->post('task_budget_id'),
			'task_est_hours' => $this->input->post('task_est_hours'),
			'task_priority' => $this->input->post('task_priority'),
			'task_due_date' => $dat,			
			'task_assigned' => $this->input->post('task_assigned'),	
			'task_parent' => $this->input->post('task_parent'),			
			'task_status' => $this->input->post('task_status')			
		);
		$this->db->insert('tbl_project_task',$data);				
	}
	public function update_projecttask(){
		//print_r($_REQUEST);
		$dat=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('task_due_date'))));
		
		$taskID=$this->input->post('task_id');
		
		$data=array(
			'task_name' => $this->input->post('task_name'),
			'task_project_id' => $this->input->post('task_project_id'),
			'task_type' => $this->input->post('task_type'),			
			'task_budget_id' => $this->input->post('task_budget_id'),
			'task_est_hours' => $this->input->post('task_est_hours'),
			'task_priority' => $this->input->post('task_priority'),
			'task_due_date' => $dat,			
			'task_assigned' => $this->input->post('task_assigned'),	
			'task_parent' => $this->input->post('task_parent'),			
			'task_status' => $this->input->post('task_status')			
		);
		$param=array(
			'task_id' => $taskID
		);
		$this->db->update('tbl_project_task',$data,$param);									
	}
	
	public function delete_projecttask(){
		$id=$this->input->post('id');			
		$this->db->delete('tbl_project_task', array('task_id' => $id));
		echo 'deleted';
	}
							
}
?>