<?php
class Paymentreceived_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	
	public function get_invoiceDetails($id){
		$this->db->join('tbl_project','project_id=invoice_project_id','left');
		$this->db->join('tbl_customer','customer_id=invoice_client_id','left');
		$qry=$this->db->select('invoice_id, project_id, project_name, customer_id, customer_name')->get_where('tbl_invoice',array('invoice_id'=>$id));
		return $qry->row_array();
	}
	
	public function save_paymentreceived(){
		//print_r($_REQUEST);
		//print_r($_FILES);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$dat=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('pr_date'))));
		$data=array(
			'pr_invoice_id' => $this->input->post('pr_invoice_id'),
			'pr_date' => $dat,
			'pr_project_id' => $this->input->post('pr_project_id'),			
			'pr_client_id' => $this->input->post('pr_client_id'),
			'pr_amount' => $this->input->post('pr_amount'),			
			'pr_remarks' => $this->input->post('pr_remarks'),									
			'pr_created_date' => $date,			
			'pr_mod_date' => $date			
		);
		$this->db->insert('tbl_payment_received',$data);					
	}
									
}
?>