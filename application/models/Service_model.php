<?php
class Service_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	//public function get_service($slug, $limit, $start, $search){
	public function get_service(){				
		$qry=$this->db->select('service_id, service_name, service_description, service_amount')->get_where('tbl_service');
		$result=$qry->result_array();		
		return $result;        
	}
			
	public function get_serviceDetails($id){
		$qry=$this->db->select('service_id, service_name, service_amount, service_description')->get_where('tbl_service',array('service_id'=>$id));
		return $qry->row_array();
	}
	
	public function save_service(){
		//print_r($_REQUEST);
		//print_r($_FILES);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$data=array(
			'service_name' => $this->input->post('service_name'),
			'service_description' => $this->input->post('service_description'),
			'service_amount' => $this->input->post('service_amount')					
		);
		$this->db->insert('tbl_service',$data);				
	}
	public function update_service(){
		//print_r($_REQUEST);
		$date=date('Y-m-d H:i:s');
		
		$id=$this->input->post('service_id');
		$data=array(
			'service_name' => $this->input->post('service_name'),
			'service_description' => $this->input->post('service_description'),
			'service_amount' => $this->input->post('service_amount')
		);
		$this->db->update('tbl_service',$data,array('service_id'=>$id));					
	}
	
	public function delete_service(){
		$id=$this->input->post('id');			
		$this->db->delete('tbl_service', array('service_id' => $id));
		echo 'deleted';
	}
						
}
?>