<?php
class Payments_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	//public function get_payment($slug, $limit, $start, $search){
	public function get_payments(){
		$this->db->join('tbl_staff','staff_id=payment_staff_id','left');	
		$qry=$this->db->select('payment_id, payment_date, payment_amount, payment_particulars, payment_remarks, staff_name')->get_where('tbl_payment');
		$result=$qry->result_array();		
		return $result;        
	}
	
	public function staffList(){
		$qry=$this->db->select('staff_id, staff_name')->get_where('tbl_staff');
		return $qry->result_array();
	}
	
	
	public function payableDet($id){
		$qry=$this->db->select('payable_staff_id, payable_date, payable_amount')->get_where('tbl_payable',array('payable_id'=>$id));
		return $qry->row_array();
	}
		
	public function get_paymentsDetails($id){
		$qry=$this->db->select('payment_id, payment_payable_id, payment_staff_id, payment_date, payment_amount, payment_particulars, payment_remarks')->get_where('tbl_payment',array('payment_id'=>$id));
		return $qry->row_array();
	}
			
	public function save_payments(){	
		$dat=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('payment_date'))));	
		$data=array(
			'payment_payable_id' => $this->input->post('payment_payable_id'),
			'payment_staff_id' => $this->input->post('payment_staff_id'),
			'payment_date' => $dat,
			'payment_amount' => $this->input->post('payment_amount'),			
			'payment_particulars' => $this->input->post('payment_particulars'),
			'payment_remarks' => $this->input->post('payment_remarks')	
		);
		$this->db->insert('tbl_payment',$data);				
	}
	
	public function update_payments(){
		//print_r($_REQUEST);
		
		$id=$this->input->post('payment_id');
		$dat=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('payment_date'))));	
		$data=array(
			'payment_payable_id' => $this->input->post('payment_payable_id'),
			'payment_staff_id' => $this->input->post('payment_staff_id'),
			'payment_date' => $dat,
			'payment_amount' => $this->input->post('payment_amount'),			
			'payment_particulars' => $this->input->post('payment_particulars'),
			'payment_remarks' => $this->input->post('payment_remarks')	
		);
		$this->db->update('tbl_payment',$data,array('payment_id'=>$id));					
	}
	
	public function delete_payments(){
		$id=$this->input->post('id');			
		$this->db->delete('tbl_payment', array('payment_id' => $id));
		echo 'deleted';
	}
								
}
?>