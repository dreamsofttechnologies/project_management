<?php
class Payables_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	//public function get_category($slug, $limit, $start, $search){
	public function get_payable(){		
		$this->db->join('tbl_staff','staff_id=payable_staff_id','left');					
		$qry=$this->db->select('payable_id, payable_date, payable_amount, payable_payitm_amount, staff_name')->get_where('tbl_payable');
		$result=$qry->result_array();
		foreach($result as $ind=>$row){
			$result[$ind]['paidAmt']=$this->getPaidAmt($row['payable_id']);
		}		
		return $result;        
	}
	
	public function getPaidAmt($id){			
		$qry=$this->db->select('sum(payment_amount) as sm')->get_where('tbl_payment',array('payment_payable_id'=>$id));
		$r=$qry->row_array();
		return $r['sm'];
	}
	
	public function itemsDets($param){
		$ids=explode(',',$param);
		$this->db->where_in('payitm_id',$ids);
		$this->db->join('tbl_staff','staff_id=payitm_staff_id','left');	
		$qry=$this->db->select('payitm_staff_id, sum(payitm_amount) as totAmt, staff_name')->group_by('payitm_staff_id')->get_where('tbl_payable_item');
		//return $this->db->last_query();
		return $qry->row_array();
	}
	
	public function get_payableDetails($id){
		$this->db->join('tbl_staff','staff_id=payable_staff_id','left');	
		$qry=$this->db->select('payable_id, payable_date, payable_staff_id, payable_amount, payable_payitm_amount, staff_name')->get_where('tbl_payable',array('payable_id'=>$id));
		return $qry->row_array();
	}
			
	public function save_payables(){
		//print_r($_REQUEST);
		//print_r($_FILES);
		$dat=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('payable_date'))));		
		$data=array(
			'payable_staff_id' => $this->input->post('payable_staff_id'),
			'payable_date' => $dat,
			'payable_amount' => $this->input->post('payable_amount'),			
			'payable_payitm_amount' => $this->input->post('payable_payitm_amount')
		);
		$this->db->insert('tbl_payable',$data);
		$payID=$this->db->insert_id();
		$itemsIds=$this->input->post('itemsIds');
		$ids=explode(',',$itemsIds);
		for($i=0;$i<count($ids);$i++){
			$data_itm=array(
				'payitm_payable_id' => $payID,
				'payitm_status' => 'Processed'
			);
			$this->db->update('tbl_payable_item',$data_itm,array('payitm_id'=>$ids[$i]));
		}
						
	}
	public function update_payables(){
		//print_r($_REQUEST);
		
		$id=$this->input->post('payable_id');
		$dat=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('payable_date'))));		
		$data=array(
			'payable_staff_id' => $this->input->post('payable_staff_id'),
			'payable_date' => $dat,
			'payable_amount' => $this->input->post('payable_amount'),						
		);
		$this->db->update('tbl_payable',$data,array('payable_id'=>$id));					
	}
	
	public function delete_payables(){
		$id=$this->input->post('id');	
		$data_itm=array(
			'payitm_payable_id' => 0,
			'payitm_status' => 'Pending'
		);
		$this->db->update('tbl_payable_item',$data_itm,array('payitm_payable_id'=>$id));
					
		$this->db->delete('tbl_payable', array('payable_id' => $id));
		echo 'deleted';
	}							
}
?>