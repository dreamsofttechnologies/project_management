<?php
class Staff_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	//public function get_staff($slug, $limit, $start, $search){
	public function get_staff(){	
		$this->db->join('tbl_stafftype','staff_type_id=staff_type','left');					
		$qry=$this->db->select('staff_id, staff_code, staff_name, staff_mobile, staff_type, staff_status, staff_type_name')->get_where('tbl_staff');
		$result=$qry->result_array();		
		return $result;        
	}
	
	public function typeList(){
		$qry=$this->db->select('staff_type_id, staff_type_name')->get_where('tbl_stafftype');
		return $qry->result_array();
	}
			
	
	public function get_staffDetails($id){
		$qry=$this->db->select('staff_id, staff_code, staff_name, staff_address, staff_city, staff_country, staff_email, staff_mobile, staff_type, staff_doj, staff_basicsalary')->get_where('tbl_staff',array('staff_id'=>$id));
		return $qry->row_array();
	}
		
	
	public function save_staff(){
		//print_r($_REQUEST);
		//print_r($_FILES);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$doj=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('staff_doj'))));
		$data=array(
			'staff_code' => $this->input->post('staff_code'),
			'staff_name' => $this->input->post('staff_name'),
			'staff_address' => $this->input->post('staff_address'),			
			'staff_city' => $this->input->post('staff_city'),
			'staff_country' => $this->input->post('staff_country'),
			'staff_email' => $this->input->post('staff_email'),
			'staff_mobile' => $this->input->post('staff_mobile'),
			'staff_type' => $this->input->post('staff_type'),
			'staff_doj' => $doj,
			'staff_basicsalary' => $this->input->post('staff_basicsalary'),
			'staff_status' => $this->input->post('staff_status'),
			'staff_created_on' => $date,			
			'staff_mod_on' => $date			
		);
		$this->db->insert('tbl_staff',$data);				
	}
	public function update_staff(){
		//print_r($_REQUEST);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$id=$this->input->post('staff_id');
		$doj=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('staff_doj'))));
		
		$data=array(
			'staff_code' => $this->input->post('staff_code'),
			'staff_name' => $this->input->post('staff_name'),
			'staff_address' => $this->input->post('staff_address'),			
			'staff_city' => $this->input->post('staff_city'),
			'staff_country' => $this->input->post('staff_country'),
			'staff_email' => $this->input->post('staff_email'),
			'staff_mobile' => $this->input->post('staff_mobile'),
			'staff_type' => $this->input->post('staff_type'),
			'staff_doj' => $doj,
			'staff_basicsalary' => $this->input->post('staff_basicsalary'),
			'staff_status' => $this->input->post('staff_status'),					
			'staff_mod_on' => $date			
		);
		$this->db->update('tbl_staff',$data,array('staff_id'=>$id));					
	}
	
	public function delete_staff(){
		$id=$this->input->post('id');			
		$this->db->delete('tbl_staff', array('staff_id' => $id));
		echo 'deleted';
	}
	
	public function get_nextcode(){
		$this->db->select('staff_code')->from('tbl_staff')->order_by('staff_id','desc')->limit(1);
		$query = $this->db->get();
		$rw=$query->row_array();	
		if($rw['staff_code']==""){
			$bno="STAFF001";
		} else {
			$bno=GetNextCode($rw['staff_code']);
		}
		return $bno;
	}						
}
?>