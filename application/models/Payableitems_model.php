<?php
class Payableitems_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	//public function get_category($slug, $limit, $start, $search){
	public function get_payableitems(){		
		$this->db->join('tbl_project','project_id=payitm_project_id','left');
		$this->db->join('tbl_budget_head','budget_head_id=payitm_budget_id','left');
		$this->db->join('tbl_staff','staff_id=payitm_staff_id','left');					
		$qry=$this->db->select('payitm_id, project_name, budget_head_name, staff_name, payitm_date, payitm_amount, payitm_status')->get_where('tbl_payable_item');
		$result=$qry->result_array();		
		return $result;        
	}
	
	
	public function projectDets($param=NULL){
		$qry=$this->db->select('project_id, project_name')->get_where('tbl_project',array('project_id'=>$param));
		$r=$qry->row_array();
		return $r;
	}
	
	public function budgetDets($projectID=NULL,$budgetID=NULL){
		$this->db->join('tbl_budget_head','budget_head_id=budget_budgethead_id','left');
		$qry=$this->db->select('budget_budgethead_id, budget_head_name')->get_where('tbl_budget',array('budget_project_id'=>$projectID,'budget_budgethead_id'=>$budgetID));
		$r=$qry->row_array();
		return $r;
	}
	
	public function staffDets($param=NULL){
		$qry=$this->db->select('staff_id, staff_name')->get_where('tbl_staff',array('staff_id'=>$param));
		$r=$qry->row_array();
		return $r;
	}
	
	public function getActCost($projectID,$bedgetID,$staffID=NULL){
		if($staffID!=""){
			$this->db->where('task_assigned',$staffID);
		}
		$this->db->join('tbl_daily_task','dailytask_task_id=task_id','left');
		$qry=$this->db->select('dailytask_hours, dailytask_staff_id')->get_where('tbl_project_task',array('task_project_id'=>$projectID,'task_budget_id'=>$bedgetID));
		$result=$qry->result_array();
		$cost=0;	
		foreach($result as $row){
			$qry2=$this->db->select('team_rate')->get_where('tbl_team',array('team_project_id'=>$projectID,'team_staff_id'=>$row['dailytask_staff_id']));
			$r2=$qry2->row_array();
			$cost=$cost+($row['dailytask_hours']*$r2['team_rate']);
		}
		return $cost;
	}
	
	public function get_payableitemsDetails($id){
		$this->db->join('tbl_project','project_id=payitm_project_id','left');
		$this->db->join('tbl_budget_head','budget_head_id=payitm_budget_id','left');
		$this->db->join('tbl_staff','staff_id=payitm_staff_id','left');	
		$qry=$this->db->select('payitm_id, payitm_project_id, payitm_budget_id, payitm_staff_id, project_name, budget_head_name, staff_name, payitm_date, payitm_amount, payitm_remarks, payitm_status')->get_where('tbl_payable_item',array('payitm_id'=>$id));
		return $qry->row_array();
	}
		
	
	public function save_payableitems(){				
		$dat=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('payitm_date'))));
		$data=array(
			'payitm_project_id' => $this->input->post('payitm_project_id'),
			'payitm_budget_id' => $this->input->post('payitm_budget_id'),
			'payitm_staff_id' => $this->input->post('payitm_staff_id'),			
			'payitm_date' => $dat,
			'payitm_amount' => $this->input->post('payitm_amount'),
			'payitm_remarks' => $this->input->post('payitm_remarks'),						
			'payitm_status' => $this->input->post('payitm_status')			
		);
		$this->db->insert('tbl_payable_item',$data);				
	}
	
	public function update_payableitems(){		
		$id=$this->input->post('payitm_id');		
		$dat=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('payitm_date'))));
		$data=array(
			'payitm_project_id' => $this->input->post('payitm_project_id'),
			'payitm_budget_id' => $this->input->post('payitm_budget_id'),
			'payitm_staff_id' => $this->input->post('payitm_staff_id'),			
			'payitm_date' => $dat,
			'payitm_amount' => $this->input->post('payitm_amount'),
			'payitm_remarks' => $this->input->post('payitm_remarks'),						
			'payitm_status' => $this->input->post('payitm_status')			
		);
		$this->db->update('tbl_payable_item',$data,array('payitm_id'=>$id));					
	}
	
	public function delete_payableitems(){
		$id=$this->input->post('id');			
		$this->db->delete('tbl_payable_item', array('payitm_id' => $id));
		echo 'deleted';
	}
		
}
?>