<?php
class Budget_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
	
	//public function get_budget($slug, $limit, $start, $search){
	public function get_budget($param){	
		$this->db->join('tbl_budget_head','budget_head_id=budget_budgethead_id','left');			
		$qry=$this->db->select('budget_id, budget_head_id, budget_head_name, budget_percentage, budget_amount')->get_where('tbl_budget',array('budget_project_id'=>$param));
		$result=$qry->result_array();		
		return $result;        
	}
	
	public function projectName($param){
		$qry=$this->db->select('project_name')->get_where('tbl_project',array('project_id'=>$param));
		$r=$qry->row_array();
		return $r['project_name'];
	}
	
	public function projectAmt($param){
		$qry=$this->db->select('sum(invoice_nett_amount) as sm')->get_where('tbl_invoice',array('invoice_project_id'=>$param));
		$r=$qry->row_array();
		return $r['sm'];
	}
	
	public function budgetHeadList(){				
		$qry=$this->db->select('budget_head_id, budget_head_name')->get_where('tbl_budget_head');
		$result=$qry->result_array();		
		return $result;        
	}
				
	public function save_budget(){
		//print_r($_REQUEST);
		//print_r($_FILES);
		$projectID=$this->input->post('projectID');
		$hidCount=$this->input->post('hidCount');
		
		$this->db->delete('tbl_budget', array('budget_project_id' => $projectID));
		for($i=1;$i<=$hidCount;$i++){
			if($this->input->post('budget_head_id'.$i)!=""){
				$data=array(			
					'budget_project_id' => $projectID,
					'budget_budgethead_id' => $this->input->post('budget_head_id'.$i),
					'budget_percentage' => $this->input->post('budget_per'.$i),
					'budget_amount' => $this->input->post('budget_amt'.$i)
				);
				$this->db->insert('tbl_budget',$data);
			}
		}			
	}							
}
?>