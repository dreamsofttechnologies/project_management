<?php
class Dailytask_model extends CI_Model {	
	public function __construct(){
    	$this->load->database();
	}  		
		
	public function dailyTaskList($param){	
		$this->db->join('tbl_project','project_id=dailytask_project_id','left');
		$this->db->join('tbl_staff','staff_id=dailytask_staff_id','left');	
		$this->db->join('tbl_project_task','task_id=dailytask_task_id','left');						
		$qry=$this->db->select('dailytask_id, dailytask_date, task_name, project_name, staff_name, dailytask_hours')->get_where('tbl_daily_task',array('dailytask_task_id'=>$param));
		$result=$qry->result_array();		
		return $result;        
	}
			
	public function taskDets($param=NULL){
		$this->db->join('tbl_project','project_id=task_project_id','left');
		$this->db->join('tbl_staff','staff_id=task_assigned','left');
		$qry=$this->db->select('task_id, task_name, project_id, project_name, staff_id, staff_name')->get_where('tbl_project_task',array('task_id'=>$param));
		return $qry->row_array();
	}
	
	public function get_dailytaskDetails($id=NULL){
		$qry=$this->db->select('dailytask_id, dailytask_cat_id, dailytask_name, dailytask_order, dailytask_status')->get_where('tbl_daily_task',array('dailytask_id'=>$id));
		return $qry->row_array();
	}
	
	public function get_dailytaskID($id){
		$qry=$this->db->select('dailytask_cat_id')->get_where('tbl_dailytask',array('dailytask_id'=>$id));
		$r=$qry->row_array();
		return $r['dailytask_cat_id'];
	}
	
	public function save_dailytask(){
		//print_r($_REQUEST);
		//print_r($_FILES);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$dat=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('dailytask_date'))));
		
		$data=array(
			'dailytask_date' => $dat,
			'dailytask_task_id' => $this->input->post('dailytask_task_id'),
			'dailytask_project_id' => $this->input->post('dailytask_project_id'),			
			'dailytask_staff_id' => $this->input->post('dailytask_staff_id'),
			'dailytask_hours' => $this->input->post('dailytask_hours')				
		);
		$this->db->insert('tbl_daily_task',$data);				
	}
	
	public function update_dailytask(){
		//print_r($_REQUEST);
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('ccusr_id');
		$id=$this->input->post('dailytask_id');
		$dat=date("Y-m-d",strtotime(str_replace('/','-',$this->input->post('dailytask_date'))));
		
		$data=array(
			'dailytask_date' => $dat,
			'dailytask_task_id' => $this->input->post('dailytask_task_id'),
			'dailytask_project_id' => $this->input->post('dailytask_project_id'),			
			'dailytask_staff_id' => $this->input->post('dailytask_staff_id'),
			'dailytask_hours' => $this->input->post('dailytask_hours')				
		);
		$this->db->update('tbl_daily_task',$data,array('dailytask_id'=>$id));					
	}
	
	public function delete_dailytask(){
		$id=$this->input->post('id');			
		$this->db->delete('tbl_dailytask', array('dailytask_id' => $id));
		echo 'deleted';
	}
							
}
?>