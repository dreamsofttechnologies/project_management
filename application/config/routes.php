<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['login'] = "index/login";
$route['login_check'] = "index/login_check";
$route['dashboard'] = "index/index";


/** Staff Links **/
$route['staff'] = "staff";
$route['addstaff'] = "staff/add_staff";
$route['editstaff/(:any)'] = "staff/edit_staff";
$route['save_staff'] = "staff/save_staff";
$route['update_staff'] = "staff/update_staff";
$route['delete_staff'] = "staff/delete_staff";

/** Client Links **/
$route['client'] = "client";
$route['addclient'] = "client/add_client";
$route['editclient/(:any)'] = "client/edit_client";
$route['save_client'] = "client/save_client";
$route['update_client'] = "client/update_client";
$route['delete_client'] = "client/delete_client";

/** User Links **/
$route['user'] = "user";
$route['user/(:any)/(:any)'] = "user";
$route['adduser'] = "user/add_user";
$route['usersettings'] = "user/user_settings";

/** Project Links **/
$route['project'] = "project";
$route['addproject'] = "project/add_project";
$route['editproject/(:any)'] = "project/edit_project";
$route['save_project'] = "project/save_project";
$route['update_project'] = "project/update_project";
$route['delete_project'] = "project/delete_project";


/** Budget Head Links **/
$route['budgethead'] = "budgethead";
$route['addbudgethead'] = "budgethead/add_budgethead";
$route['save_budgethead'] = "budgethead/save_budgethead";
$route['delete_budgethead'] = "budgethead/delete_budgethead";

/** Invoice Links **/
$route['invoice/(:any)'] = "invoice";
$route['addinvoice/(:any)'] = "invoice/add_invoice";
$route['save_invoice'] = "invoice/save_invoice";
$route['editinvoice/(:any)'] = "invoice/edit_invoice";
$route['update_invoice'] = "invoice/update_invoice";
$route['delete_invoice'] = "invoice/delete_invoice";

/** Quotation Links **/
$route['quotation/(:any)'] = "quotation";
$route['addquotation/(:any)'] = "quotation/add_quotation";
$route['save_quotation'] = "quotation/save_quotation";
$route['editquotation/(:any)'] = "quotation/edit_quotation";
$route['update_quotation'] = "quotation/update_quotation";
$route['delete_quotation'] = "quotation/delete_quotation";


/** Products Links **/
$route['listproducts/(:any)'] = "products";
$route['products/(:any)/(:any)/(:any)'] = "products";
$route['addproducts/(:any)'] = "products/add_products";
$route['editproducts/(:any)'] = "products/edit_products";
$route['save_products'] = "products/save_products";
$route['update_products'] = "products/update_products";
$route['delete_products'] = "products/delete_products";
$route['products_attimageremove'] = "products/products_attimageremove";


/** Paymentreceived Links **/
$route['paymentreceived/(:any)'] = "paymentreceived";
//$route['paymentreceived'] = "paymentreceived/add_client";
$route['editpaymentreceived/(:any)'] = "paymentreceived/edit_paymentreceived";
$route['save_paymentreceived'] = "paymentreceived/save_paymentreceived";
$route['update_paymentreceived'] = "paymentreceived/update_paymentreceived";
$route['delete_paymentreceived'] = "paymentreceived/delete_paymentreceived";

/** Service Links **/
$route['service'] = "service";
$route['addservice'] = "service/add_service";
$route['editservice/(:any)'] = "service/edit_service";
$route['save_service'] = "service/save_service";
$route['update_service'] = "service/update_service";
$route['delete_service'] = "service/delete_service";


/** Budget Links **/
$route['budget/(:any)'] = "budget";
$route['addbudget'] = "budget/add_budget";
$route['editbudget/(:any)'] = "budget/edit_budget";
$route['save_budget'] = "budget/save_budget";

/** Team Links **/
$route['team/(:any)'] = "team";
$route['save_team'] = "team/save_team";

/** Projecttask Links **/
$route['projecttask/(:any)'] = "projecttask";
$route['stafftask/(:any)'] = "projecttask/stafftask";
$route['addprojecttask/(:any)'] = "projecttask/add_projecttask";
$route['editprojecttask/(:any)'] = "projecttask/edit_projecttask";
$route['save_projecttask'] = "projecttask/save_projecttask";
$route['update_projecttask'] = "projecttask/update_projecttask";
$route['delete_projecttask'] = "projecttask/delete_projecttask";

/** Daily Tast Links **/
$route['dailytask/(:any)'] = "dailytask";
$route['save_dailytask'] = "dailytask/save_dailytask";

/** Production Links **/
$route['production'] = "production";
$route['production/(:any)/(:any)'] = "production";
$route['addproduction'] = "production/add_production";
$route['editproduction/(:any)'] = "production/edit_production";
$route['save_production'] = "production/save_team";

/** Project Cost Sheet Links **/
$route['projectcostsheet/(:any)'] = "projectcostsheet";
$route['addprojectcostsheet'] = "projectcostsheet/add_projectcostsheet";
$route['editprojectcostsheet/(:any)'] = "projectcostsheet/edit_projectcostsheet";
$route['save_projectcostsheetn'] = "projectcostsheet/save_team";

/** Payable Items Links **/
$route['payableitems'] = "payableitems";
$route['payableitems/(:any)/(:any)'] = "payableitems";
$route['addpayableitems/(:any)/(:any)/(:any)'] = "payableitems/add_payableitems";
$route['editpayableitems/(:any)'] = "payableitems/edit_payableitems";
$route['save_payableitems'] = "payableitems/save_payableitems";
$route['update_payableitems'] = "payableitems/update_payableitems";
$route['delete_payableitems'] = "payableitems/delete_payableitems";

/** Payable Items Links **/
$route['payables'] = "payables";
$route['editpayables/(:any)'] = "payables/edit_payables";
$route['save_payables'] = "payables/save_payables";
$route['update_payables'] = "payables/update_payables";
$route['delete_payables'] = "payables/delete_payables";
$route['create_payables'] = "payables/create_payables";

/** Payments Items Links **/
$route['payments/(:any)'] = "payments";
$route['addpayments/(:any)'] = "payments/add_payments";
$route['editpayments/(:any)'] = "payments/edit_payments";
$route['save_payments'] = "payments/save_payments";
$route['update_payments'] = "payments/update_payments";
$route['delete_payments'] = "payments/delete_payments";


/** Banner Links **/
$route['banner'] = "banner";
$route['addbanner'] = "banner/add_banner";
$route['editbanner/(:any)'] = "banner/edit_banner";
$route['save_banner'] = "banner/save_banner";
$route['update_banner'] = "banner/update_banner";
$route['delete_banner'] = "banner/delete_banner";
$route['banner_imageremove'] = "banner/banner_imageremove";



/** Settings Links **/
$route['settings'] = "settings";
$route['save_settings'] = "settings/save_settings";
$route['setting_imageremove'] = "settings/setting_imageremove";


/** Setting Change Password Links **/
$route['changepassword'] = "changepassword/index";
$route['change_password'] = "changepassword/change_save";


/** Product Import Links **/
$route['product_import'] = "productimport";
$route['import_products'] = "productimport/import_products";






$route['mailDispatch/(:any)'] = "orders/mailDispatch";

$route['logout'] = "index/logout";
//$route['showparameters/(:any)'] = "index/showParameters";


